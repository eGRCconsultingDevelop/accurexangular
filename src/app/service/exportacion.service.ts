import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { RequerimientoCliente } from '../shared/dto/RequerimientoCliente';
import { ParametrosCumplimiento } from '../shared/dto/ParametrosCumplimiento';
import { UsuarioService } from 'src/app/service/usuario.service';


@Injectable({
  providedIn: 'root'
})

export class ExportacionService {
	
	public codOK=200;
	public apiURLExportancion=environment.apiURLExportacion;
    private urlPeticion: string;
    private archivoPDF:string;
	private archivoExcel:string;
	private httpOptions; 
		
	constructor(protected http: HttpClient,
		private usuarioService:UsuarioService) { 
			this.httpOptions=this.usuarioService.httpOptions;
		}
	
	getManejoError(data:any){
		console.error(`Error Code: ${data.status}\nMessage: ${data.message}`);
	}

	getGenerarArchivoBusqueda(nombreUsuario:string,idRegulacion: number,estado: string,email: string) {
		this.urlPeticion=this.apiURLExportancion+'generarArchivoBusqueda/'+nombreUsuario+'/'+idRegulacion+'/'+estado+'/'+email;
 		return this.http.get<any>(this.urlPeticion, this.httpOptions);
	}

	getGenerarArchivoCumplimiento(nombreUsuario:string,idLineaBase: number,email: string,parametrosCumplimiento: ParametrosCumplimiento) {
		this.urlPeticion=this.apiURLExportancion+'generarArchivoCumplimiento/'+nombreUsuario+'/'+idLineaBase+'/'+email;
 		return this.http.post<any>(this.urlPeticion ,JSON.stringify(this.archivoPDF), this.httpOptions);
    }
    
    getGenerarArchivoCumplimientoTabla(nombreUsuario:string,idLineaBase: number,email: string,idReqSel: Array <number>) {
		this.urlPeticion=this.apiURLExportancion+'generarArchivoCumplimientoTabla/'+nombreUsuario+'/'+idLineaBase+'/'+email;
 		return this.http.post<any>(this.urlPeticion ,JSON.stringify(this.archivoExcel), this.httpOptions);
	}

	getGenerarArchivoLineaBase(nombreUsuario:string,idLineaBase: number,email: string) {
		this.urlPeticion=this.apiURLExportancion+'generarArchivoLineaBase/'+nombreUsuario+'/'+idLineaBase+'/'+email;
 		return this.http.get<any>(this.urlPeticion, this.httpOptions);
	}
	errorHandl(error) {
	    let errorMessage = '';
	    if(error.error instanceof ErrorEvent) {// Get client-side error
		    errorMessage = error.error.message;
	    } else {// Get server-side error
	    	errorMessage = `Error Code: ${error.status}\nMessage: ${error.message}`;
	    }
	    return errorMessage;
	}
	
}
