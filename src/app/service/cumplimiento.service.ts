import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { LineaBase } from '../shared/dto/LineaBase';
import { Regulacion } from '../shared/dto/Regulacion';
import { RequerimientoCliente } from '../shared/dto/RequerimientoCliente';
import { Requerimiento } from '../shared/dto/Requerimiento';
import { UsuarioService } from 'src/app/service/usuario.service';

@Injectable({
  providedIn: 'root'
})

export class CumplimientoService {
	
	public codOK=200;
	public apiURLCumplimiento=environment.apiURLCumplimiento;
	private urlPeticion: string;
	public lineaBase:LineaBase;
	public regulaciones:Array<Regulacion>;
	public requerimientoCliente:RequerimientoCliente;
	public responsables:Array <string>;
	private httpOptions; 
		
	constructor(protected http: HttpClient,
		private usuarioService:UsuarioService) { 
		this.httpOptions=this.usuarioService.httpOptions;
	}
	
	getManejoError(data:any){
		console.error(`Error Code: ${data.status}\nMessage: ${data.message}`);
	}

	getConsultarLineasBaseDefinitivas(nombreUsuario: string, cantidad: number) {
		this.urlPeticion=this.apiURLCumplimiento+nombreUsuario+'/'+cantidad;
 		return this.http.get<any>(this.urlPeticion, this.httpOptions);
	}

	getAgregarTextoReqCliente(nombreUsuario: string,idTexto: number,idRequerimiento:number,idLineaBase:number) {
		this.urlPeticion=this.apiURLCumplimiento+'agregarReqCliente'+'/'+nombreUsuario+'/'+idTexto+'/'+idRequerimiento+'/'+idLineaBase;
        return this.http.get<any>(this.urlPeticion, this.httpOptions);
	}

	getGuardarTextoReqCliente(nombreUsuario: string,reqCliente: RequerimientoCliente) {
		this.urlPeticion=this.apiURLCumplimiento+'guardarReqCliente/'+nombreUsuario;
 		return this.http.post<any>(this.urlPeticion ,JSON.stringify(reqCliente), this.httpOptions);
	}

	getConsultarRequerimientos(idRegulacion: number,nombreUsuario: string) {
		this.urlPeticion=this.apiURLCumplimiento+'requerimientos'+'/'+idRegulacion+'/'+nombreUsuario;
        return this.http.get<any>(this.urlPeticion, this.httpOptions);
	}

	errorHandl(error) {
	    let errorMessage = '';
	    if(error.error instanceof ErrorEvent) {// Get client-side error
		    errorMessage = error.error.message;
	    } else {// Get server-side error
	    	errorMessage = `Error Code: ${error.status}\nMessage: ${error.message}`;
	    }
	    return errorMessage;
	}
	
}
