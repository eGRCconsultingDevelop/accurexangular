import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { FiltrosBusqueda } from '../shared/dto/FiltrosBusqueda';
import { FiltrosDetalleBusqueda } from '../shared/dto/FiltrosDetalleBusqueda';
import { Busqueda } from '../shared/dto/Busqueda';
import { LineaBase } from '../shared/dto/LineaBase';
import { FusionLineaBase } from '../shared/dto/FusionLineaBase';
import { BusquedaUsuario } from '../shared/dto/BusquedaUsuario';
import { UsuarioService } from 'src/app/service/usuario.service';

@Injectable({
  providedIn: 'root'
})

export class LineaBaseService {
	
	public codOK=200;
	public apiURLLineaBase=environment.apiURLLineaBase;
	private urlPeticion: string;
	public lineaBase: LineaBase;
	public idBusquedasSelec:Array <number>=[];
	public busquedasSelec:Array <BusquedaUsuario>=[];
	public fusionLineaBase: FusionLineaBase;
	public lineasBaseFusion:Array <LineaBase>=[];
	private httpOptions;

	constructor(protected http: HttpClient,
		private usuarioService:UsuarioService) { 
			this.httpOptions=this.usuarioService.httpOptions;
		}	  
	
	getManejoError(data:any){
		console.error(`Error Code: ${data.status}\nMessage: ${data.message}`);
	}

	getConsultarLineasBase(nombreUsuario: string, cantidad: number) {
		this.urlPeticion=this.apiURLLineaBase+nombreUsuario+'/'+cantidad;
 		return this.http.get<any>(this.urlPeticion, this.httpOptions);
	}

	getGuardarLinea(lineaBase: LineaBase) {
		this.urlPeticion=this.apiURLLineaBase+'guardarLinea';
 		return this.http.post<any>(this.urlPeticion ,JSON.stringify(lineaBase), this.httpOptions);
	}

	getFusionarLineas(fusion: FusionLineaBase) {
		this.urlPeticion=this.apiURLLineaBase+'fusionarLineas';
 		return this.http.post<any>(this.urlPeticion ,JSON.stringify(fusion), this.httpOptions);
	}

	getBorrarLineaBaseUsuario(nombreUsuario:string,idLineaBaseUsuario:number) {
		this.urlPeticion=this.apiURLLineaBase+nombreUsuario+"/"+idLineaBaseUsuario;
 		return this.http.delete<any>(this.urlPeticion, this.httpOptions);
	}

	errorHandl(error) {
	    let errorMessage = '';
	    if(error.error instanceof ErrorEvent) {// Get client-side error
		    errorMessage = error.error.message;
	    } else {// Get server-side error
	    	errorMessage = `Error Code: ${error.status}\nMessage: ${error.message}`;
	    }
	    return errorMessage;
	}
	
}
