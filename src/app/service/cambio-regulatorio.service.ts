import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { LineaBase } from '../shared/dto/LineaBase';
import { Regulacion } from '../shared/dto/Regulacion';
import { RequerimientoCliente } from '../shared/dto/RequerimientoCliente';
import { Requerimiento } from '../shared/dto/Requerimiento';
import { CambioRegulatorio } from '../shared/dto/CambioRegulatorio';
import { UsuarioService } from 'src/app/service/usuario.service';

@Injectable({
  providedIn: 'root'
})

export class CambioRegulatorioService {
	
	public codOK=200;
	public apiURLCambioReg=environment.apiURLCambioReg;
	private urlPeticion: string;
	public cambioRegulatorio:CambioRegulatorio;
	public regulacionNueva:Regulacion;
	public lineasBaseAfect:Array <LineaBase>;
	public autores:Array<string>;
	public lineasBase:Array<string>;
	private httpOptions; 
		
	constructor(protected http: HttpClient,private usuarioService:UsuarioService) { 
		this.httpOptions=this.usuarioService.httpOptions;
	}
	
	getManejoError(data:any){
		console.error(`Error Code: ${data.status}\nMessage: ${data.message}`);
	}

	getConsultarCambiosRegulatoriosPendientes(nombreUsuario:string,idCliente: number) {
		this.urlPeticion=this.apiURLCambioReg+'pendientes/'+nombreUsuario+'/'+idCliente;
 		return this.http.get<any>(this.urlPeticion, this.httpOptions);
	}

	getAplicarCambio(nombreUsuario:string,idCambioReg: number,idCliente: number,idRegulacion: number,cambioEstado:boolean) {
		this.urlPeticion=this.apiURLCambioReg+'aplicarCambio/'+nombreUsuario+'/'+idCambioReg+'/'+idCliente+'/'+idRegulacion+'/'+cambioEstado;
        return this.http.post<any>(this.urlPeticion ,JSON.stringify(""), this.httpOptions);
	}

	getConsultarRegNuevas(nombreUsuario:string,idCliente: number) {
		this.urlPeticion=this.apiURLCambioReg+'regNuevas/'+nombreUsuario+'/'+idCliente;
        return this.http.get<any>(this.urlPeticion, this.httpOptions);
	}

	getAsociarALinea(nombreUsuario:string,idRegulacion: number,idCliente: number, idsLineasBase : Array<number>) {
		this.urlPeticion=this.apiURLCambioReg+'asociarALinea/'+nombreUsuario+'/'+idRegulacion+'/'+idCliente;
        return this.http.post<any>(this.urlPeticion ,JSON.stringify(idsLineasBase), this.httpOptions);
	}

    getConsultarCambiosRegulatoriosAplicados(nombreUsuario: string) {
		this.urlPeticion=this.apiURLCambioReg+'aplicados/'+nombreUsuario;
        return this.http.get<any>(this.urlPeticion, this.httpOptions);
	}
	errorHandl(error) {
	    let errorMessage = '';
	    if(error.error instanceof ErrorEvent) {// Get client-side error
		    errorMessage = error.error.message;
	    } else {// Get server-side error
	    	errorMessage = `Error Code: ${error.status}\nMessage: ${error.message}`;
	    }
	    return errorMessage;
	}
	
}
