import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { throwError } from 'rxjs';
import { retry, catchError } from 'rxjs/operators';
import { environment } from 'src/environments/environment';
import * as moment from 'moment';
import { FiltrosBusqueda } from '../shared/dto/FiltrosBusqueda';
import { FiltrosDetalleBusqueda } from '../shared/dto/FiltrosDetalleBusqueda';
import { Busqueda } from '../shared/dto/Busqueda';
import { ResultadoBusqueda } from '../shared/dto/ResultadoBusqueda';
import { Regulacion } from '../shared/dto/Regulacion';
import { DominioImpacto } from '../shared/dto/DominioImpacto';
import { UsuarioService } from 'src/app/service/usuario.service';

@Injectable({
  providedIn: 'root'
})

export class BusquedaService {
	
	public codOK=200;
	public apiURLBusqueda=environment.apiURLBusqueda;
	private urlPeticion: string;
	public resultadoBusqueda: ResultadoBusqueda;
	public regulacion: Regulacion;
	public eliminar:boolean;
	public id:number;
	public respuestaModal:string='';
	public dominiosImpacto:DominioImpacto;	
	public cantidadDominiosSelec:number;
	public menu:string;

	private httpOptions; 
		
	constructor(protected http: HttpClient,
		private usuarioService:UsuarioService) { 
			this.httpOptions=this.usuarioService.httpOptions;
		}		  
	
	getManejoError(data:any){
		console.error(`Error Code: ${data.status}\nMessage: ${data.message}`);
	}

	getTags(nombreUsuario:string,palabraClave: string, estado: string) {
		this.urlPeticion=this.apiURLBusqueda+'tags/'+nombreUsuario+'/'+palabraClave+'/'+estado;
 		return this.http.get<any>(this.urlPeticion, this.httpOptions);
	}
	
	getJurisdiccionesCliente(nombreUsuario:string,idCliente: number) {
		this.urlPeticion=this.apiURLBusqueda+'jurisdicciones/'+nombreUsuario+'/'+idCliente;
		console.log(this.urlPeticion);
 		return this.http.get<any>(this.urlPeticion, this.httpOptions);
	}
	
	
	getSectoresCliente(nombreUsuario:string,idCliente: number) {
		this.urlPeticion=this.apiURLBusqueda+'sectores/'+nombreUsuario+'/'+idCliente;
 		return this.http.get<any>(this.urlPeticion, this.httpOptions);
	}

	getDominiosImpactoCliente(nombreUsuario:string,idCliente: number) {
		this.urlPeticion=this.apiURLBusqueda+'dominiosImpacto/'+nombreUsuario+'/'+idCliente;
 		return this.http.get<any>(this.urlPeticion, this.httpOptions);
	} 

	getFuenteAutoridadCliente(nombreUsuario:string,idCliente: number) {
		this.urlPeticion=this.apiURLBusqueda+'fuentesAutoridad/'+nombreUsuario+'/'+idCliente;
 		return this.http.get<any>(this.urlPeticion, this.httpOptions);
	}

	getRegulacionesFiltro(filtrosBusqueda: FiltrosBusqueda, nombreUsuario: string, idBusqueda: number) {
		this.urlPeticion=this.apiURLBusqueda+'filtro/'+nombreUsuario+'/'+idBusqueda;
 		return this.http.post<any>(this.urlPeticion ,JSON.stringify(filtrosBusqueda), this.httpOptions);
	}

	getNombresRegulacion(nombreRegulacion: string, estado:string ,nombreUsuario:string) {
		this.urlPeticion=this.apiURLBusqueda+'nombreReg/'+nombreRegulacion+'/'+estado+'/'+nombreUsuario;
 		return this.http.get<any>(this.urlPeticion, this.httpOptions);
	}

	getRegulacionesNombre(idRegulacionesSelec: number[], nombreUsuario: string, nombreRegulacion: string, estado: string, idBusqueda:number) {
		this.urlPeticion=this.apiURLBusqueda+'filtroNombre/'+nombreUsuario+'/'+nombreRegulacion+'/'+estado+'/'+idBusqueda;
 		return this.http.post<any>(this.urlPeticion ,JSON.stringify(idRegulacionesSelec), this.httpOptions);
	}

	getRegulacionesFiltroDetalle(nombreUsuario:string,filtroDetalle: FiltrosDetalleBusqueda ,idBusqueda:number) {
		this.urlPeticion=this.apiURLBusqueda+'filtroDetalle/'+nombreUsuario+'/'+idBusqueda;
 		return this.http.post<any>(this.urlPeticion ,JSON.stringify(filtroDetalle), this.httpOptions);
	}

	getBusquedas(nombreUsuario: string, cantidad:number) {
		this.urlPeticion=this.apiURLBusqueda+nombreUsuario+'/'+cantidad;
 		return this.http.get<any>(this.urlPeticion, this.httpOptions);
	}

	getBorrarBusquedaUsuario(nombreUsuario:string,idBusquedaUsuario:number) {
		this.urlPeticion=this.apiURLBusqueda+nombreUsuario+'/'+idBusquedaUsuario;
 		return this.http.delete<any>(this.urlPeticion, this.httpOptions);
	}

	getBusquedasEstado(nombreUsuario: string, estado:string) {
		this.urlPeticion=this.apiURLBusqueda+'estado/'+nombreUsuario+'/'+estado;
 		return this.http.get<any>(this.urlPeticion, this.httpOptions);
	}
	
	getBusquedaUsuario(nombreUsuario:string,idBusquedaUsuario: number, estado:string) {
		this.urlPeticion=this.apiURLBusqueda+'busqueda/'+nombreUsuario+'/'+idBusquedaUsuario+'/'+estado;
 		return this.http.get<any>(this.urlPeticion, this.httpOptions);
	}

	getguardarBusqueda(nombreUsuario:string,busquedaDTO: Busqueda) {
		this.urlPeticion=this.apiURLBusqueda+'guardarBusqueda/'+nombreUsuario;
 		return this.http.put<any>(this.urlPeticion,JSON.stringify(busquedaDTO), this.httpOptions);
	}
	errorHandl(error) {
	    let errorMessage = '';
	    if(error.error instanceof ErrorEvent) {// Get client-side error
		    errorMessage = error.error.message;
	    } else {// Get server-side error
	    	errorMessage = `Error Code: ${error.status}\nMessage: ${error.message}`;
	    }
	    return errorMessage;
	}
	
}
