import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { throwError } from 'rxjs';
import { retry, catchError } from 'rxjs/operators';
import { environment } from 'src/environments/environment';
import * as moment from 'moment';
import { FiltrosBusqueda } from '../shared/dto/FiltrosBusqueda';
import { FiltrosDetalleBusqueda } from '../shared/dto/FiltrosDetalleBusqueda';
import { Busqueda } from '../shared/dto/Busqueda';
import { ResultadoBusqueda } from '../shared/dto/ResultadoBusqueda';
import { Regulacion } from '../shared/dto/Regulacion';
import { DominioImpacto } from '../shared/dto/DominioImpacto';
import { Usuario } from '../shared/dto/Usuario';
import { Soporte } from '../shared/dto/Soporte';

@Injectable({
  providedIn: 'root'
})

export class UsuarioService {
	
	public codOK=200;
	public apiURLUsuario=environment.apiURLUsuario;
	private urlPeticion: string;
	public usuario:Usuario=new Usuario();
	public usuarioEditar:Usuario=new Usuario();
	public opcion:string="";
	public httpOptions ;
	
	constructor(protected http: HttpClient) {
		this.inicializarHeaders();
	 }		 
	 
	 inicializarHeaders(){
		this.httpOptions = {
			headers: new HttpHeaders({
				'Content-Type': 'application/json',
				'Access-Control-Allow-Methods': 'GET, POST, DELETE, PUT, OPTIONS, HEAD',
				'Authorization': this.usuario.token 
				
				})
			}
	 }
	
	getManejoError(data:any){
		console.error(`Error Code: ${data.status}\nMessage: ${data.message}`);
	}

	agregarToken(){
		console.log(this.usuario);
		this.httpOptions = {
			headers: new HttpHeaders({
				'Content-Type': 'application/json',
				'Access-Control-Allow-Methods': 'GET, POST, DELETE, PUT, OPTIONS, HEAD',
				'Authorization': this.usuario.token 
				})
			}
	}

	getConsultarUsuariosCliente(nombreUsuario: string, idCliente: number) {
		this.urlPeticion=this.apiURLUsuario+nombreUsuario+'/'+idCliente;
 		return this.http.get<any>(this.urlPeticion, this.httpOptions);
	}

	getiIncluirUsuario(nombreUsuario: string,idCliente: number,usuario:Usuario) {
		this.urlPeticion=this.apiURLUsuario+nombreUsuario+'/'+idCliente;
 		return this.http.post<any>(this.urlPeticion ,JSON.stringify(usuario), this.httpOptions);
	}

	getInactivarUsuario(nombreUsuario: string,nombreUsuarioMod:string,idCliente:number) {
		this.urlPeticion=this.apiURLUsuario+'inactivar/'+nombreUsuario+'/'+nombreUsuarioMod+'/'+idCliente;
 		return this.http.put<any>(this.urlPeticion,JSON.stringify(nombreUsuarioMod), this.httpOptions);
	}

	getActivarUsuario(nombreUsuario: string,nombreUsuarioMod:string,idCliente:number) {
		this.urlPeticion=this.apiURLUsuario+'activar/'+nombreUsuario+'/'+nombreUsuarioMod+'/'+idCliente;
 		return this.http.put<any>(this.urlPeticion,JSON.stringify(nombreUsuarioMod), this.httpOptions);
	}

	getMoficarUsuario(nombreUsuario: string,nombreUsuarioMod:string,idCliente:number, usuario:Usuario) {
		this.urlPeticion=this.apiURLUsuario+'activar/'+nombreUsuario+'/'+nombreUsuarioMod+'/'+idCliente;
 		return this.http.put<any>(this.urlPeticion,JSON.stringify(usuario), this.httpOptions);
	} 

	getBorrarUsuario(nombreUsuario: string,nombreUsuarioBorrar:string) {
		this.urlPeticion=this.apiURLUsuario+nombreUsuario+'/'+nombreUsuarioBorrar;
 		return this.http.delete<any>(this.urlPeticion, this.httpOptions);
	}

	getCambiarContrasena(usuario: Usuario) {
		this.agregarToken();
		console.log(this.httpOptions)
		this.urlPeticion=this.apiURLUsuario+'cambiarContrasena';
 		return this.http.post<any>(this.urlPeticion ,JSON.stringify(usuario), this.httpOptions);
	}
	
	getAutenticar(usuario: Usuario) {
		this.inicializarHeaders();
		this.urlPeticion=this.apiURLUsuario+'autenticar';
 		return this.http.post<any>(this.urlPeticion ,JSON.stringify(usuario), this.httpOptions);
	}

	getSolicitarSoporte(nombreUsuario: string, soporte: Soporte) {
		this.urlPeticion=this.apiURLUsuario+'soporte/'+nombreUsuario;
 		return this.http.post<any>(this.urlPeticion ,JSON.stringify(soporte), this.httpOptions);
	}

	getValidarToken(nombreUsuario: string) {
		this.urlPeticion=this.apiURLUsuario+'validarToken/'+nombreUsuario;
		return this.http.post<any>(this.urlPeticion ,JSON.stringify(nombreUsuario), this.httpOptions);
	}
	

	errorHandl(error) {
	    let errorMessage = '';
	    if(error.error instanceof ErrorEvent) {// Get client-side error
		    errorMessage = error.error.message;
	    } else {// Get server-side error
	    	errorMessage = `Error Code: ${error.status}\nMessage: ${error.message}`;
	    }
	    return errorMessage;
	}
	
}
