import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';


const routes: Routes = [
  { path: '', pathMatch: 'full', redirectTo: 'login' },
  { path: 'search', loadChildren: () => import('./pages/search/search.module').then(m => m.SearchModule) },
  { path: 'fulfillment', loadChildren: () => import('./pages/fulfillment/fulfillment.module').then(m => m.FulfillmentModule) },
  { path: 'baseline', loadChildren: () => import('./pages/baseline/baseline.module').then(m => m.BaselineModule) },
  { path: 'login', loadChildren: () => import('./pages/login/login.module').then(m => m.LoginModule) },
  { path: 'password', loadChildren: () => import('./pages/password/password.module').then(m => m.PasswordModule) },
  { path: 'users', loadChildren: () => import('./pages/users/users.module').then(m => m.UsersModule) },
  { path: 'regulations', loadChildren: () => import('./pages/regulations/regulations.module').then(m => m.RegulationsModule) },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
