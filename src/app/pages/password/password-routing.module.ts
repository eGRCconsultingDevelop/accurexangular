import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ChangeComponent } from './change/change.component';
import { ForgotComponent } from './forgot/forgot.component';

const routes: Routes = [
  { path: 'forgot', component: ForgotComponent },
  { path: 'change', component: ChangeComponent }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PasswordRoutingModule { }
