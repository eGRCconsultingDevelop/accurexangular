import {
  MatCardModule,
  MatButtonModule,
  MatCheckboxModule
} from '@angular/material';

export const PasswordMaterialModules = [
  MatCardModule,
  MatButtonModule,
  MatCheckboxModule
];
