import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PasswordRoutingModule } from './password-routing.module';

import { ForgotComponent } from './forgot/forgot.component';
import { ChangeComponent } from './change/change.component';

import { PasswordMaterialModules } from './password.material';
import { FormsModule } from '@angular/forms';
import { PasswordStrengthBarComponent } from './password-strength/password-strength.component';

@NgModule({
  declarations: [
    ForgotComponent,
    ChangeComponent,
    PasswordStrengthBarComponent
  ],
  imports: [
    CommonModule,
    PasswordRoutingModule,
    FormsModule,
    ...PasswordMaterialModules
  ]
})
export class PasswordModule { }
