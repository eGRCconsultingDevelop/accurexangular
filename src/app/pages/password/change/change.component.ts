import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material';
import { Router } from '@angular/router';
import { UsuarioService } from 'src/app/service/usuario.service';
import { ModalValidadorComponent } from 'src/app/shared/components/modal-validador/modal-validador.component';

@Component({
  selector: 'app-change',
  templateUrl: './change.component.html',
  styleUrls: ['./change.component.scss']
})
export class ChangeComponent implements OnInit {
   
  public coinciden:boolean =true;
  public contrasenaNueva:string ="";
  public confirmacionContrasena:string ="";
  public barLabel: string = "Password strength:";  
  constructor(private usuarioService:UsuarioService,
    public router:Router,public dialog: MatDialog) { }
  
  ngOnInit() {
  
  }
  cancelar(){
    this.router.navigate(['/search/history']);
  }
  cambiarContrasena(){
    console.log(this.usuarioService.usuario)
    if(this.contrasenaNueva != this.confirmacionContrasena){
      this.coinciden=false;
    }else if(this.contrasenaNueva == this.usuarioService.usuario.contrasena){
      const dialogRef = this.dialog.open(ModalValidadorComponent, {
        width: '500px',
        data: {
          body1:"La contraseña es igual a la anterior",
          title:"Cambio de contraseña",
          acceptButton:"Aceptar"
        }
      });
    
    }else{
     
    
      this.usuarioService.usuario.contrasenaNueva=this.confirmacionContrasena;
      this.usuarioService.getCambiarContrasena(this.usuarioService.usuario).subscribe(data => {
        if(data.status==this.usuarioService.codOK){

        console.log(data.response);
        const dialogRef = this.dialog.open(ModalValidadorComponent, {
          width: '500px',
          data: {
            body1:"La contraseña se cambio satisfactoriamente",
            title:"Cambio de contraseña",
            acceptButton:"Aceptar"
          }
        });
        this.router.navigate(['/search/history']);
        }else{
        this.usuarioService.getManejoError(data);
        }
        })

      this.coinciden=true;
    }
  }

}
