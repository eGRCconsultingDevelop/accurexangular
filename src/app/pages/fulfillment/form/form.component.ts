import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material';

import { ModalNotificationsComponent } from '../../../shared/components/modal-notifications/modal-notifications.component';

@Component({
  selector: 'app-form',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.scss']
})
export class FormComponent implements OnInit {

  constructor(
    private dialog: MatDialog
  ) { }

  ngOnInit() {
  }

  openNotificationsForm() {
    const dialogRef = this.dialog.open(ModalNotificationsComponent, {
      width: '500px',
      data: {

      }
    });
  }

}
