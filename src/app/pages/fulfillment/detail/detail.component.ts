import { Component, OnInit } from '@angular/core';
import { NestedTreeControl } from '@angular/cdk/tree';
import { MatTreeNestedDataSource, MatDialog } from '@angular/material';

import { ModalIngoBaselineComponent } from '../../../../app/shared/components/modal-ingo-baseline/modal-ingo-baseline.component';
import { BusquedaService } from 'src/app/service/busqueda.service';
import { LineaBaseService } from 'src/app/service/lineabase.service';
import { ItemNode } from 'src/app/shared/dto/ItemNode';
import { Regulacion } from 'src/app/shared/dto/Regulacion';
import { Nivel } from 'src/app/shared/dto/Nivel';
import {SelectionModel} from '@angular/cdk/collections';
import { Router } from '@angular/router';
import { LineaBase } from 'src/app/shared/dto/LineaBase';
import { CumplimientoService } from 'src/app/service/cumplimiento.service';
import { ModalCumplimientoComponent } from 'src/app/shared/components/modal-cumplimiento/modal-cumplimiento.component';
import { RequerimientoCliente } from 'src/app/shared/dto/RequerimientoCliente';
import { ParametrosCumplimiento } from 'src/app/shared/dto/ParametrosCumplimiento';
import { ExportacionService } from 'src/app/service/exportacion.service';
import { UsuarioService } from 'src/app/service/usuario.service';

@Component({
  selector: 'app-detail',
  templateUrl: './detail.component.html',
  styleUrls: ['./detail.component.scss']
})
export class DetailComponent implements OnInit {
  public collapsed = true;
  public filtersCollapsed = false;
  public arbol:Array<ItemNode>=[];
  public expandedTree=false;
  public collapsedTree =false;
  public mostrarReq=false;
  listTreeControl : Array<NestedTreeControl<any>>=[];
  listDatasources: Array<MatTreeNestedDataSource<any>>=[];
  public opcion: string;
  public busqueda:string='';
  public regulaciones: Array<Regulacion>=this.cumplimientoService.lineaBase.regulaciones;
  public lineaBase:LineaBase=this.cumplimientoService.lineaBase;
  public cont=0;
  public fuenteAutoridad:string;
  public nombreRegulacion:string;
  public palabraClave:string='';
  public repetida:boolean=false;
  public lineasBase:Array <LineaBase>;
  checklistSelection = new SelectionModel<ItemNode>(true /* multiple */);
  public regulacion:Regulacion;
  public requerimientosCliente:Array<RequerimientoCliente>=[];
  public requerimientosClienteTotal:Array<RequerimientoCliente>=[];
  public requerimientosConCumplimiento:number=0;
  public parametrosCumplimiento :ParametrosCumplimiento= new ParametrosCumplimiento();
 //public nivelesMod:Array <Nivel>=this.cumplimientoService.lineaBase.regulaciones
  constructor(private cumplimientoService:CumplimientoService,
    private dialog: MatDialog,
    private busquedaService:BusquedaService,
    private lineaBaseService:LineaBaseService,
    private exportacionService:ExportacionService,
    private usuarioService:UsuarioService,
    private router: Router
  ) {
   
    for(let i=0;i<this.regulaciones.length;i++){ 
      this.consultarRequerimientos(this.regulaciones[i].id);
      }
  }


  hasChild = (_: number, node: any) => !!node.children && node.children.length > 0;
  isReq = (_: number, node: any) => node.type=='R';

  tieneHijos(node){
    return !!node.children && node.children.length > 0;
  }

  ngOnInit() {

    for(let i=0;i < this.regulaciones.length;i++){
      for(let j=0;j < this.regulaciones[i].niveles.length;j++){
        this.parametrosCumplimiento.idTextosSel.push(this.regulaciones[i].niveles[j].idTexto);
        if(this.regulaciones[i].niveles[j].tieneRequerimientos == "S"){
     
          for(let h=0;h<this.regulaciones[i].niveles[j].requerimientos.length;h++){
            console.log(this.regulaciones[i].niveles[j].requerimientos[h].idRequerimiento);
            this.parametrosCumplimiento.idReqSel.push(this.regulaciones[i].niveles[j].requerimientos[h].idRequerimiento);
          }
        }
      
      }

    }
 
    console.log( this.regulaciones);
    this.listTreeControl=[];
    this.listDatasources=[];
    for(let reg of this.regulaciones){
      this.construirArbol('',reg);
    }
  
    //this.consultarLineasBase();
  }

  construirArbolPalabra(palabraClaveAux:string){
    this.palabraClave=palabraClaveAux;
    this.listTreeControl=[];
    this.listDatasources=[];
    for(let reg of this.regulaciones){
      this.construirArbol( this.palabraClave, reg);
    }
  }

  consultarRequerimientos(id:number){
    console.log(id);
   // requerimientosAny:Array<any>
    this.cumplimientoService.getConsultarRequerimientos(id,this.usuarioService.usuario.nombreUsuario).subscribe(data => {
      if(data.status==this.cumplimientoService.codOK){
      this.requerimientosCliente=data.response;
      console.log(this.requerimientosCliente);
      for(let i=0;i<this.requerimientosCliente.length;i++){ 
        this.requerimientosClienteTotal.push(this.requerimientosCliente[i]);
      }
      }else{
      this.cumplimientoService.getManejoError(data);
      }
      })
      
  }
  consultarRegulaciones(nombreRegulacion:string){
  this.busquedaService.getNombresRegulacion(nombreRegulacion,"ACTIVO",this.usuarioService.usuario.nombreUsuario).subscribe(data => {
    if(data.status==this.busquedaService.codOK){
    this.regulacion=data.response;
    console.log( this.regulacion);

    }else{
    this.busquedaService.getManejoError(data);
    }
    })
  }

  construirDS(id:number){
    for(let i=0;i<this.listDatasources.length;i++){
      if(this.listDatasources[i].data[0].idRegulacion==id){
        return this.listDatasources[i];
        break;
      }
    }
  }

  openInfoDialog(id) {
    this.busquedaService.regulacion=id;
    const dialogRef = this.dialog.open(ModalIngoBaselineComponent, {
      width: '900px'
    });
  }

  infoRequerimiento(requerimiento:string) {
    for(let i=0;i<this.requerimientosClienteTotal.length;i++){
        if(requerimiento==this.requerimientosClienteTotal[i].requerimiento.requerimiento){
          this.cumplimientoService.requerimientoCliente=this.requerimientosClienteTotal[i];
          console.log(this.cumplimientoService.requerimientoCliente);
        }
    }
    const dialogRef = this.dialog.open(ModalCumplimientoComponent, {
      width: '500px'
    });
  }

  opcionSeleccionada(event){

    if(this.opcion == "Expandir"){
      for(let i=0;i<this.listDatasources.length;i++){
        this.listTreeControl[i].dataNodes=this.listDatasources[i].data;
        this.listTreeControl[i].expandAll();
      }
    }else if(this.opcion =="Expandir Nivel 1"){
      for(let i=0;i<this.listDatasources.length;i++){   
        this.listTreeControl[i].dataNodes=this.listDatasources[i].data;
        this.listTreeControl[i].collapseAll();
        for(let node of this.listTreeControl[i].dataNodes){
        this.listTreeControl[i].expand(node);  
        }
      }
    }else if(this.opcion =="Contraer"){
        for(let i=0;i<this.listDatasources.length;i++){
        this.listTreeControl[i].collapseAll();    
      }
    }
  }

 /* descendantsAllSelected(node: ItemNode): boolean {
    for(let i=0;i<this.listDatasources.length;i++){
    const descendants =  this.listTreeControl[i].getDescendants(node);
    return descendants.every(child => this.checklistSelection.isSelected(child));
  }
}

  descendantsPartiallySelected(node: ItemNode): boolean {
    for(let i=0;i<this.listDatasources.length;i++){
  
      const descendants =  this.listTreeControl[i].getDescendants(node);
      const result = descendants.some(child => this.checklistSelection.isSelected(child));
      return result && !this.descendantsAllSelected(node);  
    }
   
  }

  todoItemSelectionToggle(node: ItemNode): void {
    for(let i=0;i<this.listDatasources.length;i++){
    this.checklistSelection.toggle(node);
    const descendants = this.listTreeControl[i].getDescendants(node);
    this.checklistSelection.isSelected(node)
      ? this.checklistSelection.select(...descendants)
      : this.checklistSelection.deselect(...descendants);
  }
}*/


todoItemSelectionToggle(node: ItemNode,event) {
  console.log(node);
   if(event.checked){
     this.lineaBaseService.lineaBase.idTextos.push(node.id);
     console.log(this.lineaBaseService.lineaBase.idTextos);
  }else{
    var index=this.lineaBaseService.lineaBase.idTextos.indexOf(node.id);
      if ( index !== -1 ) {
        this.lineaBaseService.lineaBase.idTextos.splice( index, 1 );
      }
      console.log(this.lineaBaseService.lineaBase.idTextos);
  }
}
  evaluarIsExpanded(node:any, index:number){
    if(node.cargaInicial){
      node.cargaInicial=false;
      if(node.coincidencias){
        this.listTreeControl[index].expand(node);
        return true;
      }
      if(!this.listTreeControl[index].isExpanded(node)){
        if(this.evaluarHijosCoincidentes(node)){
          this.listTreeControl[index].expand(node);
          return true;
        }
      }
    } 
    return this.listTreeControl[index].isExpanded(node);
  }

  evaluarHijosCoincidentes(node:any){
    if(this.tieneHijos(node)){
      for(let child of node.children){
        if(child.coincidencias){
          return true;
        }else{
          this.evaluarHijosCoincidentes(child);
        }
      }
    }
    return false;
  }

    construirArbol(busqueda:string, regulacion:Regulacion) {
    //defino la variable arbol
    let dataSource = new MatTreeNestedDataSource<any>();
    let treeControl = new NestedTreeControl<any>(node => node.children);
    let nodo: ItemNode= new ItemNode();
    let nivelesSuperiores:Array<Nivel>=[];
    this.arbol=[];
		//saco los niveles que NO tienen padre, es decir que el idTextoPadre sea nulo o vacio
    for(let i=0;i< regulacion.niveles.length ; i++){
      if(regulacion.niveles[i].nivelSuperior == null){
        nivelesSuperiores.push(regulacion.niveles[i]);
      }
    }
		//instancio el arbol
		//inicio la adi cion de hijos, le envio el arbol entero, los niveles padres, y TODOS los niveles existentes (padres e hijos)
    this.busqueda=busqueda;
    this.addRamaArbol(nodo, nivelesSuperiores, regulacion.niveles, regulacion.id, busqueda);
    dataSource.data=this.arbol;
    this.listTreeControl.push(treeControl);
    this.listDatasources.push(dataSource);
	}
	
  addRamaArbol(nodo:ItemNode,hijos:Array<Nivel>,nivelesEstructura:Array<Nivel>, idRegulacion:number, busqueda:string) {
    let nivelesSuperioresAux: Array<Nivel>=[];

    let nivel2:Nivel;
      for(let nivel of hijos) {
        for(let nivel2 of nivelesEstructura) {
          if(nivel2.idTextoPadre !=null){
            if(nivel.idTexto==nivel2.idTextoPadre) {
              nivelesSuperioresAux.push(nivel2);
            }
          }
        }
    
        let nodoHijo: ItemNode= new ItemNode();
        let reqs: Array<ItemNode>= [];
        let coincidencias:boolean=false;

        if(busqueda!='' && (nivel.texto.toUpperCase().includes(busqueda.toUpperCase()) 
        || nivel.textoAsociado.toUpperCase().includes(busqueda.toUpperCase()))){
          coincidencias=true;
        }

        if(nivel.requerimientos.length>0){
          for(let i=0;i< nivel.requerimientos.length ; i++){
            let nodoReq: ItemNode= new ItemNode();
            nodoReq.id=nivel.requerimientos[i].idRequerimiento;
            nodoReq.idTextoPadre=nivel.idTexto;
            nodoReq.texto=nivel.requerimientos[i].codigoRequerimiento;
            nodoReq.textoAsociado=nivel.requerimientos[i].requerimiento;
            nodoReq.type='R';
            nodoReq.tieneRequerimientos=nivel.tieneRequerimientos;
            nodoReq.coincidencias=false;
            nodoReq.cargaInicial=false;
            reqs.push(nodoReq);
          }
        }
        if(nivel.idTextoPadre==null){
          nodo= new ItemNode();
          nodo.id=nivel.idTexto;
          nodo.texto=nivel.texto;
          nodo.idTextoPadre=nivel.idTextoPadre;
          nodo.textoAsociado=nivel.textoAsociado;
          nodo.type='N';
          nodo.tieneRequerimientos=nivel.tieneRequerimientos;
          nodo.children=reqs;
          nodo.coincidencias=coincidencias;
          nodo.cargaInicial=true;
          nodo.idRegulacion=idRegulacion;
          nodoHijo=nodo;
          this.arbol.push(nodo);
        }else{
          nodoHijo.type='N';
          nodoHijo.id=nivel.idTexto;
          nodoHijo.texto=nivel.texto;
          nodoHijo.idTextoPadre=nivel.idTextoPadre;
          nodoHijo.textoAsociado=nivel.textoAsociado;
          nodoHijo.tieneRequerimientos=nivel.tieneRequerimientos;
          nodoHijo.children=reqs;
          nodoHijo.coincidencias=coincidencias;
          nodoHijo.cargaInicial=true;
          nodo.children.push(nodoHijo);
        }
        //llamo nuevamente a este metodo agregando un nodo nuevo, y los nuevos hijos q ahora son padres, y TODOS los niveles nuevamente
        this.addRamaArbol(nodoHijo, nivelesSuperioresAux, nivelesEstructura, idRegulacion, busqueda);
        nivelesSuperioresAux=[];      
      }
  }
  
  guardarLineaBase(){

    this.lineaBaseService.lineaBase.estadoLineaBaseUsuario="DEFINITIVO";

    console.log(this.lineaBaseService.lineaBase);
    this.lineaBaseService.getGuardarLinea(this.lineaBaseService.lineaBase).subscribe(data => {
      if(data.status==this.lineaBaseService.codOK){
  
      this.router.navigate(['/baseline']);
      }else{
      this.lineaBaseService.getManejoError(data);
      }
      })

  }
  guardarLineaBaseTemporal(){

    this.lineaBaseService.lineaBase.estadoLineaBaseUsuario="TEMPORAL";
    
   
    console.log(this.lineaBaseService.lineaBase);
    this.lineaBaseService.getGuardarLinea(this.lineaBaseService.lineaBase).subscribe(data => {
      if(data.status==this.lineaBaseService.codOK){
  
      this.router.navigate(['/baseline']);
      }else{
      this.lineaBaseService.getManejoError(data);
      }
      })

  }
  consultarLineasBase(){
    this.lineaBaseService.getConsultarLineasBase(this.usuarioService.usuario.nombreUsuario,1000).subscribe(data => {
      if(data.status==this.lineaBaseService.codOK){
        this.lineasBase=data.response;
        console.log(this.lineasBase);
        for(let i=0; i < this.lineasBase.length ; i++){
       if(this.lineaBaseService.lineaBase.nombreLineaBase == this.lineasBase[i].nombreLineaBase ){
        this.lineaBaseService.lineaBase.idLinea=this.lineasBase[i].idLinea;
        console.log(this.lineaBaseService.lineaBase);
      }
    }
        
      }else{
      this.lineaBaseService.getManejoError(data);
      }
      })
  }

  guardarCumplimiento(){

    for(let i=0;i<this.requerimientosClienteTotal.length;i++){
      if(this.requerimientosClienteTotal[i].nivelCumplimiento!="NO_ESPECIFICADO"){
        this.requerimientosConCumplimiento++;
      }
   
    }
    this.lineaBase.origen="BUSQUEDA";
    this.lineaBase.estadoCumplimiento=(this.requerimientosConCumplimiento/this.requerimientosClienteTotal.length)*100;
    console.log(this.lineaBase);

  /* this.lineaBaseService.getGuardarLinea(this.lineaBase).subscribe(data => {
      if(data.status==this.lineaBaseService.codOK){
    
      this.router.navigate(['/baseline']);
      }else{
      this.lineaBaseService.getManejoError(data);
      }
      })*/
  }

  exportarPdf(){
  
    console.log( this.parametrosCumplimiento.idReqSel);
    console.log( this.parametrosCumplimiento.idTextosSel);
  this.exportacionService.getGenerarArchivoCumplimiento(this.usuarioService.usuario.nombreUsuario,this.lineaBase.idLinea,"0",this.parametrosCumplimiento).subscribe(data => {
    if(data.status==this.exportacionService.codOK){
      var sampleArr = this.base64ToArrayBuffer(data.response);
      this.saveByteArray(this.lineaBase.nombreLineaBase, sampleArr);
     
    }else{
    
     this.busquedaService.getManejoError(data);
    }
    })
}

base64ToArrayBuffer(base64) {
  var binaryString = window.atob(base64);
  var binaryLen = binaryString.length;
  var bytes = new Uint8Array(binaryLen);
  for (var i = 0; i < binaryLen; i++) {
     var ascii = binaryString.charCodeAt(i);
     bytes[i] = ascii;
  }
  return bytes;
}

saveByteArray(reportName, byte) {
var blob = new Blob([byte], {type: "application/pdf"});
var link = document.createElement('a');
link.href = window.URL.createObjectURL(blob);
var fileName = reportName;
link.download = fileName;
link.click();
}
}
