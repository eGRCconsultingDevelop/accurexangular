import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { FulfillmentRoutingModule } from './fulfillment-routing.module';
import { FulfillmentComponent } from './fulfillment.component';

import { SharedModule } from '../../shared/shared.module';
import { FulfillmentMaterialModules } from './fulfillment.material';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { ListComponent } from './list/list.component';
import { FormComponent } from './form/form.component';
import { MomentModule } from 'ngx-moment';
import { DetailComponent } from './detail/detail.component';

@NgModule({
  declarations: [FulfillmentComponent, ListComponent, FormComponent, DetailComponent],
  imports: [
    CommonModule,
    FulfillmentRoutingModule,
    SharedModule,
    ReactiveFormsModule,
    FormsModule,
    MomentModule,
    ...FulfillmentMaterialModules
  ]
})
export class FulfillmentModule { }
