import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator, MatTableDataSource } from '@angular/material';
import { LineaBaseService } from 'src/app/service/lineabase.service';
import { LineaBase } from 'src/app/shared/dto/LineaBase';
import { Router } from '@angular/router';
import { CumplimientoService } from 'src/app/service/cumplimiento.service';
import { UsuarioService } from 'src/app/service/usuario.service';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss']
})
export class ListComponent implements OnInit {
  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  public switchHistory = 'recents';
  public carga:boolean=true;
  displayedColumns = ['name', 'description', 'date_entered','date_modified','percent','cumplimiento'];
  dataSource = new MatTableDataSource<any>([]);
  public lineasBase:Array <LineaBase>;

  
  constructor(private lineaBaseService:LineaBaseService,
    private router: Router,
    private usuarioService:UsuarioService, 
    private cumplimientoService:CumplimientoService) { }

  consultarLineasBase(){
    this.cumplimientoService.getConsultarLineasBaseDefinitivas(this.usuarioService.usuario.nombreUsuario,5).subscribe(data => {
      if(data.status==this.lineaBaseService.codOK){
        this.lineasBase=data.response;
        console.log(this.lineasBase);
        this.datosTabla();
        this.carga=false;
        this.dataSource.paginator = this.paginator;
      }else{
      this.lineaBaseService.getManejoError(data);
      }
      })
  }

  consultarLineaBase(element){
    console.log(element);
    this.cumplimientoService.lineaBase=element;
    this.router.navigate(['/fulfillment']);
  }
  datosTabla(){
    console.log("arreglo actualizado"+this.lineasBase);
 
      this.dataSource = new MatTableDataSource(this.lineasBase);  
      console.log(this.lineasBase);

  }
  ngOnInit() {

   this.consultarLineasBase();
    this.dataSource.paginator = this.paginator;
  }
}
