import { Component, OnInit, ɵConsole } from '@angular/core';
import { CumplimientoService } from 'src/app/service/cumplimiento.service';
import { Regulacion } from 'src/app/shared/dto/Regulacion';
import { NestedTreeControl } from '@angular/cdk/tree';
import { MatTreeNestedDataSource, MatDialog } from '@angular/material';
import { ItemNode } from 'src/app/shared/dto/ItemNode';
import { LineaBase } from 'src/app/shared/dto/LineaBase';
import { SelectionModel } from '@angular/cdk/collections';
import { Nivel } from 'src/app/shared/dto/Nivel';
import { BusquedaUsuario } from 'src/app/shared/dto/BusquedaUsuario';
import { BusquedaService } from 'src/app/service/busqueda.service';
import { ModalIngoBaselineComponent } from 'src/app/shared/components/modal-ingo-baseline/modal-ingo-baseline.component';
import { RequerimientoCliente } from 'src/app/shared/dto/RequerimientoCliente';
import { Requerimiento } from 'src/app/shared/dto/Requerimiento';
import { Router } from '@angular/router';
import { ModalNotificationsComponent } from 'src/app/shared/components/modal-notifications/modal-notifications.component';
import { ModalBaselineComponent } from 'src/app/shared/components/modal-baseline/modal-baseline.component';
import { ModalCumpliminetoConfirmarComponent } from 'src/app/shared/components/modal-cumplimiento-confirmar/modal-cumplimiento-confirmar.component';
import { LineaBaseService } from 'src/app/service/lineabase.service';
import { DatePipe } from '@angular/common';
import { ExportacionService } from 'src/app/service/exportacion.service';
import { ParametrosCumplimiento } from 'src/app/shared/dto/ParametrosCumplimiento';
import { UsuarioService } from 'src/app/service/usuario.service';


@Component({
  selector: 'app-fulfillment',
  templateUrl: './fulfillment.component.html',
  styleUrls: ['./fulfillment.component.scss']
})
export class FulfillmentComponent implements OnInit {

  public parametrosCumplimiento :ParametrosCumplimiento= new ParametrosCumplimiento();
  public requerimientoV :boolean=true;
  public criticidadV :boolean=true;
  public nivelV :boolean=true;
  public controlesV :boolean=true;
  public responsableV :boolean=true;
  public emailV :boolean=true;
  public planAccionV :boolean=true;
  public fechaPlanV :boolean=true;
  public notificacionV :boolean=true;
  public opcion: string;
  public busqueda:string='';
  public regulaciones: Array<Regulacion>=this.cumplimientoService.lineaBase.regulaciones;
  public lineaBase:LineaBase=this.cumplimientoService.lineaBase;
  public cont:number=0;
  public fuenteAutoridad:string;
  public nombreRegulacion:string;
  public palabraClave:string='';
  public repetida:boolean=false;
  public lineasBase:Array <LineaBase>;
  checklistSelection = new SelectionModel<ItemNode>(true /* multiple */);
  public activeHeaderTitles = [];
  public colorselect:string="Alta";
  public requerimientos:Array<Requerimiento>;
  public requerimiento:RequerimientoCliente=new RequerimientoCliente;
  public requerimientosCliente:Array<RequerimientoCliente>=[];
  public requerimientosClienteTotal:Array<RequerimientoCliente>=[];
  public requerimientosClienteGenerales:Array<RequerimientoCliente>=[];
  public idRequerimientos:Array <number>=[];
  public textosRequerimientos:Array <string>=[];
  public codigosRequerimiento:Array <string>=[];
  public nombreLineaBase:string=this.cumplimientoService.lineaBase.nombreLineaBase;
  public requerimientosConCumplimiento:number=0;
  public responsables:Array <string>=[];
  public headerTitles = [
     'Requerimiento', 'Criticidad',
    'Nivel de cumplimiento', 'Controles', 'Responsable',
    'email responsable', 'Plan de acción','Fecha de PA','Notificación'
  ];

  public requerimientoF: string;
  public controles: string;
  public emails: Array <string>=[];
  public plan: string;
  public criticidad:Array<string>;
  public nivelCumplimiento:Array<string>;
  public responsableSelec:Array<string>;
  public emailSelec:Array<string>;
  public fechaPlanAccion:string;
  public notificacion:Array<string>;
  public requerimientosFiltrados:Array<RequerimientoCliente>=[];
  public openedItems = [0, 3];
  public nivelCambio:Nivel;
  
  constructor( private lineaBaseService:LineaBaseService,
    private dialog: MatDialog,
    private cumplimientoService:CumplimientoService,
    private router: Router,
    private busquedaService:BusquedaService,
    private usuarioService:UsuarioService, 
    private exportacionService:ExportacionService) { }

  ngOnInit() {
    for(let i=0;i < this.regulaciones.length;i++){
      for(let j=0;j < this.regulaciones[i].niveles.length;j++){

        if(this.regulaciones[i].niveles[0].idTextoPadre != null){
          for(let j=0;j < this.regulaciones[i].niveles.length;j++){
            if(this.regulaciones[i].niveles[j].idTextoPadre == null){
              this.nivelCambio=this.regulaciones[i].niveles[0];
            this.regulaciones[i].niveles[0]=this.regulaciones[i].niveles[j];
            this.regulaciones[i].niveles[j]=this.nivelCambio;
            break;
            }
          
          }
        }

        this.parametrosCumplimiento.idTextosSel.push(this.regulaciones[i].niveles[j].idTexto);
        if(this.regulaciones[i].niveles[j].tieneRequerimientos == "S"){
     
          for(let h=0;h<this.regulaciones[i].niveles[j].requerimientos.length;h++){
            console.log(this.regulaciones[i].niveles[j].requerimientos[h].idRequerimiento);
            this.parametrosCumplimiento.idReqSel.push(this.regulaciones[i].niveles[j].requerimientos[h].idRequerimiento);
          }
        }
      
      }

    }
 
    this.activeHeaderTitles = [...this.headerTitles];
    //this.cumplimientoService.lineaBase=new LineaBase();
    console.log(this.regulaciones);
    console.log(this.nombreLineaBase);
    console.log(this.requerimientosFiltrados.length);
    if(this.requerimientosFiltrados.length == 0){
    for(let i=0;i<this.regulaciones.length;i++){ 
    this.consultarRequerimientos(this.regulaciones[i].id);
    }
  }else{
  
    this.requerimientosClienteTotal=[];
    this.requerimientosClienteTotal=this.requerimientosFiltrados;
    this.requerimientosFiltrados=[];
  }
    console.log(this.requerimientosClienteTotal);
    console.log(this.responsables);


  } 
  opcionSeleccionadaCriti(event, j){
  
    for(let i=0;i<this.requerimientosClienteTotal.length;i++){

      if(j==this.requerimientosClienteTotal[i].requerimiento.idRequerimiento){
        this.requerimientosClienteTotal[i].criticidad=event;
       
      }
    }
  }

  opcionSeleccionadaCumpli(event, j){
   
    for(let i=0;i<this.requerimientosClienteTotal.length;i++){

      if(j==this.requerimientosClienteTotal[i].requerimiento.idRequerimiento){
        this.requerimientosClienteTotal[i].nivelCumplimiento=event;
       
      }
    }
  }
  requerimientoControles(j,value){
 
    for(let i=0;i<this.requerimientosClienteTotal.length;i++){

      if(j==this.requerimientosClienteTotal[i].requerimiento.idRequerimiento){
        this.requerimientosClienteTotal[i].controles=value;
       
      }
    }
  }
  requerimientoResponsable(j,value){
    
    for(let i=0;i<this.requerimientosClienteTotal.length;i++){

      if(j==this.requerimientosClienteTotal[i].requerimiento.idRequerimiento){
        this.requerimientosClienteTotal[i].responsable=value;
       
      }
    }
  }
  requerimientoEmail(j,value){
  
    for(let i=0;i<this.requerimientosClienteTotal.length;i++){

      if(j==this.requerimientosClienteTotal[i].requerimiento.idRequerimiento){
        this.requerimientosClienteTotal[i].correosElectronicosResponsable=value;
       
      }
    }
  }
  requerimientoPlan(j,value){
    
    for(let i=0;i<this.requerimientosClienteTotal.length;i++){

      if(j==this.requerimientosClienteTotal[i].requerimiento.idRequerimiento){
        this.requerimientosClienteTotal[i].planAccion=value;
       
      }
    }
  }

  requerimientoFechaPlan(j,value){
    
    for(let i=0;i<this.requerimientosClienteTotal.length;i++){

      if(j==this.requerimientosClienteTotal[i].requerimiento.idRequerimiento){
        this.requerimientosClienteTotal[i].fechaEjecucionPlanAccion=value;
       
      }
    }
  }
  openInfoDialog(id) {
    this.busquedaService.regulacion=id;
    const dialogRef = this.dialog.open(ModalIngoBaselineComponent, {
      width: '900px'
    });
  }

  tieneHijos(node){
    return !!node.children && node.children.length > 0;
  }
  
  changeHeaders(event,headerTitle) {
    console.log(headerTitle);
    console.log(event.checked);
    if(event.checked){
    if(headerTitle == "Requerimiento" ){
      this.requerimientoV=true;
    }
    if(headerTitle == "Criticidad" ){
      this.criticidadV=true;
    }
    if(headerTitle == "Nivel de cumplimiento" ){
      this.nivelV=true;
    }
    if(headerTitle == "Controles" ){
      this.controlesV=true;
    }
    if(headerTitle == "Responsable" ){
      this.responsableV=true;
    }
    if(headerTitle == "email responsable" ){
      this.emailV=true;
    }
    if(headerTitle == "Plan de acción" ){
      this.planAccionV=true;
    }
    if(headerTitle == "Fecha de PA" ){
      this.fechaPlanV=true;
    }
    if(headerTitle == "Notificación" ){
      this.notificacionV=true;
    }
  }else{
    if(headerTitle == "Requerimiento" ){
      this.requerimientoV=false;
    }
    if(headerTitle == "Criticidad" ){
      this.criticidadV=false;
    }
    if(headerTitle == "Nivel de cumplimiento" ){
      this.nivelV=false;
    }
    if(headerTitle == "Controles" ){
      this.controlesV=false;
    }
    if(headerTitle == "Responsable" ){
      this.responsableV=false;
    }
    if(headerTitle == "email responsable" ){
      this.emailV=false;
    }
    if(headerTitle == "Plan de acción" ){
      this.planAccionV=false;
    }
    if(headerTitle == "Fecha de PA" ){
      this.fechaPlanV=false;
    }
    if(headerTitle == "Notificación" ){
      this.notificacionV=false;
    }
  }
  }

  toggleItem(i) {
    const key = this.openedItems.indexOf(i);
    if (key >= 0) {
      delete this.openedItems[key];
    } else {
      this.openedItems.push(i);
    }

  }
 
  notificaciones(j){
    for(let i=0;i<this.requerimientosClienteTotal.length;i++){

      if(j==this.requerimientosClienteTotal[i].requerimiento.idRequerimiento){
        this.cumplimientoService.requerimientoCliente=this.requerimientosClienteTotal[i];
       
      }
    }
    console.log(this.cumplimientoService.requerimientoCliente)
    const dialogRef = this.dialog.open(ModalNotificationsComponent, {
      width: '500px'
    });
  }
  arboles(){
    this.cumplimientoService.lineaBase=this.lineaBase;
    this.cumplimientoService.lineaBase.regulaciones=this.regulaciones;
    this.router.navigate(['/fulfillment/detail']);
  }
  notificacionActiva(j){
    for(let i=0;i<this.requerimientosClienteTotal.length;i++){

      if(j==this.requerimientosClienteTotal[i].requerimiento.idRequerimiento){
       return this.requerimientosClienteTotal[i].notificacionesActivo;
       
      }
    }
   
   
  }
  tieneInformacion(j){
    for(let i=0;i<this.requerimientosClienteTotal.length;i++){

      if(j==this.requerimientosClienteTotal[i].requerimiento.idRequerimiento){
     
        if(this.requerimientosClienteTotal[i].periodicidadNotificaciones != null || this.requerimientosClienteTotal[i].fechaInicioNotificaciones != null 
          || this.requerimientosClienteTotal[i].correosElectronicosNotificaciones != ""){
          
            return true;
        }
     
       
      }
    }
  }

  traerColorRequerimiento(j){
    for(let req of this.requerimientosClienteTotal){

      if(j.idRequerimiento==req.requerimiento.idRequerimiento){
       
        return req.criticidad;
      }
    }
    return new RequerimientoCliente;
  }

  traerResponsableRequerimiento(j){
    for(let req of this.requerimientosClienteTotal){

      if(j.idRequerimiento==req.requerimiento.idRequerimiento){
       
        return req.responsable;
      }
    }
    return new RequerimientoCliente;
  }
  
  traerRequerimientoCliente(j){
    for(let req of this.requerimientosClienteTotal){
   
      if(j.idRequerimiento==req.requerimiento.idRequerimiento){
       
        return req.requerimiento.requerimiento;
      }
    }
    return null;
  }

  traerControlesRequerimiento(j){
    for(let req of this.requerimientosClienteTotal){
   
      if(j.idRequerimiento==req.requerimiento.idRequerimiento){
       
        return req.controles;
      }
    }
    return new RequerimientoCliente;
  }

  traerCumplimientoRequerimiento(j){
    for(let req of this.requerimientosClienteTotal){
   
      if(j.idRequerimiento==req.requerimiento.idRequerimiento){
       
        return req.nivelCumplimiento;
      }
    }
    return new RequerimientoCliente;
  }

  traerEmailRequerimiento(j){
    for(let req of this.requerimientosClienteTotal){
   
      if(j.idRequerimiento==req.requerimiento.idRequerimiento){
       
        return req.correosElectronicosResponsable;
      }
    }
    return new RequerimientoCliente;
  }
  traerPlanRequerimiento(j){
    for(let req of this.requerimientosClienteTotal){
   
      if(j.idRequerimiento==req.requerimiento.idRequerimiento){
       
        return req.planAccion;
      }
    }
    return new RequerimientoCliente;
  }

  traerFechaPlanRequerimiento(j){
    for(let req of this.requerimientosClienteTotal){
   
      if(j.idRequerimiento==req.requerimiento.idRequerimiento){
       
        return req.fechaEjecucionPlanAccion;
      }
    }
    return new RequerimientoCliente;
  }

  consultarRequerimientos(id:number){
    console.log(id);
    this.responsables=[];
    let band=0;
    let band2=0;
   // requerimientosAny:Array<any>
    this.cumplimientoService.getConsultarRequerimientos(id,this.usuarioService.usuario.nombreUsuario).subscribe(data => {
      if(data.status==this.cumplimientoService.codOK){
      this.requerimientosCliente=data.response;
      console.log(this.requerimientosCliente);
      for(let i=0;i<this.requerimientosCliente.length;i++){ 
        this.requerimientosClienteTotal.push(this.requerimientosCliente[i]);
       if(this.requerimientosCliente[i].responsable != ""){
         for(let j=0;j<this.responsables.length;j++){
            if(this.requerimientosCliente[i].responsable ==this.responsables[j]){
              band=1;
              break
            }else{
              band=0;
            }
         }

         if(band == 0){
          this.responsables.push(this.requerimientosCliente[i].responsable);
         }

         band=0;
      
       }
       if(this.requerimientosCliente[i].correosElectronicosResponsable != ""){

        for(let j=0;j<this.emails.length;j++){
          if(this.requerimientosCliente[i].correosElectronicosResponsable ==this.emails[j]){
            band2=1;
            break
          }else{
            band2=0;
          }
        }
        if(band2 == 0){
          this.emails.push(this.requerimientosCliente[i].correosElectronicosResponsable);
         }

         band2=0;
     
    
       }
        
      }

      this.requerimientosClienteGenerales=this.requerimientosClienteTotal;
      }else{
      this.cumplimientoService.getManejoError(data);
      }
      })
      
  }

  transform(value: string) {

    if(value != null){
    if(value.length == undefined){
      var datePipe = new DatePipe("en-US");
      value = datePipe.transform(value, 'dd/MM/yyyy');
      return value;
    }else{
    return value;
    }
  }
 }

 

  guardarCumplimiento(){

    this.requerimientosConCumplimiento
      console.log(this.requerimientosClienteTotal);
      for(let i=0;i<this.requerimientosClienteTotal.length;i++){
        if(this.requerimientosClienteTotal[i].nivelCumplimiento!="NO_ESPECIFICADO"){
          this.requerimientosConCumplimiento++;
        }
        this.requerimientosClienteTotal[i].fechaEjecucionPlanAccion=this.transform(this.requerimientosClienteTotal[i].fechaEjecucionPlanAccion);
      this.cumplimientoService.getGuardarTextoReqCliente(this.usuarioService.usuario.nombreUsuario,this.requerimientosClienteTotal[i]).subscribe(data => {
        if(data.status==this.cumplimientoService.codOK){
          console.log(data);
        }else{
        this.cumplimientoService.getManejoError(data);
        }
        })
      }
   /*   this.lineaBase.origen="BUSQUEDA";
      this.lineaBase.estadoCumplimiento=(this.requerimientosConCumplimiento/this.requerimientosClienteTotal.length)*100;
      console.log(this.lineaBase);

     this.lineaBaseService.getGuardarLinea(this.lineaBase).subscribe(data => {
        if(data.status==this.lineaBaseService.codOK){
      
       
        }else{
        this.lineaBaseService.getManejoError(data);
        }
        })
*/
      const data = {
      title: 'Confirmación',
      body1: `Se ha guardado satisfactoriamente el cumplimento de la linea base `,
      body2: "''"+this.nombreLineaBase+"''",
      acceptButton: 'Aceptar',
      reason: ''
    };

    const dialogRef = this.dialog.open(ModalCumpliminetoConfirmarComponent, {
      width: '500px',
      data
    });
    this.router.navigate(['/fulfillment/list']);
  }

  filtrar(){

    this.requerimientosFiltrados=[];
    console.log(this.requerimientoF);
    console.log(this.controles);
    console.log(this.emailSelec);
    console.log(this.plan);
    console.log(this.criticidad);
    console.log(this.nivelCumplimiento);
    console.log(this.notificacion);
    console.log(this.responsableSelec);

    this.fechaPlanAccion=this.transform(this.fechaPlanAccion);
    console.log(this.fechaPlanAccion);
    for(let i=0;i<this.requerimientosClienteGenerales.length;i++){
      for(let h=0;h<1;h++){
        let band=0;
        if( this.controles != undefined){
           let intIndex =  this.requerimientosClienteGenerales[i].controles.indexOf(this.controles);
          if( intIndex != -1){
            band=1;
          }else{
            band=0;
            break;
          }
        }
          if(this.criticidad != undefined){
              for(let j=0;j<this.criticidad.length;j++){
                if(this.criticidad[j] == this.requerimientosClienteGenerales[i].criticidad){
                 band=1;
                 break;
                 }else{
                  band=0;
                 }
             }
             if(band==0){
              break;    
             }
           }
  
          if( this.requerimientoF != undefined){
            let intIndex =  this.requerimientosClienteGenerales[i].requerimiento.requerimiento.indexOf(this.requerimientoF);
            if( intIndex != -1){
              band=1;
            }else{
             band=0;
             break;
           }
          }
  
          if( this.responsableSelec != undefined){
            for(let j=0;j<this.responsableSelec.length;j++){
              if(this.responsableSelec[j] == this.requerimientosClienteGenerales[i].responsable){
               band=1;
               break;
               }else{
                band=0;
               }
           }
           if(band==0){
            break;    
           }
         }
  
         if( this.nivelCumplimiento != undefined){
          for(let j=0;j<this.nivelCumplimiento.length;j++){
            if(this.nivelCumplimiento[j] == this.requerimientosClienteGenerales[i].nivelCumplimiento){
             band=1;
             break;
             }else{
              band=0;
              
             }
         }
         if(band==0){
          break;    
         }
       }
      
       if( this.emailSelec != undefined){
        for(let j=0;j<this.emailSelec.length;j++){
          if(this.emailSelec[j] == this.requerimientosClienteGenerales[i].correosElectronicosResponsable){
           band=1;
           break;
           }else{
            band=0;
            
           }
       }
       if(band==0){
        break;    
       }
       
       }
       if( this.fechaPlanAccion != undefined){
        if(this.fechaPlanAccion == this.requerimientosClienteGenerales[i].fechaEjecucionPlanAccion){
           band=1;
         }else{
          band=0;
          break;
        }
       }
       if(this.plan != undefined){
         let intIndex =  this.requerimientosClienteGenerales[i].planAccion.indexOf(this.plan);
        if( intIndex != -1){
           band=1;
         }else{
          band=0;
        }
       }

       
  
       if( this.notificacion != undefined){
        for(let j=0;j<this.notificacion.length;j++){
          if(this.notificacion[j] == this.requerimientosClienteGenerales[i].notificacionesActivo){
           band=1;
           break;
           }else{
            band=0;
           }
       }
       if(band==0){
        break;    
       }
     }
  
     console.log(band);
          if(band==1){
            this.requerimientosFiltrados.push(this.requerimientosClienteGenerales[i]);
            console.log(this.requerimientosFiltrados);
            console.log(band);
          }
      }
     
  }

  this.ngOnInit();
}

exportarPdf(){
  
    console.log( this.parametrosCumplimiento.idReqSel);
    console.log( this.parametrosCumplimiento.idTextosSel);
  this.exportacionService.getGenerarArchivoCumplimiento(this.usuarioService.usuario.nombreUsuario,this.lineaBase.idLinea,"0",this.parametrosCumplimiento).subscribe(data => {
    if(data.status==this.exportacionService.codOK){
      var sampleArr = this.base64ToArrayBuffer(data.response);
      this.saveByteArray(this.lineaBase.nombreLineaBase, sampleArr);
     
    }else{
    
     this.busquedaService.getManejoError(data);
    }
    })
}

exportarExcel(){

  console.log( this.parametrosCumplimiento.idReqSel);

this.exportacionService.getGenerarArchivoCumplimientoTabla(this.usuarioService.usuario.nombreUsuario,this.lineaBase.idLinea,"0",this.parametrosCumplimiento.idTextosSel).subscribe(data => {
  if(data.status==this.exportacionService.codOK){
    var sampleArr = this.base64ToArrayBuffer(data.response);
    this.saveByteArray2(this.lineaBase.nombreLineaBase+".xls", sampleArr);
   
  }else{
  
   this.busquedaService.getManejoError(data);
  }
  })
}


base64ToArrayBuffer(base64) {
  var binaryString = window.atob(base64);
  var binaryLen = binaryString.length;
  var bytes = new Uint8Array(binaryLen);
  for (var i = 0; i < binaryLen; i++) {
     var ascii = binaryString.charCodeAt(i);
     bytes[i] = ascii;
  }
  return bytes;
}

saveByteArray(reportName, byte) {
var blob = new Blob([byte], {type: "application/pdf"});
var link = document.createElement('a');
link.href = window.URL.createObjectURL(blob);
var fileName = reportName;
link.download = fileName;
link.click();
}
saveByteArray2(reportName, byte) {
  var blob = new Blob([byte], {type: "application/xls"});
  var link = document.createElement('a');
  link.href = window.URL.createObjectURL(blob);
  var fileName = reportName;
  link.download = fileName;
  link.click();
  }
}
