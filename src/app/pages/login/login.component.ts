import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { BusquedaService } from 'src/app/service/busqueda.service';
import { CambioRegulatorioService } from 'src/app/service/cambio-regulatorio.service';
import { CumplimientoService } from 'src/app/service/cumplimiento.service';
import { ExportacionService } from 'src/app/service/exportacion.service';
import { LineaBaseService } from 'src/app/service/lineabase.service';
import { UsuarioService } from 'src/app/service/usuario.service';
import { Usuario } from 'src/app/shared/dto/Usuario';
import {Md5} from 'ts-md5/dist/md5';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  public nombreUsuario:string;
  public contrasena:string;
  public usuario:Usuario=new Usuario();
  
  constructor(private usuarioService:UsuarioService,
    private router: Router) { }

  ngOnInit() {

    const md5 = new Md5();
console.log(md5.appendStr('Steven1').end());
  }
  iniciarSesion(){
    console.log(this.nombreUsuario)
    console.log(this.contrasena)
    this.usuario.nombreUsuario=this.nombreUsuario;
    this.usuario.contrasena=this.contrasena;
    this.usuario.estado="ACTIVO";
    this.usuarioService.getAutenticar(this.usuario).subscribe(data => {
      if(data.status==this.usuarioService.codOK){

        this.usuarioService.usuario=data.response;
        this.usuarioService.agregarToken();
        this.usuarioService.usuario.contrasena=this.contrasena;
        console.log(this.usuarioService.usuario);
        console.log(this.usuarioService.httpOptions);
        this.router.navigate(['/search/history']);
      }else{
       this.usuarioService.getManejoError(data);
      }
      })
  }

  
}
