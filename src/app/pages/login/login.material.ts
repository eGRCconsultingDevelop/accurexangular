import {
  MatCardModule,
  MatButtonModule,
  MatCheckboxModule
} from '@angular/material';

export const LoginMaterialModules = [
  MatCardModule,
  MatButtonModule,
  MatCheckboxModule
];
