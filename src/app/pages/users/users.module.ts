import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MomentModule } from 'ngx-moment';

import { UsersRoutingModule } from './users-routing.module';

import { ListComponent } from './list/list.component';
import { FormComponent } from './form/form.component';
import { SharedModule } from '../../shared/shared.module';

import { UsersMaterialModules } from './users.material';
import { FormsModule } from '@angular/forms';

@NgModule({
  declarations: [ListComponent, FormComponent],
  imports: [
    CommonModule,
    UsersRoutingModule,
    SharedModule,
    MomentModule,
    FormsModule,
    ...UsersMaterialModules
  ]
})
export class UsersModule { }
