import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator, MatTableDataSource } from '@angular/material';
import { Router } from '@angular/router';
import { UsuarioService } from 'src/app/service/usuario.service';
import { Usuario } from 'src/app/shared/dto/Usuario';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss']
})
export class ListComponent implements OnInit {
  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;

  displayedColumns = ['username', 'firstname', 'lastname', 'rol', 'status', 'delete'];

  dataSource = new MatTableDataSource<any>([]);

  public switchOp = 'recents';
  public usuarios:Array <Usuario>;
  constructor(private usuarioService:UsuarioService, public router:Router) { }

  ngOnInit() {

    this.consultarUsuarios();
 

    this.dataSource.paginator = this.paginator;
  }


    consultarUsuarios(){
      this.usuarioService.getConsultarUsuariosCliente(this.usuarioService.usuario.nombreUsuario,this.usuarioService.usuario.cliente.idCliente).subscribe(data => {
        if(data.status==this.usuarioService.codOK){

        console.log(data.response);
        this.usuarios=data.response;
        
       
         for(let i=0;i<this.usuarios.length;i++){
          if(this.usuarios[i].nombreUsuario==this.usuarioService.usuario.nombreUsuario){
            this.usuarios.splice( i, 1 );
        
          }
         }
         console.log(this.usuarios);
      this.datosTabla(); 
      this.dataSource.paginator = this.paginator;
        }else{
        this.usuarioService.getManejoError(data);
        }
        })
    }

    editarUsuario(usuarioEditar:Usuario){
      console.log(usuarioEditar);
      this.usuarioService.opcion="Editar";
      this.usuarioService.usuarioEditar=usuarioEditar;
      console.log(this.usuarioService.usuarioEditar);
      this.router.navigate(['/users/edit'])
    }

    datosTabla(){
      console.log("arreglo actualizado"+this.usuarios);
   
        this.dataSource = new MatTableDataSource(this.usuarios);  
        console.log(this.usuarios);
  
    }

    inactivarUsuario(nombreUsuario:string){
      let indicadorUsiario=0
      for(let i=0;i < this.usuarios.length;i++){
        if(nombreUsuario == this.usuarios[i].nombreUsuario){
          indicadorUsiario=i;
        }
      }
      this.usuarios[indicadorUsiario].estado="INACTIVO";

      this.usuarioService.getInactivarUsuario(this.usuarioService.usuario.nombreUsuario,this.usuarios[indicadorUsiario].nombreUsuario,this.usuarioService.usuario.cliente.idCliente).subscribe(data => {
        if(data.status==this.usuarioService.codOK){

        console.log(data.response);
        }else{
        this.usuarioService.getManejoError(data);
        }
        })
      
    
    }
    activarUsuario(nombreUsuario:string){
   

      let indicadorUsiario=0
      for(let i=0;i < this.usuarios.length;i++){
        if(nombreUsuario == this.usuarios[i].nombreUsuario){
          indicadorUsiario=i;
        }
      }
      this.usuarios[indicadorUsiario].estado="ACTIVO";

      this.usuarioService.getActivarUsuario(this.usuarioService.usuario.nombreUsuario,this.usuarios[indicadorUsiario].nombreUsuario,this.usuarioService.usuario.cliente.idCliente).subscribe(data => {
        if(data.status==this.usuarioService.codOK){

        console.log(data.response);
        }else{
        this.usuarioService.getManejoError(data);
        }
        })
    }


    crearUsuario(){
      this.usuarioService.usuarioEditar=undefined;
      this.usuarioService.opcion="Crear";
      console.log(this.usuarioService.usuarioEditar);
      this.router.navigate(['/users/create'])
    }

      eliminar(usuario:Usuario){
        console.log(usuario);

     
      this.usuarioService.getBorrarUsuario(this.usuarioService.usuario.nombreUsuario,usuario.nombreUsuario).subscribe(data => {
        if(data.status==this.usuarioService.codOK){
          let i=this.usuarios.indexOf(usuario);
          this.usuarios.splice(i);
          this.ngOnInit();
        console.log(data.response);
        }else{
        this.usuarioService.getManejoError(data);
        }
        })
      }
  }
