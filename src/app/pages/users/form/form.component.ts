import { THIS_EXPR } from '@angular/compiler/src/output/output_ast';
import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material';
import { Router } from '@angular/router';
import { UsuarioService } from 'src/app/service/usuario.service';
import { ModalValidadorComponent } from 'src/app/shared/components/modal-validador/modal-validador.component';
import { Usuario } from 'src/app/shared/dto/Usuario';

@Component({
  selector: 'app-form',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.scss']
})
export class FormComponent implements OnInit {

  public usuario:Usuario=new Usuario();
  public nombreUsuario:string="";
  constructor(private usuarioService:UsuarioService,
    private router:Router,
    public dialog: MatDialog) { }

  ngOnInit() {
    console.log(this.usuarioService.usuarioEditar);
    if(this.usuarioService.usuarioEditar != undefined){
      this.usuario=this.usuarioService.usuarioEditar;
    }
    console.log(this.usuarioService.opcion);
  }

    crearUsuario(){
      this.usuario.idRol=3;
      this.usuario.estado="ACTIVO";
      
      console.log(this.usuario);
     this.usuarioService.getiIncluirUsuario(this.usuarioService.usuario.nombreUsuario,this.usuarioService.usuario.cliente.idCliente,this.usuario).subscribe(data => {
        if(data.status==this.usuarioService.codOK){
          const dialogRef = this.dialog.open(ModalValidadorComponent, {
            width: '500px',
            data: {
              body1:"Se creo el usuario satisfactoriamente",
              title:"Creación usuario",
              acceptButton:"Aceptar"
            }
          });
        console.log(data.response);
        this.router.navigate(['/users/list'])
        }else{
        this.usuarioService.getManejoError(data);
        if(data.message =="El cliente ya tiene creados todos los usuarios contratados"){
          const dialogRef = this.dialog.open(ModalValidadorComponent, {
            width: '500px',
            data: {
              body1:"El cliente ya tiene creados todos los usuarios contratados",
              title:"Creación usuario",
              acceptButton:"Aceptar"
            }
          });
        }
        }
        })
        
    }


     editarUsuario(){
       console.log(this.usuario);
      this.usuarioService.getMoficarUsuario(this.usuarioService.usuario.nombreUsuario,this.usuario.nombreUsuario,this.usuarioService.usuario.cliente.idCliente,this.usuario).subscribe(data => {
        if(data.status==this.usuarioService.codOK){
          const dialogRef = this.dialog.open(ModalValidadorComponent, {
            width: '500px',
            data: {
              body1:"Se edito el usuario satisfactoriamente",
              title:"Edición usuario",
              acceptButton:"Aceptar"
            }
          });
        console.log(data.response);
        this.router.navigate(['/users/list'])
        }else{
        this.usuarioService.getManejoError(data);
        }
        })
     }
    cancelar (){
      this.usuarioService.usuarioEditar=undefined;
      this.router.navigate(['/users/list'])
    }
}
