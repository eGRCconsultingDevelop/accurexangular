import {
  MatCardModule,
  MatTableModule,
  MatButtonModule,
  MatIconModule,
  MatGridListModule,
  MatSelectModule,
  MatSliderModule
} from '@angular/material';

export const UsersMaterialModules = [
  MatCardModule,
  MatTableModule,
  MatButtonModule,
  MatIconModule,
  MatGridListModule,
  MatSelectModule,
  MatSliderModule
];
