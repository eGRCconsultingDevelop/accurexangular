import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {MatInputModule} from '@angular/material/input'; 
import { FormsModule } from '@angular/forms';
import { MomentModule } from 'ngx-moment';
import {MatAutocompleteModule} from '@angular/material/autocomplete';

import { SearchRoutingModule } from './search-routing.module';
import { SearchComponent } from './search.component';
import { SharedModule } from 'src/app/shared/shared.module';
import { SearchMaterialModules } from './search.material';
import { HistoryComponent } from './history/history.component';
import { DetailComponent } from './detail/detail.component';
import { ConsultaComponent } from './consulta/consulta.component';
import { ConsultaDetailComponent } from './consulta-detail/consulta-detail.component';


@NgModule({
  declarations: [
    SearchComponent,
    HistoryComponent,
    DetailComponent,
    ConsultaComponent,
    ConsultaDetailComponent
  ],
  imports: [
    CommonModule,
    SearchRoutingModule,
    SharedModule,
    MomentModule,
    MatInputModule,
    FormsModule,
    MatAutocompleteModule,
    ...SearchMaterialModules
  ]
})
export class SearchModule { }
