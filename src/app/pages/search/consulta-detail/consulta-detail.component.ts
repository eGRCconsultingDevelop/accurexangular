import { Component, OnInit, ViewChild } from '@angular/core';
import { NestedTreeControl } from '@angular/cdk/tree';
import { MatTreeNestedDataSource } from '@angular/material';
import { ActivatedRoute, Router } from '@angular/router';
import { BusquedaService } from 'src/app/service/busqueda.service';
import { Regulacion } from 'src/app/shared/dto/Regulacion';
import { Nivel } from 'src/app/shared/dto/Nivel';
import { ItemNode } from 'src/app/shared/dto/ItemNode';
import { ExportacionService } from 'src/app/service/exportacion.service';
import { UsuarioService } from 'src/app/service/usuario.service';


@Component({
  selector: 'app-consulta-detail',
  templateUrl: './consulta-detail.component.html',
  styleUrls: ['./consulta-detail.component.scss']
})

export class ConsultaDetailComponent implements OnInit {

  public busqueda:string='';
  public collapsed = true;
  public filtersCollapsed = false;
  public expandedTree=false;
  public collapsedTree =false;
  public regulacion : Regulacion = this.busquedaService.regulacion;
  treeControl = new NestedTreeControl<any>(node => node.children);
  dataSource = new MatTreeNestedDataSource<any>();
  public mockItem: any ;
  public arbol:Array<ItemNode>=[];
  public mostrarReq=false;
  public palabraClave:String;
  public opcion: string;

  
  constructor(private rutaActiva: ActivatedRoute, 
    private usuarioService:UsuarioService,
     private busquedaService : BusquedaService,
     private exportacionService:ExportacionService) {

    var fechaDerogado = this.regulacion.fechaDerogado.split("/", 3); 
  
    var fechaVigencia = this.regulacion.fechaVigencia.split("/", 3); 

    if(this.regulacion.estado == "DEROGADO"){
    this.mockItem ={
      id: this.regulacion.id,
      title: this.regulacion.fuenteAutoridad,
      check_title: this.regulacion.nombreRegulacion,
      info: [
        {
          name: 'Riesgos asociados',
          value: this.regulacion.riesgosAsociados,
        },
        {
          name: 'Multas asociadas',
          value: this.regulacion.multasAsociadas,
        },
        {
          name: 'Areas Responsables',
          value:  this.regulacion.areasResponsables,
        },
        {
          name: 'Areas Involucradas',
          value: this.regulacion.areasInvolucradas,
        },
      ],
      validity: {
        state: this.regulacion.estado,
        date: {
          day: fechaDerogado[1],
            month: fechaDerogado[0],
            year: fechaDerogado[2] 
        }
      },
      resume: this.regulacion.abstractReg,
      sectors: [this.regulacion.sector],
      jurisdictions: [this.regulacion.jurisdiccion],
      impacts: [this.regulacion.dominiosImpacto]
    };
  }else{
    this.mockItem ={
      id: this.regulacion.id,
      title: this.regulacion.fuenteAutoridad,
      check_title: this.regulacion.nombreRegulacion,
      info: [
        {
          name: 'Riesgos asociados',
          value: this.regulacion.riesgosAsociados,
        },
        {
          name: 'Multas asociadas',
          value: this.regulacion.multasAsociadas,
        },
        {
          name: 'Areas Responsables',
          value:  this.regulacion.areasResponsables,
        },
        {
          name: 'Areas Involucradas',
          value: this.regulacion.areasInvolucradas,
        },
      ],
      validity: {
        state: this.regulacion.estado,
        date: {
          day: fechaVigencia[1],
            month: fechaVigencia[0],
            year: fechaVigencia[2] 
        }
      },
      resume: this.regulacion.abstractReg,
      sectors: [this.regulacion.sector],
      jurisdictions: [this.regulacion.jurisdiccion],
      impacts: [this.regulacion.dominiosImpacto]
    };
    this.construirArbol('');

    
  }
    console.log(this.rutaActiva.snapshot.params["id"]);
  }

  hasChild = (_: number, node: any) => !!node.children && node.children.length > 0;
  isReq = (_: number, node: any) => node.type=='R';

  ngOnInit() {
    
      this.treeControl.dataNodes=this.dataSource.data;
      this.treeControl.collapseAll();
      for(let node of this.treeControl.dataNodes){
      this.treeControl.expand(node);  
      
    }
  }

  tieneHijos(node){
    return !!node.children && node.children.length > 0;
  }


  opcionSeleccionada(event){
    if(this.opcion == "Expandir"){
      this.collapsedTree=false;
      this.expandedTree=true;
      this.treeControl.dataNodes=this.dataSource.data;
      this.treeControl.expandAll();
      console.log(event.checked);
    }else if(this.opcion =="Expandir Nivel 1"){
      this.treeControl.dataNodes=this.dataSource.data;
      this.treeControl.collapseAll();
      for(let node of this.treeControl.dataNodes){
      this.treeControl.expand(node);  
      }
    }else if(this.opcion =="Contraer"){
      this.collapsedTree=true;
      this.expandedTree=false;
      this.treeControl.collapseAll();
      console.log(event.checked);
    }
  }

  evaluarIsExpanded(node:any){
    if(node.cargaInicial){
      node.cargaInicial=false;
      if(node.coincidencias){
        this.treeControl.expand(node);
        return true;
      }
      if(!this.treeControl.isExpanded(node)){
        if(this.evaluarHijosCoincidentes(node)){
          this.treeControl.expand(node);
          return true;
        }
      }
    } 
    return this.treeControl.isExpanded(node);
  }

  evaluarHijosCoincidentes(node:any){
    if(this.tieneHijos(node)){
      console.log("HIJOS!!!"+node.children);
      for(let child of node.children){
        if(child.coincidencias){
          return true;
        }else{
          this.evaluarHijosCoincidentes(child);
        }
      }
    }
    return false;
  }

    construirArbol(busqueda:string) {
		//defino la variable arbol
    let nodo: ItemNode= new ItemNode();
    let nivelesSuperiores:Array<Nivel>=[];
    this.arbol=[];
		//saco los niveles que NO tienen padre, es decir que el idTextoPadre sea nulo o vacio
    for(let i=0;i< this.regulacion.niveles.length ; i++){
      if(this.regulacion.niveles[i].nivelSuperior == null){
        nivelesSuperiores.push(this.regulacion.niveles[i]);
      }
    }
		//instancio el arbol
		//inicio la adi cion de hijos, le envio el arbol entero, los niveles padres, y TODOS los niveles existentes (padres e hijos)
    this.busqueda=busqueda;
    this.addRamaArbol(nodo, nivelesSuperiores, this.regulacion.niveles, busqueda);
    this.dataSource.data=this.arbol;
	}
	
  addRamaArbol(nodo:ItemNode,hijos:Array<Nivel>,nivelesEstructura:Array<Nivel>, busqueda:string) {
    let nivelesSuperioresAux: Array<Nivel>=[];

    let nivel2:Nivel;
      for(let nivel of hijos) {
        for(let nivel2 of nivelesEstructura) {
          if(nivel2.idTextoPadre !=null){
            if(nivel.idTexto==nivel2.idTextoPadre) {
              nivelesSuperioresAux.push(nivel2);
            }
          }
        }
    
        let nodoHijo: ItemNode= new ItemNode();
        let reqs: Array<ItemNode>= [];
        let coincidencias:boolean=false;

        if(busqueda!='' && (nivel.texto.toUpperCase().includes(busqueda.toUpperCase()) 
        || nivel.textoAsociado.toUpperCase().includes(busqueda.toUpperCase()))){
          coincidencias=true;
        }

        if(nivel.requerimientos.length>0){
          for(let i=0;i< nivel.requerimientos.length ; i++){
            let nodoReq: ItemNode= new ItemNode();
            nodoReq.id=nivel.requerimientos[i].idRequerimiento;
            nodoReq.idTextoPadre=nivel.idTexto;
            nodoReq.texto=nivel.requerimientos[i].codigoRequerimiento;
            nodoReq.textoAsociado=nivel.requerimientos[i].requerimiento;
            nodoReq.type='R';
            nodoReq.tieneRequerimientos=nivel.tieneRequerimientos;
            nodoReq.coincidencias=false;
            nodoReq.cargaInicial=false;
            reqs.push(nodoReq);
          }
        }
        if(nivel.idTextoPadre==null){
          nodo= new ItemNode();
          nodo.id=nivel.idTexto;
          nodo.texto=nivel.texto;
          nodo.idTextoPadre=nivel.idTextoPadre;
          nodo.textoAsociado=nivel.textoAsociado;
          nodo.type='N';
          nodo.tieneRequerimientos=nivel.tieneRequerimientos;
          nodo.children=reqs;
          nodo.coincidencias=coincidencias;
          nodo.cargaInicial=true;
          nodoHijo=nodo;
          this.arbol.push(nodo);
        }else{
          nodoHijo.type='N';
          nodoHijo.id=nivel.idTexto;
          nodoHijo.texto=nivel.texto;
          nodoHijo.idTextoPadre=nivel.idTextoPadre;
          nodoHijo.textoAsociado=nivel.textoAsociado;
          nodoHijo.tieneRequerimientos=nivel.tieneRequerimientos;
          nodoHijo.children=reqs;
          nodoHijo.coincidencias=coincidencias;
          nodoHijo.cargaInicial=true;
          nodo.children.push(nodoHijo);
        }
        //llamo nuevamente a este metodo agregando un nodo nuevo, y los nuevos hijos q ahora son padres, y TODOS los niveles nuevamente
        this.addRamaArbol(nodoHijo, nivelesSuperioresAux, nivelesEstructura, busqueda);
        nivelesSuperioresAux=[];      
      }
  }
  
  exportarPdf(){
   
    this.exportacionService.getGenerarArchivoBusqueda(this.usuarioService.usuario.nombreUsuario,this.regulacion.id,"ACTIVO","0").subscribe(data => {
      if(data.status==this.exportacionService.codOK){
        var sampleArr = this.base64ToArrayBuffer(data.response);
        this.saveByteArray(this.regulacion.nombreRegulacion, sampleArr);
       
      }else{
      
       this.exportacionService.getManejoError(data);
      }
      })
  }


  base64ToArrayBuffer(base64) {
    var binaryString = window.atob(base64);
    var binaryLen = binaryString.length;
    var bytes = new Uint8Array(binaryLen);
    for (var i = 0; i < binaryLen; i++) {
       var ascii = binaryString.charCodeAt(i);
       bytes[i] = ascii;
    }
    return bytes;
 }

  saveByteArray(reportName, byte) {
  var blob = new Blob([byte], {type: "application/pdf"});
  var link = document.createElement('a');
  link.href = window.URL.createObjectURL(blob);
  var fileName = reportName;
  link.download = fileName;
  link.click();
}
}
