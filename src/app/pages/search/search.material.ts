import {
  MatExpansionModule,
  MatCheckboxModule,
  MatPaginatorModule,
  MatButtonModule,
  MatCardModule,
  MatTableModule,
  MatDialogModule,
  MatIconModule,
  MatTreeModule,
  MatMenuModule,
  MatTooltipModule,
  MatDatepickerModule,
  MatFormFieldModule,
  MatInputModule,
  MatProgressBarModule,
  MatRadioModule
} from '@angular/material';

export const SearchMaterialModules = [
  MatExpansionModule,
  MatCheckboxModule,
  MatPaginatorModule,
  MatButtonModule,
  MatCardModule,
  MatTableModule,
  MatDialogModule,
  MatIconModule,
  MatTreeModule,
  MatMenuModule,
  MatTooltipModule,
  MatDatepickerModule,
  MatFormFieldModule,
  MatInputModule,
  MatProgressBarModule,
  MatRadioModule
];
