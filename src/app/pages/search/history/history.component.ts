import { Component, OnInit, ViewChild, OnDestroy } from '@angular/core';
import { MatTableDataSource, MatPaginator, MatDialog } from '@angular/material';
import { BusquedaService } from 'src/app/service/busqueda.service';
import { ModalComponent } from '../../../shared/components/modal/modal.component';
import { BusquedaUsuario } from 'src/app/shared/dto/BusquedaUsuario';
import { Router } from '@angular/router';
import * as moment from 'moment';
import { ModalConfirmacionComponent } from 'src/app/shared/components/modal-confirmacion/modal-confirmacion.component';
import { UsuarioService } from 'src/app/service/usuario.service';

@Component({
  selector: 'app-history',
  templateUrl: './history.component.html',
  styleUrls: ['./history.component.scss']
})
export class HistoryComponent implements OnInit {
  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  public switchHistory = 'recents';
  public carga:boolean=true;
  public busquedasUsuarioGuardadas: Array<BusquedaUsuario>=[];
  public busquedasUsuarioCreadas: Array<BusquedaUsuario>=[];
  public nombreBusquedaEliminar:String;
  public busquedaEliminar:BusquedaUsuario;
  public fechaCreacion:string;
  displayedColumns = ['name', 'description', 'date_entered', 'date_modified', 'id'];
  public busquedaCreada:BusquedaUsuario;
  dataSource: MatTableDataSource<BusquedaUsuario>=null;

  constructor(
    public dialog: MatDialog,
    private busquedaService: BusquedaService,
    private usuarioService:UsuarioService,
    private router: Router
  ) { 

 
  }

  ngOnInit() {

    this.consultarBusquedasGuardadas();
    this.consultarBusquedasCreadas();
   
    
  }

 /* ngOnDestroy() {
    this.showDialog();
  }*/

 /* showDialog() {
    const dialogRef = this.dialog.open(ModalComponent, {
      width: '500px',
      data: {
        title: 'Confirmación',
        body: `Existe una búsqueda con fecha <span class="text-primary-green">23 de Mayo de 2019</span> sin guardar. ¿Desea complementarla?`,
        cancelButton: 'Descartar',
        acceptButton: 'Continuar'
      }
    });
    
  }*/

  ventanaContinuar(busquedaCreada:BusquedaUsuario,idBusqueda:number,fechaBusqueda:String) {
    let respuestaDialogo='';
    const dialogRef = this.dialog.open(ModalConfirmacionComponent, {
      width: '500px',
      data: {
        title: 'Confirmación',
        body1: `Existe una búsqueda sin guardar con fecha`,
        body2:  '¿Desea Finalizarla?',
        cancelButton: 'Cancelar',
        acceptButton: 'Confirmar',
        id:idBusqueda,
        fecha:fechaBusqueda,
        busqueda:busquedaCreada
        
      }
    });

    dialogRef.afterClosed().subscribe(
      data =>{ if(data.reason=='cancel'){
        let i=this.busquedasUsuarioCreadas.indexOf(this.busquedaEliminar);
        this.busquedasUsuarioCreadas.splice(i);
        this.ngOnInit();
      };});  
    
    }
  
  ventanaConfirmacion(idBusqueda:number,nombreBusqueda:String) {
    let respuestaDialogo='';
    const dialogRef = this.dialog.open(ModalComponent, {
      width: '500px',
      data: {
        title: 'Confirmación',
        body1: `Esta opción elimina la búsqueda`,
        body2:  'Por favor confirmar esta acción',
        cancelButton: 'Cancelar',
        acceptButton: 'Confirmar',
        id:idBusqueda,
        busqueda:`"`+nombreBusqueda+`"`,
        tipo:"Busqueda"
      }
    });

    dialogRef.afterClosed().subscribe(
      data =>{ if(data.reason=='accept'){
        let i=this.busquedasUsuarioGuardadas.indexOf(this.busquedaEliminar);
        this.busquedasUsuarioGuardadas.splice(i);
        this.ngOnInit();
      };});  
    
    }

  datosTabla(){
    console.log("arreglo actualizado"+this.busquedasUsuarioGuardadas);
 
      this.dataSource = new MatTableDataSource(this.busquedasUsuarioGuardadas);  
      console.log(this.busquedasUsuarioGuardadas);

  }

  eliminar(id:number){
    for(let i=0;i < this.busquedasUsuarioGuardadas.length;i++){
      if(this.busquedasUsuarioGuardadas[i].idBusqueda == id){
        this.nombreBusquedaEliminar=this.busquedasUsuarioGuardadas[i].nombreBusqueda;
        this.busquedaEliminar=this.busquedasUsuarioGuardadas[i];
      }

      
    }

    this.ventanaConfirmacion(id,this.nombreBusquedaEliminar);
    }

  consultarBusquedasGuardadas(){
 
    this.busquedaService.getBusquedasEstado(this.usuarioService.usuario.nombreUsuario,"GUARDADA").subscribe(data => {
      if(data.status==this.busquedaService.codOK){
      this.busquedasUsuarioGuardadas=data.response;
      console.log(this.busquedasUsuarioGuardadas);
      this.datosTabla(); 
      this.dataSource.paginator = this.paginator;
      this.carga=false;
     // this.showDialog();
      }else{
      this.busquedaService.getManejoError(data);
      }
      })
  }

  consultarBusquedasCreadas(){
    
    this.busquedaService.getBusquedasEstado(this.usuarioService.usuario.nombreUsuario,"CREADA").subscribe(data => {
      if(data.status==this.busquedaService.codOK){
      this.busquedasUsuarioCreadas=data.response;
      console.log(this.busquedasUsuarioCreadas);
      if(this.busquedasUsuarioCreadas.length >0){

        this.busquedaCreada= this.busquedasUsuarioCreadas[0];
        this.fechaCreacion=this.busquedasUsuarioCreadas[0].fechaCreacion;
      
          console.log(this.fechaCreacion);
        
        this.ventanaContinuar(this.busquedaCreada,this.busquedasUsuarioCreadas[0].idBusqueda,this.fechaCreacion);
      
      }
        
     // this.showDialog();
      }else{
      this.busquedaService.getManejoError(data);
      }
      })
  }
  editarBusqueda(element){
    console.log(element);
    this.busquedaService.getBusquedaUsuario(this.usuarioService.usuario.nombreUsuario,element.idBusqueda,"ACTIVO").subscribe(data => {
      if(data.status==this.busquedaService.codOK){
      this.busquedaService.resultadoBusqueda=data.response;
      console.log(this.busquedaService.resultadoBusqueda);
      this.router.navigate(['/search']);
      }else{
      this.busquedaService.getManejoError(data);
      }
      })
 
  }

  consultarBusqueda(element){
    console.log(element);
    this.busquedaService.getBusquedaUsuario(this.usuarioService.usuario.nombreUsuario,element.idBusqueda,"ACTIVO").subscribe(data => {
      if(data.status==this.busquedaService.codOK){
      this.busquedaService.resultadoBusqueda=data.response;
      console.log(this.busquedaService.resultadoBusqueda);
      this.router.navigate(['/search/consulta']);
      }else{
      this.busquedaService.getManejoError(data);
      }
      })
 
  }
}
