import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material';

import { ModalSaveSearchComponent } from '../../shared/components/modal-save-search/modal-save-search.component';
import { BusquedaService } from 'src/app/service/busqueda.service';
import { FiltrosDetalleBusqueda } from 'src/app/shared/dto/FiltrosDetalleBusqueda';

import { Regulacion } from 'src/app/shared/dto/Regulacion';
import { FiltrosBusqueda } from 'src/app/shared/dto/FiltrosBusqueda';
import { ResultadoBusqueda } from 'src/app/shared/dto/ResultadoBusqueda';
import {Observable} from 'rxjs';
import {map, startWith} from 'rxjs/operators';
import {FormControl} from '@angular/forms';
import * as moment from 'moment';

import { UsuarioService } from 'src/app/service/usuario.service';
@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.scss']
})
export class SearchComponent implements OnInit {

  public filtersCollapsed = false;
  public filtrosDetalle : FiltrosDetalleBusqueda;
  public filtrosBusqueda : FiltrosBusqueda;
  public regulacion : Array <Regulacion> = this.busquedaService.resultadoBusqueda.regulacionesDTO;
  public cantidadRegulaciones: number= this.regulacion.length;
  public cantidadRegulacionesSelec: number =0;
  public mockItem: any ;
  public resultadoBusqueda: ResultadoBusqueda = this.busquedaService.resultadoBusqueda;
  public seleccionado : boolean;
  public arregloMockItem: Array<any>; 
  public fechaHasta: string;
  public fechaDesde: string;
  public options: string[] = [];
  public filteredOptions: Observable<string[]>;
  public myControl = new FormControl();
  public habilitarCard:boolean;
  constructor(
    public dialog: MatDialog,
    private busquedaService: BusquedaService,
    private usuarioService: UsuarioService
  ) {
    this.filtrosDetalle= this.busquedaService.resultadoBusqueda.filtrosDetalleDTO
    this.cantidadRegulacionesSelec=this.busquedaService.resultadoBusqueda.filtrosDetalleDTO.idRegulacionesSel.length;
    console.log(this.busquedaService.dominiosImpacto);
 
   }

  ngOnInit() {
    this.arregloMockItem= [];
    for(let i=0; i < this.regulacion.length;i++){
     console.log(this.regulacion[i]);
      var fechaDerogado = this.regulacion[i].fechaDerogado.split("/", 3); 
      console.log( this.regulacion[i].multasAsociadas);
      var fechaVigencia = this.regulacion[i].fechaVigencia.split("/", 3); 
     
      if(this.regulacion[i].estado == "DEROGADO"){
      this.mockItem = {
        habilitar: this.regulacion[i].contratado,
        id: this.regulacion[i].id,
        title: this.regulacion[i].fuenteAutoridad,
        title2:this.regulacion[i].nombreRegulacionNivelII,
        check_title: this.regulacion[i].nombreRegulacion,
        info: [
          { 
            name: 'Riesgos asociados',
            value: this.regulacion[i].riesgosAsociados,
          },
          {
            name: 'Multas asociadas',
            value: this.regulacion[i].multasAsociadas,
          },
          {
            name: 'Areas Responsables',
            value: this.regulacion[i].areasResponsables,
          },
          {
            name: 'Areas Involucradas',
            value: this.regulacion[i].areasInvolucradas,
          },
         
        ],
        validity: {
          state: this.regulacion[i].estado,
          date: {
            day: fechaDerogado[1],
            month: fechaDerogado[0],
            year: fechaDerogado[2]
          }
        },
        url:this.regulacion[i].url,
        resume: this.regulacion[i].abstractReg,
        sectors: [this.regulacion[i].sector],
        jurisdictions: [this.regulacion[i].jurisdiccion],
        impacts: [this.regulacion[i].dominiosImpacto]
      };
      this.arregloMockItem.push(this.mockItem);
    }else{
      this.mockItem = {
        habilitar: this.regulacion[i].contratado,
        id: this.regulacion[i].id,
        title: this.regulacion[i].fuenteAutoridad,
        title2:this.regulacion[i].nombreRegulacionNivelII,
        check_title: this.regulacion[i].nombreRegulacion,
        info: [
          { 
            name: 'Riesgos asociados',
            value: this.regulacion[i].riesgosAsociados,
          },
          {
            name: 'Multas asociadas',
            value: this.regulacion[i].multasAsociadas,
          },
          {
            name: 'Areas Responsables',
            value: this.regulacion[i].areasResponsables,
          },
          {
            name: 'Areas Involucradas',
            value: this.regulacion[i].areasInvolucradas,
          },
        
        ],
        validity: {
          state: this.regulacion[i].estado,
          date: {
            day: fechaVigencia[1],
            month: fechaVigencia[0],
            year: fechaVigencia[2]
          }
        },
        url:this.regulacion[i].url,
        resume: this.regulacion[i].abstractReg,
        sectors: [this.regulacion[i].sector],
        jurisdictions: [this.regulacion[i].jurisdiccion],
        impacts: [this.regulacion[i].dominiosImpacto]
      };
      this.arregloMockItem.push(this.mockItem);
    }
    
    }
    this.filteredOptions = this.myControl.valueChanges
    .pipe(
      startWith(''),
      map(value => this._filter(value))
    );


  }
  private _filter(value: string): string[] {
    const filterValue = value.toLowerCase();

    return this.options.filter(palabraClave => palabraClave.toLowerCase().includes(filterValue));
  }

  consultarTags(valor){
   
    }

  public onCheckChange(event): void {
   
    console.log(event);
    console.log(event.item.id);
    console.log(event.check_state.checked);

    if(event.check_state.checked == true){
      this.cantidadRegulacionesSelec=this.cantidadRegulacionesSelec+1;
      this.busquedaService.resultadoBusqueda.filtrosDetalleDTO.idRegulacionesSel.push(event.item.id);
      console.log(this.busquedaService.resultadoBusqueda.filtrosDetalleDTO.idRegulacionesSel);
    }else{
      this.cantidadRegulacionesSelec=this.cantidadRegulacionesSelec-1;
      var index=this.busquedaService.resultadoBusqueda.filtrosDetalleDTO.idRegulacionesSel.indexOf(event.item.id)
      if ( index !== -1 ) {
        this.busquedaService.resultadoBusqueda.filtrosDetalleDTO.idRegulacionesSel.splice( index, 1 );
      }
      console.log(this.busquedaService.resultadoBusqueda.filtrosDetalleDTO.idRegulacionesSel);
    }
 
   
  }

  saveSearch() {
    const dialogRef = this.dialog.open(ModalSaveSearchComponent, {
      width: '500px',
      data: {

      }
    });
  }
  agregarRiesgo(event,id){
    console.log(event);
    if(event.checked){
    this.filtrosDetalle.idRiesgosAsociadosSel.push(id);
    console.log(this.filtrosDetalle.idRiesgosAsociadosSel);
    }else{
      var index=this.filtrosDetalle.idRiesgosAsociadosSel.indexOf(id);
      if ( index !== -1 ) {
        this.filtrosDetalle.idRiesgosAsociadosSel.splice( index, 1 );
      }
      console.log(this.filtrosDetalle.idRiesgosAsociadosSel);
    }
  }

  agregarAreaResponsable(event,id){
    console.log(event);
    if(event.checked){
    this.filtrosDetalle.idAreasResponsablesSel.push(id);
    console.log(this.filtrosDetalle.idAreasResponsablesSel);
    }else{
      var index=this.filtrosDetalle.idAreasResponsablesSel.indexOf(id);
      if ( index !== -1 ) {
        this.filtrosDetalle.idAreasResponsablesSel.splice( index, 1 );
      }
      console.log(this.filtrosDetalle.idAreasResponsablesSel);
    }

  }

  agregarAreaInvolucrada(event,id){
    console.log(event);
    if(event.checked){
    this.filtrosDetalle.idAreasInvolucradasSel.push(id);
    console.log(this.filtrosDetalle.idAreasInvolucradasSel);
    }else{
      var index=this.filtrosDetalle.idAreasInvolucradasSel.indexOf(id);
      if ( index !== -1 ) {
        this.filtrosDetalle.idAreasInvolucradasSel.splice( index, 1 );
      }
      console.log(this.filtrosDetalle.idAreasInvolucradasSel);
    }
    
  }

  agregarEstadoReg(event,id){
    console.log(event);
    if(event.checked){
    this.filtrosDetalle.idEstadoRegulacionSel=id;
    console.log(this.filtrosDetalle.idEstadoRegulacionSel);
    }else{
      
     this.filtrosDetalle.idEstadoRegulacionSel=0;
      
      console.log(this.filtrosDetalle.idEstadoRegulacionSel);
    }
    
  }


  consultarFiltrosDetalle(event){
 

    if(moment(this.fechaDesde,['YYYYMMMDD']).format()!='Invalid date'){
      this.fechaDesde=moment(this.fechaDesde,['YYYYMMMDD']).format();
    }
    if(moment(this.fechaHasta,['YYYYMMMDD']).format()!='Invalid date'){
      this.fechaHasta=moment(this.fechaHasta,['YYYYMMMDD']).format();
    }
    console.log(this.fechaDesde);
    console.log(this.fechaHasta);
    console.log(this.filtrosDetalle);
    this.filtrosDetalle.fechaInicio=this.fechaDesde;
    this.filtrosDetalle.fechaFin=this.fechaHasta;
    this.busquedaService.getRegulacionesFiltroDetalle(this.usuarioService.usuario.nombreUsuario,this.filtrosDetalle,this.resultadoBusqueda.idBusqueda).subscribe(data => {
      if(data.status==this.busquedaService.codOK){
        this.busquedaService.resultadoBusqueda=data.response;
        console.log(this.busquedaService.resultadoBusqueda);
        this.regulacion=[];
        for(let i=0;i<this.busquedaService.resultadoBusqueda.regulacionesDTO.length;i++){
          this.regulacion.push(this.busquedaService.resultadoBusqueda.regulacionesDTO[i]);
        }
        console.log(this.regulacion);
        this.cantidadRegulaciones=this.regulacion.length;
        this.ngOnInit();
      }else{
       this.busquedaService.getManejoError(data);
      }
      })
  }
}
