import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';


import { SearchComponent } from './search.component';
import { ConsultaComponent } from './consulta/consulta.component';
import { HistoryComponent } from './history/history.component';
import { DetailComponent } from './detail/detail.component';
import { ConsultaDetailComponent } from './consulta-detail/consulta-detail.component';

const routes: Routes = [
  { path: '', component: SearchComponent },
  { path: 'history', component: HistoryComponent },
  { path: 'consulta', component: ConsultaComponent },
  { path: 'detail', component: DetailComponent },
  { path: 'consulta-detail', component: ConsultaDetailComponent }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SearchRoutingModule { }
