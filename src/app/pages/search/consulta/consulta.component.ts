import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material';

import { BusquedaService } from 'src/app/service/busqueda.service';
import { FiltrosDetalleBusqueda } from 'src/app/shared/dto/FiltrosDetalleBusqueda';
import { Busqueda } from 'src/app/shared/dto/Busqueda';
import { Regulacion } from 'src/app/shared/dto/Regulacion';
import { FiltrosBusqueda } from 'src/app/shared/dto/FiltrosBusqueda';
import { ResultadoBusqueda } from 'src/app/shared/dto/ResultadoBusqueda';
import {Observable} from 'rxjs';
import {map, startWith} from 'rxjs/operators';
import {FormControl} from '@angular/forms';
import * as moment from 'moment';
@Component({
  selector: 'app-consulta',
  templateUrl: './consulta.component.html',
  styleUrls: ['./consulta.component.scss']
})
export class ConsultaComponent implements OnInit {

  public filtersCollapsed = false;
  public filtrosDetalle : FiltrosDetalleBusqueda = this.busquedaService.resultadoBusqueda.filtrosDetalleDTO;
  public filtrosBusqueda : FiltrosBusqueda;
  public regulacion : Array <Regulacion> = [];
  public regulacionesTotal:Array <Regulacion>=this.busquedaService.resultadoBusqueda.regulacionesDTO;
  public cantidadRegulaciones: number= this.regulacion.length;
  public cantidadRegulacionesSelec: number =0;
  public mockItem: any ;
  public resultadoBusqueda: ResultadoBusqueda = this.busquedaService.resultadoBusqueda;
  public seleccionado : boolean;
  public arregloMockItem: Array<any>; 
  public fechaHasta: string;
  public fechaDesde: string;
  public options: string[] = [];
  public filteredOptions: Observable<string[]>;
  public myControl = new FormControl();

  constructor(
    public dialog: MatDialog,
    private busquedaService: BusquedaService
  ) {
    this.cantidadRegulacionesSelec=this.busquedaService.resultadoBusqueda.filtrosDetalleDTO.idRegulacionesSel.length;
    this.arregloMockItem= [];
   for(let i=0; i < this.regulacionesTotal.length;i++){
    if(this.regulacionesTotal[i].seleccionada == "S"){
      this.regulacion.push(this.regulacionesTotal[i]);
    }
   }
   this.cantidadRegulaciones=this.regulacion.length;
    for(let i=0; i < this.regulacion.length;i++){
     
      var fechaDerogado = this.regulacion[i].fechaDerogado.split("/", 3); 
      console.log( this.regulacion[i].multasAsociadas);
      var fechaVigencia = this.regulacion[i].fechaVigencia.split("/", 3); 
      console.log( this.regulacion[i].multasAsociadas);
      
      if(this.regulacion[i].estado == "DEROGADO"){
      this.mockItem = {
        id: this.regulacion[i].id,
        title: this.regulacion[i].fuenteAutoridad,
        check_title: this.regulacion[i].nombreRegulacion,
        info: [
          { 
            name: 'Riesgos asociados',
            value: this.regulacion[i].riesgosAsociados,
          },
          {
            name: 'Multas asociadas',
            value: this.regulacion[i].multasAsociadas,
          },
          {
            name: 'Areas Responsables',
            value: this.regulacion[i].areasResponsables,
          },
          {
            name: 'Areas Involucradas',
            value: this.regulacion[i].areasInvolucradas,
          },
        ],
        validity: {
          state: this.regulacion[i].estado,
          date: {
            day: fechaDerogado[1],
            month: fechaDerogado[0],
            year: fechaDerogado[2]
          }
        },
        url:this.regulacion[i].url,
        resume: this.regulacion[i].abstractReg,
        sectors: [this.regulacion[i].sector],
        jurisdictions: [this.regulacion[i].jurisdiccion],
        impacts: [this.regulacion[i].dominiosImpacto]
      };
      this.arregloMockItem.push(this.mockItem);
    }else{
      this.mockItem = {
        id: this.regulacion[i].id,
        title: this.regulacion[i].fuenteAutoridad,
        check_title: this.regulacion[i].nombreRegulacion,
        info: [
          { 
            name: 'Riesgos asociados',
            value: this.regulacion[i].riesgosAsociados,
          },
          {
            name: 'Multas asociadas',
            value: this.regulacion[i].multasAsociadas,
          },
          {
            name: 'Areas Responsables',
            value: this.regulacion[i].areasResponsables,
          },
          {
            name: 'Areas Involucradas',
            value: this.regulacion[i].areasInvolucradas,
          },
        ],
        validity: {
          state: this.regulacion[i].estado,
          date: {
            day: fechaVigencia[1],
            month: fechaVigencia[0],
            year: fechaVigencia[2]
          }
        },
        url:this.regulacion[i].url,
        resume: this.regulacion[i].abstractReg,
        sectors: [this.regulacion[i].sector],
        jurisdictions: [this.regulacion[i].jurisdiccion],
        impacts: [this.regulacion[i].dominiosImpacto]
      };
      this.arregloMockItem.push(this.mockItem);
    }
    
    }
   }

  ngOnInit() {
    this.filteredOptions = this.myControl.valueChanges
    .pipe(
      startWith(''),
      map(value => this._filter(value))
    );


  }
  private _filter(value: string): string[] {
    const filterValue = value.toLowerCase();

    return this.options.filter(palabraClave => palabraClave.toLowerCase().includes(filterValue));
  }

}
