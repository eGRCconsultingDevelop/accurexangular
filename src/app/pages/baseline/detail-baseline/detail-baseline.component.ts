import { Component, OnInit } from '@angular/core';
import { NestedTreeControl } from '@angular/cdk/tree';
import { MatTreeNestedDataSource, MatDialog } from '@angular/material';

import { ModalIngoBaselineComponent } from '../../../shared/components/modal-ingo-baseline/modal-ingo-baseline.component';
import { BusquedaService } from 'src/app/service/busqueda.service';
import { LineaBaseService } from 'src/app/service/lineabase.service';
import { ItemNode } from 'src/app/shared/dto/ItemNode';
import { Regulacion } from 'src/app/shared/dto/Regulacion';
import { Nivel } from 'src/app/shared/dto/Nivel';
import {SelectionModel} from '@angular/cdk/collections';
import { Router } from '@angular/router';
import { LineaBase } from 'src/app/shared/dto/LineaBase';
import { Requerimiento } from 'src/app/shared/dto/Requerimiento';
import { RequerimientoCliente } from 'src/app/shared/dto/RequerimientoCliente';
import { CumplimientoService } from 'src/app/service/cumplimiento.service';
import { ModalBaselineComponent } from 'src/app/shared/components/modal-baseline/modal-baseline.component';
import { ExportacionService } from 'src/app/service/exportacion.service';
import { UsuarioService } from 'src/app/service/usuario.service';


@Component({
  selector: 'app-detail-baseline',
  templateUrl: './detail-baseline.component.html',
  styleUrls: ['./detail-baseline.component.scss']
})
export class DetailBaselineComponent implements OnInit {
  public collapsed = true;
  public filtersCollapsed = false;
  public arbol:Array<ItemNode>=[];
  public expandedTree=false;
  public collapsedTree =false;
  public mostrarReq=false;
  listTreeControl : Array<NestedTreeControl<any>>=[];
  listDatasources: Array<MatTreeNestedDataSource<any>>=[];
  public opcion: string;
  public busqueda:string='';
  public regulaciones: Array<Regulacion>=[];
  public cont=0;
  public fuenteAutoridad:string;
  public nombreRegulacion:string;
  public palabraClave:string='';
  public repetida:boolean=false;
  public lineasBase:Array <LineaBase>;
  checklistSelection = new SelectionModel<ItemNode>(true /* multiple */);
  public requerimientos:Array<Requerimiento>;
  public requerimiento:RequerimientoCliente=new RequerimientoCliente;
  public requerimientosCliente:Array<RequerimientoCliente>=[];
  public idRequerimientos:Array <number>=[];
  public textosRequerimientos:Array <string>=[];
  public codigosRequerimiento:Array <string>=[];
  public nodosSeleccionados:Array<ItemNode>=[];
  

  constructor(
    private dialog: MatDialog,
    private busquedaService:BusquedaService,
    private lineaBaseService:LineaBaseService,
    private router: Router,
    private cumplimientoService:CumplimientoService,
    private usuarioService:UsuarioService,
    private exportacionService:ExportacionService
  ) {
    this.lineaBaseService.lineaBase.idTextos=[];
 
  }


  hasChild = (_: number, node: any) => !!node.children && node.children.length > 0;
  isReq = (_: number, node: any) => node.type=='R';

  tieneHijos(node){
    return !!node.children && node.children.length > 0;
  }

  ngOnInit() {

    
  
    this.consultarLineasBase();
    console.log(this.lineaBaseService.lineaBase.idBusquedas);
    console.log(this.lineaBaseService.lineaBase.regulaciones);
    if(this.lineaBaseService.lineaBase.idBusquedas == null){
      this.regulaciones=this.lineaBaseService.lineaBase.regulaciones
      for(let reg of this.regulaciones){
        this.construirArbol('',reg);
      }
    }else{
    for(let i=0;i<this.lineaBaseService.busquedasSelec.length;i++){
      this.consultarBusquedas(this.lineaBaseService.busquedasSelec[i].idBusqueda);

    }
  }
  for(let i=0;i<this.listDatasources.length;i++){   
    this.listTreeControl[i].dataNodes=this.listDatasources[i].data;
    this.listTreeControl[i].collapseAll();
    for(let node of this.listTreeControl[i].dataNodes){
    this.listTreeControl[i].expand(node);  
    }
  }
  }

  construirArbolPalabra(palabraClaveAux:string){
    this.palabraClave=palabraClaveAux;
    this.listTreeControl=[];
    this.listDatasources=[];
    for(let reg of this.regulaciones){
      this.construirArbol( this.palabraClave, reg);
    }
  }
  consultarBusquedas(id){
  this.busquedaService.getBusquedaUsuario(this.usuarioService.usuario.nombreUsuario,id,"ACTIVO").subscribe(data => {
    if(data.status==this.busquedaService.codOK){
    this.busquedaService.resultadoBusqueda=data.response;
    for(let i=0;i<this.busquedaService.resultadoBusqueda.regulacionesDTO.length;i++){

      if(this.busquedaService.resultadoBusqueda.regulacionesDTO[i].seleccionada == "S"){
        for(let j=0;j< this.regulaciones.length;j++){
          console.log(this.regulaciones[j]);
          console.log(this.busquedaService.resultadoBusqueda.regulacionesDTO[i]);
          if(this.regulaciones[j].id==this.busquedaService.resultadoBusqueda.regulacionesDTO[i].id){
            this.repetida=true;
       
          }
        }
        console.log( this.repetida);
        if(!this.repetida){
          console.log(this.busquedaService.resultadoBusqueda.regulacionesDTO[i]);
        this.regulaciones.push(this.busquedaService.resultadoBusqueda.regulacionesDTO[i]);
        console.log(this.regulaciones);
        this.lineaBaseService.lineaBase.regulaciones.push(this.busquedaService.resultadoBusqueda.regulacionesDTO[i]);
        }
        this.repetida=false;
  
      }
    }
    this.listTreeControl=[];
    this.listDatasources=[];
    for(let reg of this.regulaciones){
      this.construirArbol('',reg);
    }

    }else{
    this.busquedaService.getManejoError(data);
    }
    })
  }

  construirDS(id:number){
    for(let i=0;i<this.listDatasources.length;i++){
      if(this.listDatasources[i].data[0].idRegulacion==id){
        return this.listDatasources[i];
        break;
      }
    }
  }

  openInfoDialog(id) {
    this.busquedaService.regulacion=id;
    const dialogRef = this.dialog.open(ModalIngoBaselineComponent, {
      width: '900px'
    });
  }
  openInfoDialogGuardarTemporal(id) {
    this.busquedaService.regulacion=id;
    const dialogRef = this.dialog.open(ModalIngoBaselineComponent, {
      width: '900px'
    });
  }
  openInfoDialogGuardarDefinitivo(id) {
    this.busquedaService.regulacion=id;
    const dialogRef = this.dialog.open(ModalIngoBaselineComponent, {
      width: '900px'
    });
  }
  opcionSeleccionada(event){

    if(this.opcion == "Expandir"){
      for(let i=0;i<this.listDatasources.length;i++){
        this.listTreeControl[i].dataNodes=this.listDatasources[i].data;
        this.listTreeControl[i].expandAll();
      }
    }else if(this.opcion =="Expandir Nivel 1"){
      for(let i=0;i<this.listDatasources.length;i++){   
        this.listTreeControl[i].dataNodes=this.listDatasources[i].data;
        this.listTreeControl[i].collapseAll();
        for(let node of this.listTreeControl[i].dataNodes){
        this.listTreeControl[i].expand(node);  
        }
      }
    }else if(this.opcion =="Contraer"){
        for(let i=0;i<this.listDatasources.length;i++){
        this.listTreeControl[i].collapseAll();    
      }
    }
  }



todoItemSelectionToggle(node: ItemNode,event) {
  console.log(node);
   if(event.checked){
     this.lineaBaseService.lineaBase.idTextos.push(node.id);
      this.nodosSeleccionados.push(node);
      console.log(this.nodosSeleccionados);  

  }else{
    var index=this.lineaBaseService.lineaBase.idTextos.indexOf(node.id);
      if ( index !== -1 ) {
        this.lineaBaseService.lineaBase.idTextos.splice( index, 1 );
      }
      console.log(this.lineaBaseService.lineaBase.idTextos);
  
      var index2=this.nodosSeleccionados.indexOf(node);
      if ( index !== -1 ) {
        this.nodosSeleccionados.splice( index, 1 );
      }
      console.log(this.nodosSeleccionados);  

  }
 
}
  evaluarIsExpanded(node:any, index:number){
    if(node.cargaInicial){
      node.cargaInicial=false;
      if(node.coincidencias){
        this.listTreeControl[index].expand(node);
        return true;
      }
      if(!this.listTreeControl[index].isExpanded(node)){
        if(this.evaluarHijosCoincidentes(node)){
          this.listTreeControl[index].expand(node);
          return true;
        }
      }
    } 
    return this.listTreeControl[index].isExpanded(node);
  }

  evaluarHijosCoincidentes(node:any){
    if(this.tieneHijos(node)){
      for(let child of node.children){
        if(child.coincidencias){
          return true;
        }else{
          this.evaluarHijosCoincidentes(child);
        }
      }
    }
    return false;
  }

    construirArbol(busqueda:string, regulacion:Regulacion) {
    //defino la variable arbol
    console.log(regulacion)
    let dataSource = new MatTreeNestedDataSource<any>();
    let treeControl = new NestedTreeControl<any>(node => node.children);
    let nodo: ItemNode= new ItemNode();
    let nivelesSuperiores:Array<Nivel>=[];
    this.arbol=[];
		//saco los niveles que NO tienen padre, es decir que el idTextoPadre sea nulo o vacio
    for(let i=0;i< regulacion.niveles.length ; i++){
      if(regulacion.niveles[i].nivelSuperior == null){
        nivelesSuperiores.push(regulacion.niveles[i]);
      }
    }
		//instancio el arbol
		//inicio la adi cion de hijos, le envio el arbol entero, los niveles padres, y TODOS los niveles existentes (padres e hijos)
    this.busqueda=busqueda;

    this.addRamaArbol(nodo, nivelesSuperiores, regulacion.niveles, regulacion.id, busqueda);
    dataSource.data=this.arbol;
    console.log(dataSource)
    this.listTreeControl.push(treeControl);

    this.listDatasources.push(dataSource);
	}
	
  addRamaArbol(nodo:ItemNode,hijos:Array<Nivel>,nivelesEstructura:Array<Nivel>, idRegulacion:number, busqueda:string) {
    let nivelesSuperioresAux: Array<Nivel>=[];

    let nivel2:Nivel;
      for(let nivel of hijos) {
        for(let nivel2 of nivelesEstructura) {
          if(nivel2.idTextoPadre !=null){
            if(nivel.idTexto==nivel2.idTextoPadre) {
              nivelesSuperioresAux.push(nivel2);
            }
          }
        }
    
        let nodoHijo: ItemNode= new ItemNode();
        let reqs: Array<ItemNode>= [];
        let coincidencias:boolean=false;

        if(busqueda!='' && (nivel.texto.toUpperCase().includes(busqueda.toUpperCase()) 
        || nivel.textoAsociado.toUpperCase().includes(busqueda.toUpperCase()))){
          coincidencias=true;
        }

        if(nivel.requerimientos.length>0){
          for(let i=0;i< nivel.requerimientos.length ; i++){
            let nodoReq: ItemNode= new ItemNode();
            nodoReq.id=nivel.requerimientos[i].idRequerimiento;
            nodoReq.idTextoPadre=nivel.idTexto;
            nodoReq.texto=nivel.requerimientos[i].codigoRequerimiento;
            nodoReq.textoAsociado=nivel.requerimientos[i].requerimiento;
            nodoReq.type='R';
            nodoReq.tieneRequerimientos=nivel.tieneRequerimientos;
            nodoReq.coincidencias=false;
            nodoReq.cargaInicial=false;
            reqs.push(nodoReq);
          }
        }
        if(nivel.idTextoPadre==null){
          nodo= new ItemNode();
          nodo.id=nivel.idTexto;
          nodo.texto=nivel.texto;
          nodo.idTextoPadre=nivel.idTextoPadre;
          nodo.textoAsociado=nivel.textoAsociado;
          nodo.type='N';
          nodo.tieneRequerimientos=nivel.tieneRequerimientos;
          nodo.children=reqs;
          nodo.coincidencias=coincidencias;
          nodo.cargaInicial=true;
          nodo.idRegulacion=idRegulacion;
          nodoHijo=nodo;
          this.arbol.push(nodo);
        }else{
          nodoHijo.type='N';
          nodoHijo.id=nivel.idTexto;
          nodoHijo.texto=nivel.texto;
          nodoHijo.idTextoPadre=nivel.idTextoPadre;
          nodoHijo.textoAsociado=nivel.textoAsociado;
          nodoHijo.tieneRequerimientos=nivel.tieneRequerimientos;
          nodoHijo.children=reqs;
          nodoHijo.coincidencias=coincidencias;
          nodoHijo.cargaInicial=true;
          nodo.children.push(nodoHijo);
        }
        //llamo nuevamente a este metodo agregando un nodo nuevo, y los nuevos hijos q ahora son padres, y TODOS los niveles nuevamente
        this.addRamaArbol(nodoHijo, nivelesSuperioresAux, nivelesEstructura, idRegulacion, busqueda);
        nivelesSuperioresAux=[];      
      }
  }
  
  guardarLineaBase(){

    this.lineaBaseService.lineaBase.estadoLineaBaseUsuario="DEFINITIVO";
    this.lineaBaseService.lineaBase.origen="BUSQUEDA";
    for(let i=0;i<this.nodosSeleccionados.length;i++){
      if(this.nodosSeleccionados[i].tieneRequerimientos=="S"){
        for(let j=0;j<this.nodosSeleccionados[i].children.length;j++){
           if(this.nodosSeleccionados[i].children[j].type=="R"){
              console.log(this.nodosSeleccionados[i]);
              console.log(this.nodosSeleccionados[i].children[j]);
              console.log(this.nodosSeleccionados[i].id);
              console.log(this.nodosSeleccionados[i].children[j].id);
              this.cumplimientoService.getAgregarTextoReqCliente(this.usuarioService.usuario.nombreUsuario,this.nodosSeleccionados[i].id,this.nodosSeleccionados[i].children[j].id,this.lineaBaseService.lineaBase.idLinea).subscribe(data => {
                if(data.status==this.cumplimientoService.codOK){
               
                  console.log(data.status);
                }else{
                this.cumplimientoService.getManejoError(data);
                }
                })

                  }
           
           
        }
      
      }
    }
    console.log(this.lineaBaseService.lineaBase);
    this.lineaBaseService.getGuardarLinea(this.lineaBaseService.lineaBase).subscribe(data => {
      if(data.status==this.lineaBaseService.codOK){
      /*  for(let i=0;i<this.requerimientosCliente.length;i++){
     
          this.cumplimientoService.getGuardarTextoReqCliente(this.requerimientosCliente[i]).subscribe(data => {
            if(data.status==this.cumplimientoService.codOK){
         
           
            }else{
            this.lineaBaseService.getManejoError(data);
            }
            })
        }
        */
      this.router.navigate(['/baseline']);
      }else{
      this.lineaBaseService.getManejoError(data);
      }
      })
 
     
  }
  guardarLineaBaseTemporal(){
    const data = {
      title: 'Confirmación',
      body1: `Esta opción le permitirá seguir trabajando en esta Linea Base, de tal forma que guarda todas las selecciones que ha realizado más las regulaciones que no han sido cubiertas`,
      cancelButton: 'Cancelar',
      acceptButton: 'Confirmar',
      reason: ''
    };

    const dialogRef = this.dialog.open(ModalBaselineComponent, {
      width: '500px',
      data
    });

    dialogRef.afterClosed().subscribe(result => {
      if (data.reason === 'accept') {
        this.lineaBaseService.lineaBase.estadoLineaBaseUsuario="TEMPORAL";
    
        this.lineaBaseService.lineaBase.origen="BUSQUEDA";
        console.log(this.lineaBaseService.lineaBase);
        this.lineaBaseService.getGuardarLinea(this.lineaBaseService.lineaBase).subscribe(data => {
          if(data.status==this.lineaBaseService.codOK){
           
          this.router.navigate(['/baseline']);
          }else{
          this.lineaBaseService.getManejoError(data);
          }
          })
      } else {
        
      }
    });
 

  }
  consultarLineasBase(){
    this.lineaBaseService.getConsultarLineasBase(this.usuarioService.usuario.nombreUsuario,1000).subscribe(data => {
      if(data.status==this.lineaBaseService.codOK){
        this.lineasBase=data.response;
        console.log(this.lineasBase);
        for(let i=0; i < this.lineasBase.length ; i++){
       if(this.lineaBaseService.lineaBase.nombreLineaBase == this.lineasBase[i].nombreLineaBase ){
        this.lineaBaseService.lineaBase.idLinea=this.lineasBase[i].idLinea;
        console.log(this.lineaBaseService.lineaBase);
      }
    }
        
      }else{
      this.lineaBaseService.getManejoError(data);
      }
      })
  }
  

  exportarPdf(){
    console.log(this.lineaBaseService.lineaBase.idLinea)
    this.exportacionService.getGenerarArchivoLineaBase(this.usuarioService.usuario.nombreUsuario,this.lineaBaseService.lineaBase.idLinea,"0").subscribe(data => {
      if(data.status==this.exportacionService.codOK){
        var sampleArr = this.base64ToArrayBuffer(data.response);
        this.saveByteArray(this.lineaBaseService.lineaBase.nombreLineaBase, sampleArr);
       
      }else{
      
       this.exportacionService.getManejoError(data);
      }
      })
  }


  base64ToArrayBuffer(base64) {
    var binaryString = window.atob(base64);
    var binaryLen = binaryString.length;
    var bytes = new Uint8Array(binaryLen);
    for (var i = 0; i < binaryLen; i++) {
       var ascii = binaryString.charCodeAt(i);
       bytes[i] = ascii;
    }
    return bytes;
 }

  saveByteArray(reportName, byte) {
  var blob = new Blob([byte], {type: "application/pdf"});
  var link = document.createElement('a');
  link.href = window.URL.createObjectURL(blob);
  var fileName = reportName;
  link.download = fileName;
  link.click();
}
}
