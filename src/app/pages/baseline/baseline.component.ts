import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator, MatTableDataSource, MatDialog } from '@angular/material';

import { ModalComponent } from '../../../app/shared/components/modal/modal.component';
import { Router } from '@angular/router';
import { ModalDetailComponent } from 'src/app/shared/components/modal-detail/modal-detail.component';
import { ModalBaselineComponent } from 'src/app/shared/components/modal-baseline/modal-baseline.component';
import { LineaBaseService } from 'src/app/service/lineabase.service';
import { ModalSaveBaselineComponent } from 'src/app/shared/components/modal-save-baseline/modal-save-baseline.component';
import { LineaBase } from 'src/app/shared/dto/LineaBase';
import { FusionLineaBase } from 'src/app/shared/dto/FusionLineaBase';
import { UsuarioService } from 'src/app/service/usuario.service';
import { ModalValidadorComponent } from 'src/app/shared/components/modal-validador/modal-validador.component';

@Component({
  selector: 'app-baseline',
  templateUrl: './baseline.component.html',
  styleUrls: ['./baseline.component.scss']
})
export class BaselineComponent implements OnInit {
  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;

  displayedColumns = ['change_reg', 'fused', 'name', 'description','estado','date_entered', 'date_modified', 'id'];

  dataSource = new MatTableDataSource<any>([]);
  public carga:boolean=true;
  public fusionar:boolean;
  public switchOp = 'recents';
  public switchFilter = 'filters';
  public cantidadLineasBaseSelec:number;
  public lineasBase:Array <LineaBase>;
  public lineaEliminar:LineaBase;
  public nombreLineaEliminar:string;
  constructor(private lineaBaseService:LineaBaseService,
    private dialog: MatDialog,
    private usuarioService:UsuarioService,
    private router: Router
  ) { }

  ngOnInit() {
    this.cantidadLineasBaseSelec=0;
    this.lineaBaseService.fusionLineaBase = new FusionLineaBase();
    this.consultarLineasBase();
    this.dataSource.paginator = this.paginator;

  }
  fusionarLineaBase() {
    const dialogRef = this.dialog.open(ModalSaveBaselineComponent, {
      width: '500px',
      data: {

      }
    });

    dialogRef.afterClosed().subscribe(
      data =>{ if(data.reason=='accept'){
      /*  let i=this.lineasBase.indexOf(this.busquedaEliminar);
        this.busquedasUsuarioGuardadas.splice(i);*/
        this.disableFuse();
        this.ngOnInit();
      };});  
  }
  enableFuse() {
    this.displayedColumns = ['fuse', 'change_reg', 'fused', 'name', 'description', 'date_entered', 'date_modified', 'id'];
    this.fusionar=true;
  }
  disableFuse() {
    this.displayedColumns = ['change_reg', 'fused', 'name', 'description', 'date_entered', 'date_modified', 'id'];
    this.fusionar=false;
  }
  datosTabla(){
    console.log("arreglo actualizado"+this.lineasBase);
 
      this.dataSource = new MatTableDataSource(this.lineasBase);  
      console.log(this.lineasBase);

  }

  ventanaConfirmacion(idLinea:number,nombreLinea:String) {
    let respuestaDialogo='';
    const dialogRef = this.dialog.open(ModalComponent, {
      width: '500px',
      data: {
        title: 'Confirmación',
        body1: `Esta opción elimina la Linea Base`,
        body2:  'Por favor confirmar esta acción',
        cancelButton: 'Cancelar',
        acceptButton: 'Confirmar',
        id:idLinea,
        busqueda:`"`+nombreLinea+`"`,
        tipo:"LineaBase"

      }
    });

    dialogRef.afterClosed().subscribe(
      data =>{ if(data.reason=='accept'){
        let i=this.lineasBase.indexOf(this.lineaEliminar);
        this.lineasBase.splice(i);
        this.ngOnInit();
      };});  
    
    }

  eliminar(id:number){
    for(let i=0;i < this.lineasBase.length;i++){
      if(this.lineasBase[i].idLinea == id){
        this.nombreLineaEliminar=this.lineasBase[i].nombreLineaBase;
        this.lineaEliminar=this.lineasBase[i];
      }
    }
    if(this.lineaEliminar.estadoLineaBaseUsuario == "DEFINITIVO"){
      const dialogRef = this.dialog.open(ModalValidadorComponent, {
        width: '500px',
        data: {
          body1:"No puede eliminar una linea base definivita",
          title:"Validación",
          acceptButton:"Aceptar"
        }
      });
    }else{
    this.ventanaConfirmacion(id,this.nombreLineaEliminar);
    }
  }


  detailLineaBase(element){
    console.log(element);
    this.lineaBaseService.lineaBase=element;
    this.router.navigateByUrl('/baseline/detail');
  }
  editarLineaBase(element){
    console.log(element);
    this.lineaBaseService.lineaBase=element;
    this.router.navigateByUrl('/baseline/save');
  }
  onCheckChange(event,lineasBase) {
    console.log(event.checked);
    
    if(event.checked){
      this.cantidadLineasBaseSelec++;
      console.log(this.cantidadLineasBaseSelec);
      this.lineaBaseService.lineasBaseFusion.push(lineasBase);
      /*this.busqueda.idRegulacionesSeleccionadas.push(id);
      console.log(this.busqueda.idRegulacionesSeleccionadas);*/
    }else{
      this.cantidadLineasBaseSelec--;
      console.log(this.cantidadLineasBaseSelec);
     
     var index= this.lineaBaseService.lineasBaseFusion.indexOf(lineasBase);
      if ( index !== -1 ) {
        this.lineaBaseService.lineasBaseFusion.splice( index, 1 );
      }
      console.log(this.lineaBaseService.lineasBaseFusion);
    }
    
  }

  consultarLineasBase(){
    this.lineaBaseService.getConsultarLineasBase(this.usuarioService.usuario.nombreUsuario,1000).subscribe(data => {
      if(data.status==this.lineaBaseService.codOK){
        this.lineasBase=data.response;
        console.log(this.lineasBase);
        this.datosTabla();
        this.carga=false;
      }else{
      this.lineaBaseService.getManejoError(data);
      }
      })
  }
  new() {
    const data = {
      title: 'Confirmación',
      body1: `Seleccione si requiere construir una linea base a partir de búsquedas guardadas o de una nueva búsqueda`,
      cancelButton: 'Inicio',
      acceptButton: 'Búsquedas guardadas',
      reason: ''
    };

    const dialogRef = this.dialog.open(ModalBaselineComponent, {
      width: '500px',
      data
    });

    dialogRef.afterClosed().subscribe(result => {
      if (data.reason === 'accept') {
        this.router.navigateByUrl('/baseline/recurrent');
      } else {
        this.router.navigateByUrl('/search/history');
      }
    });
  }

}


