import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-regulation',
  templateUrl: './regulation.component.html',
  styleUrls: ['./regulation.component.scss']
})
export class RegulationComponent implements OnInit {

  public mockItem: any = {
    id: 1,
    title: 'Super Financiera',
    check_title: 'Circular 01 relativa a pago de impuestos',
    info: [
      {
        name: 'Riesgos asociados',
        value: 'Duis aute irure dolor',
      },
      {
        name: 'Multas asociadas',
        value: 'Excepteur sint occaecat cupidatat',
      },
      {
        name: 'Areas Responsables',
        value: 'Duis aute irure dolor',
      },
      {
        name: 'Areas Involucradas',
        value: 'Excepteur sint occaecat cupidatat',
      },
    ],
    validity: {
      state: 'VIGENTE',
      date: {
        day: '23',
        month: 'Ene',
        year: '2019'
      }
    },
    resume: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua...',
    sectors: ['Bancario'],
    jurisdictions: ['Nacional'],
    impacts: ['Laft', 'Corrupción', 'Soborno', 'Riesgo Operativo']
  };

  constructor() { }

  ngOnInit() {
  }

}
