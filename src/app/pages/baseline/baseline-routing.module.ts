import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { BaselineComponent } from './baseline.component';
import { NewComponent } from './new/new.component';
import { RecurrentComponent } from './recurrent/recurrent.component';
import { SaveComponent } from './save/save.component';
import { RegulationComponent } from './regulation/regulation.component';
import { DetailBaselineComponent } from './detail-baseline/detail-baseline.component';

const routes: Routes = [
  { path: '', component: BaselineComponent },
  { path: 'new', component: NewComponent },
  { path: 'recurrent', component: RecurrentComponent },
  { path: 'save', component: SaveComponent },
  { path: 'detail', component: DetailBaselineComponent },
  { path: 'regulation', component: RegulationComponent }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class BaselineRoutingModule { }
