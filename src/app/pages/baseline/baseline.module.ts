
import { BaselineRoutingModule } from './baseline-routing.module';
import { BaselineComponent } from './baseline.component';

import { BaselineMaterialModules } from './baseline.material';

import { RecurrentComponent } from './recurrent/recurrent.component';
import { NewComponent } from './new/new.component';
import { SaveComponent } from './save/save.component';
import { RegulationComponent } from './regulation/regulation.component';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {MatInputModule} from '@angular/material/input'; 
import { FormsModule } from '@angular/forms';
import { MomentModule } from 'ngx-moment';
import {MatAutocompleteModule} from '@angular/material/autocomplete';

import { SharedModule } from 'src/app/shared/shared.module';

import { DetailBaselineComponent } from './detail-baseline/detail-baseline.component';


@NgModule({
  declarations: [
    BaselineComponent, 
    RecurrentComponent, 
    NewComponent, 
    SaveComponent,
    RegulationComponent,
    DetailBaselineComponent
      
     ],
  imports: [
    CommonModule,
    BaselineRoutingModule,
    MomentModule,
    SharedModule,
    CommonModule,
    MatInputModule,
    FormsModule,
    MatAutocompleteModule,
    ...BaselineMaterialModules
  ]
})
export class BaselineModule { }
