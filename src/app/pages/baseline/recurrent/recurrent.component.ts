import { Component, OnInit, ViewChild, OnDestroy } from '@angular/core';
import { MatTableDataSource, MatPaginator, MatDialog } from '@angular/material';
import { BusquedaService } from 'src/app/service/busqueda.service';
import { ModalComponent } from '../../../shared/components/modal/modal.component';
import { BusquedaUsuario } from 'src/app/shared/dto/BusquedaUsuario';
import { Router } from '@angular/router';
import * as moment from 'moment';
import { ModalConfirmacionComponent } from 'src/app/shared/components/modal-confirmacion/modal-confirmacion.component';
import { LineaBaseService } from 'src/app/service/lineabase.service';
import { UsuarioService } from 'src/app/service/usuario.service';

@Component({
  selector: 'app-recurrent',
  templateUrl: './recurrent.component.html',
  styleUrls: ['./recurrent.component.scss']
})
export class RecurrentComponent implements OnInit {
  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  public switchHistory = 'recents';
  public busquedasUsuarioGuardadas: Array<BusquedaUsuario>=[];
  public nombreBusquedaEliminar:String;
  public busquedaEliminar:BusquedaUsuario;
  public fechaCreacion:string;

  dataSource: MatTableDataSource<BusquedaUsuario>=null;
  displayedColumns = ['name', 'description', 'date_entered', 'date_modified', 'select', 'id'];


  constructor(
    public dialog: MatDialog,private busquedaService: BusquedaService,
    private lineaBaseService:LineaBaseService,
    private usuarioService:UsuarioService,
    private router: Router
  ) { }

  ngOnInit() {
 
    this.consultarBusquedasGuardadas();
  }
  /* ngOnDestroy() {
    this.showDialog();
  }*/

 /* showDialog() {
    const dialogRef = this.dialog.open(ModalComponent, {
      width: '500px',
      data: {
        title: 'Confirmación',
        body: `Existe una búsqueda con fecha <span class="text-primary-green">23 de Mayo de 2019</span> sin guardar. ¿Desea complementarla?`,
        cancelButton: 'Descartar',
        acceptButton: 'Continuar'
      }
    });
    
  }*/
  consultarBusqueda(element){
    console.log(element);
    this.busquedaService.getBusquedaUsuario(this.usuarioService.usuario.nombreUsuario,element.idBusqueda,"ACTIVO").subscribe(data => {
      if(data.status==this.busquedaService.codOK){
      this.busquedaService.resultadoBusqueda=data.response;
      console.log(this.busquedaService.resultadoBusqueda);
      this.router.navigate(['/search/consulta']);
      }else{
      this.busquedaService.getManejoError(data);
      }
      })
 
  }
    
  
  onCheckChange(event,id) {
    console.log(id);
    if(event.checked){
      var index=this.lineaBaseService.idBusquedasSelec.indexOf(id);
      if ( index == -1 ) {
        this.lineaBaseService.idBusquedasSelec.push(id);
        console.log(this.lineaBaseService.idBusquedasSelec);
      }
   
    }else{
      var index=this.lineaBaseService.idBusquedasSelec.indexOf(id);
      if ( index !== -1 ) {
        this.lineaBaseService.idBusquedasSelec.splice( index, 1 );
      }
      console.log(this.lineaBaseService.idBusquedasSelec);
    }
    
  }

  datosTabla(){
    console.log("arreglo actualizado"+this.busquedasUsuarioGuardadas);
 
      this.dataSource = new MatTableDataSource(this.busquedasUsuarioGuardadas);  
      console.log(this.busquedasUsuarioGuardadas);

  }



  consultarBusquedasGuardadas(){
 
    this.busquedaService.getBusquedasEstado(this.usuarioService.usuario.nombreUsuario,"GUARDADA").subscribe(data => {
      if(data.status==this.busquedaService.codOK){
      this.busquedasUsuarioGuardadas=data.response;
      console.log(this.busquedasUsuarioGuardadas);
      this.datosTabla(); 
      this.dataSource.paginator = this.paginator;
     // this.showDialog();
      }else{
      this.busquedaService.getManejoError(data);
      }
      })
  }

  editarBusqueda(element){
    console.log(element);
    this.busquedaService.getBusquedaUsuario(this.usuarioService.usuario.nombreUsuario,element.idBusqueda,"ACTIVO").subscribe(data => {
      if(data.status==this.busquedaService.codOK){
      this.busquedaService.resultadoBusqueda=data.response;
      console.log(this.busquedaService.resultadoBusqueda);
      this.router.navigate(['/search']);
      }else{
      this.busquedaService.getManejoError(data);
      }
      })
 
  }


}
