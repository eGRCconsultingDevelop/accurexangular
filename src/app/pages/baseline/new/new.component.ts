import { Component, OnInit } from '@angular/core';
import { MatDialog, MatTableDataSource } from '@angular/material';
import { LineaBaseService } from 'src/app/service/lineabase.service';
import { BusquedaUsuario } from 'src/app/shared/dto/BusquedaUsuario';
import { BusquedaService } from 'src/app/service/busqueda.service';
import { Router } from '@angular/router';
import { LineaBase } from 'src/app/shared/dto/LineaBase';
import { ModalValidadorComponent } from 'src/app/shared/components/modal-validador/modal-validador.component';
import { UsuarioService } from 'src/app/service/usuario.service';

@Component({
  selector: 'app-new',
  templateUrl: './new.component.html',
  styleUrls: ['./new.component.scss']
})
export class NewComponent implements OnInit {
  displayedColumns = ['name', 'description', 'select'];
  dataSource = new MatTableDataSource<any>([]);
  public busquedasUsuarioGuardadas: Array<BusquedaUsuario>=[];
  public busquedasSelec:Array <BusquedaUsuario>=[];
  public cont:number=0;
  public busquedasSeleccionadas:boolean=true;
  public lineaBase:LineaBase = new LineaBase();
  public nombreLineaBase:string="";
  public descripcionLineaBase:string="";
  public lineasBase:Array <LineaBase>;
  public lineasBaseNombres:Array <LineaBase>=[];
  public nombreIgual:boolean=false;

  constructor(   
  public dialog: MatDialog,private lineaBaseService:LineaBaseService, 
  private busquedaService:BusquedaService,
  private usuarioService:UsuarioService, 
  private router: Router) { }

  ngOnInit() {
    this.busquedasSelec=[];
    this.consultarBusquedasGuardadas();
    console.log(this.lineaBaseService.idBusquedasSelec);

    this.consultarLineasBase();

    

  }
  consultarLineasBase(){
    this.lineaBaseService.getConsultarLineasBase(this.usuarioService.usuario.nombreUsuario,1000).subscribe(data => {
      if(data.status==this.lineaBaseService.codOK){
        this.lineasBaseNombres=data.response;
        console.log(this.lineasBaseNombres);
     
   
      }else{
      this.lineaBaseService.getManejoError(data);
      }
      })
  }
  datosTabla(){
    console.log("arreglo actualizado"+this.busquedasSelec);
 
      this.dataSource = new MatTableDataSource(this.busquedasSelec);  
      console.log(this.busquedasSelec);

  }

  onCheckChange(event,id) {
    console.log(id);
    if(event.checked){
      this.lineaBaseService.idBusquedasSelec.push(id);
      console.log(this.lineaBaseService.idBusquedasSelec);
      
        this.busquedasSeleccionadas=true;
    
    }else{
      var index=this.lineaBaseService.idBusquedasSelec.indexOf(id);
      if ( index !== -1 ) {
        this.lineaBaseService.idBusquedasSelec.splice( index, 1 );
      }
      if(this.lineaBaseService.idBusquedasSelec.length == 0){
        this.busquedasSeleccionadas=false;
      }
      console.log(this.lineaBaseService.idBusquedasSelec);
    }
    
  }
  consultarBusquedasGuardadas(){
 
    this.busquedaService.getBusquedasEstado(this.usuarioService.usuario.nombreUsuario,"GUARDADA").subscribe(data => {
      if(data.status==this.busquedaService.codOK){
      this.busquedasUsuarioGuardadas=data.response;
      console.log(this.busquedasUsuarioGuardadas);
     // this.showDialog();
     for(let i=0;i< this.busquedasUsuarioGuardadas.length;i++){
      for(let j=0;j< this.lineaBaseService.idBusquedasSelec.length;j++){

      
      if(this.busquedasUsuarioGuardadas[i].idBusqueda == this.lineaBaseService.idBusquedasSelec[j]){
        this.busquedasSelec[this.cont]=this.busquedasUsuarioGuardadas[i];
        this.cont++;
      }

    }
   
  }
  this.datosTabla();
  console.log(this.busquedasSelec);

  console.log(this.lineaBaseService.idBusquedasSelec);
      }else{
      this.busquedaService.getManejoError(data);
      }
      })
  }

  guardarLineaBase() {

    for(let i=0;i<this.lineasBaseNombres.length;i++){
      if(this.lineasBaseNombres[i].nombreLineaBase == this.nombreLineaBase){
        this.nombreIgual=true;
        console.log("Son iguales");
      }else{
        this.nombreIgual=false;
      }

   }

   if(this.nombreIgual){
    const dialogRef = this.dialog.open(ModalValidadorComponent, {
      width: '500px',
      data: {
        body1:"Ya existe una Linea Base con este nombre",
        title:"Lineas Base iguales",
        acceptButton:"Aceptar"
      }
    });

    this.nombreIgual=false;

   }else if(this.nombreLineaBase== "" && this.descripcionLineaBase != ""){
      const dialogRef = this.dialog.open(ModalValidadorComponent, {
        width: '500px',
        data: {
          body1:"El campo Nombre de Linea Base esta vacio",
          title:"Validación de campos",
          acceptButton:"Aceptar"
        }
      });
     }else if(this.descripcionLineaBase == "" && this.nombreLineaBase != ""){
      const dialogRef = this.dialog.open(ModalValidadorComponent, {
        width: '500px',
        data: {
          body1:"El campo Descripción de la Linea Base esta vacio",
          title:"Validación de campos",
          acceptButton:"Aceptar"
        }
      });
  
    }else if(this.nombreLineaBase== "" && this.descripcionLineaBase == ""){
      const dialogRef = this.dialog.open(ModalValidadorComponent, {
        width: '500px',
        data: {
          body1:"Los campos Nombre de Linea Base y Descripción de la linea base estan vacios",
          title:"Validación de campos",
          acceptButton:"Aceptar"
        }
      });
    } else{
    this.lineaBaseService.lineaBase=new LineaBase();
    this.lineaBaseService.lineaBase.nombreLineaBase=this.nombreLineaBase;
    this.lineaBaseService.lineaBase.descripcionLineaBase=this.descripcionLineaBase;
    this.lineaBaseService.lineaBase.origen="BUSQUEDA";
    /* if(this.busqueda.filtrosDetalle.filtros.estado == null){
       this.busqueda.filtrosDetalle.filtros= new FiltrosBusqueda();
     }*/
    
     this.lineaBaseService.busquedasSelec=this.busquedasSelec;
     for(let i=0;i<this.busquedasSelec.length;i++){
      this.lineaBaseService.lineaBase.idBusquedas.push(this.busquedasSelec[i].idBusqueda);
     }
     this.lineaBaseService.lineaBase.estadoLineaBaseUsuario="TEMPORAL";
    this.lineaBaseService.lineaBase.nombreUsuario=this.usuarioService.usuario.nombreUsuario;
    this.lineaBaseService.getGuardarLinea(this.lineaBaseService.lineaBase).subscribe(data => {
      if(data.status==this.lineaBaseService.codOK){
        this.lineaBaseService.idBusquedasSelec=[];
        this.router.navigate(['/baseline/save']);
      }else{
      this.lineaBaseService.getManejoError(data);
      }
      })
   }

  }

  atras(){
    this.lineaBaseService.idBusquedasSelec=[];
    this.router.navigate(['/baseline/recurrent']);
  }

}
