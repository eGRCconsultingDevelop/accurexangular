import {
  MatCardModule,
  MatTableModule,
  MatCheckboxModule,
  MatPaginatorModule,
  MatButtonModule,
  MatExpansionModule,
  MatDialogModule,
  MatIconModule,
  MatTreeModule,
  MatMenuModule,
  MatTooltipModule,
  MatProgressBarModule,
  MatRadioModule
} from '@angular/material';

export const BaselineMaterialModules = [
  MatCardModule,
  MatExpansionModule,
  MatTableModule,
  MatCheckboxModule,
  MatExpansionModule,
  MatCheckboxModule,
  MatPaginatorModule,
  MatButtonModule,
  MatCardModule,
  MatTableModule,
  MatDialogModule,
  MatIconModule,
  MatTreeModule,
  MatMenuModule,
  MatTooltipModule,
  MatProgressBarModule,
  MatRadioModule
];
