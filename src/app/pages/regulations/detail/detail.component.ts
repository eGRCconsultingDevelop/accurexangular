import { Component, OnInit } from '@angular/core';
import { NestedTreeControl } from '@angular/cdk/tree';
import { MatTreeNestedDataSource, MatDialog } from '@angular/material';
import { ModalRegulationsComponent } from 'src/app/shared/components/modal-regulations/modal-regulations.component';
import { Regulacion } from 'src/app/shared/dto/Regulacion';
import { ItemNode } from 'src/app/shared/dto/ItemNode';
import { Nivel } from 'src/app/shared/dto/Nivel';
import { CambioRegulatorioService } from 'src/app/service/cambio-regulatorio.service';
import { SelectionModel } from '@angular/cdk/collections';
import { CambioRegulatorio } from 'src/app/shared/dto/CambioRegulatorio';
import { ModalCambioRegComponent } from 'src/app/shared/components/modal-cambio-reg/modal-cambio-reg.component';
import { Requerimiento } from 'src/app/shared/dto/Requerimiento';
import { UsuarioService } from 'src/app/service/usuario.service';

@Component({
  selector: 'app-detail',
  templateUrl: './detail.component.html',
  styleUrls: ['./detail.component.scss']
})
export class DetailComponent implements OnInit {
  public collapsed = true;
  public filtersCollapsed = false;

  treeControl = new NestedTreeControl<any>(node => node.children);
  dataSource = new MatTreeNestedDataSource<any>();
  public busqueda:string='';
  public expandedTree=false;
  public collapsedTree =false;
  public regulacion : Regulacion=this.cambioRegService.cambioRegulatorio.regulacionNueva;
  public idCambioReg:number=this.cambioRegService.cambioRegulatorio.idCambioReg;
  public cambioReg:CambioRegulatorio=this.cambioRegService.cambioRegulatorio;
  listTreeControl : Array<NestedTreeControl<any>>=[];
  listDatasources: Array<MatTreeNestedDataSource<any>>=[];
  public mockItem: any ;
  public arbol:Array<ItemNode>=[];
  public mostrarReq=false;
  public palabraClave:String;
  public opcion: string;
  public nodosSeleccionados:Array<ItemNode>=[];
  public nombresLineasBase:Array<string>=[];
  public autores:Array<string>=[];
  checklistSelection = new SelectionModel<ItemNode>(true /* multiple */);
  

  constructor(
    private dialog: MatDialog,
    public cambioRegService:CambioRegulatorioService,
    private usuarioService:UsuarioService
  ) {
    this.construirArbol('');
    console.log(this.cambioReg);
    var fechaCambio = this.cambioReg.fechaCambio.split("/", 3); 
    for(let i=0;i<this.cambioReg.lineasBaseAfectadas.length;i++){
        this.nombresLineasBase.push(this.cambioReg.lineasBaseAfectadas[i].nombreLineaBase);
        this.autores.push(this.cambioReg.lineasBaseAfectadas[i].autor);
    }
    this.cambioRegService.autores=this.autores;
    this.cambioRegService.lineasBase=this.nombresLineasBase;
    this.mockItem ={
      id: this.cambioReg.idCambioReg,
      lineasBase:[this.nombresLineasBase],
      autores:[this.autores],
      idRegulacion:this.cambioReg.regulacionNueva,
      title: this.cambioReg.nombreCambioReg,
      check_title: this.cambioReg.regulacionNueva.nombreRegulacion,
      info: [
        {
          name: 'Descripción cambio',
          value: this.cambioReg.descripcionCambio,
        },
      ],
      validity: {
        state: this.regulacion.estado,
        date: {
          day: fechaCambio[1],
            month: fechaCambio[0],
            year: fechaCambio[2] 
        }
      },
     
    };
  }

  hasChild = (_: number, node: any) => !!node.children && node.children.length > 0;
  isReq = (_: number, node: any) => node.type=='R';
  ngOnInit() {
    this.treeControl.dataNodes=this.dataSource.data;
    this.treeControl.collapseAll();
    for(let node of this.treeControl.dataNodes){
    this.treeControl.expand(node);  
    }
  }
  opcionSeleccionada(event){
    if(this.opcion == "Expandir"){
      this.collapsedTree=false;
      this.expandedTree=true;
      this.treeControl.dataNodes=this.dataSource.data;
      this.treeControl.expandAll();
      console.log(event.checked);
    }else if(this.opcion =="Expandir Nivel 1"){
      this.treeControl.dataNodes=this.dataSource.data;
      this.treeControl.collapseAll();
      for(let node of this.treeControl.dataNodes){
      this.treeControl.expand(node);  
      }
    }else if(this.opcion =="Contraer"){
      this.treeControl.collapseAll();
      
    }
  }

    textoNuevo(nivel:Nivel){

      if(nivel.cambioRegulatorioRegulacion == this.idCambioReg){

        return nivel.textoAsociado;
      }

    return null;
  
  }

  tituloNuevo(nivel:Nivel){

    if(nivel.cambioRegulatorioRegulacion == this.idCambioReg  ){

      return nivel.texto;
    }

  return null;

}
  metodoComparar(nivel:Nivel){

    for (let nivelMod of this.cambioReg.nivelesModificados ){
      
      if(nivel.idTextoPadre==nivelMod.idTextoPadre && nivel.orden==nivelMod.orden
      && nivelMod.estado=='INACTIVO' && nivel.textoAsociado != nivelMod.textoAsociado && nivel.ordenPadre == nivelMod.ordenPadre){
        
    
        return nivelMod.textoAsociado;
      }
      if(nivel.nombreNivel==nivelMod.nombreNivel && nivelMod.orden==nivel.orden && nivel.textoAsociado != nivelMod.textoAsociado
        && nivel.ordenPadre == nivelMod.ordenPadre){
        
    
       return nivelMod.textoAsociado;
      }
    }
    return null;
  
  }
  metodoCompararTitulo(nivel:Nivel){

    for (let nivelMod of this.cambioReg.nivelesModificados ){
      
      if(nivel.idTextoPadre==nivelMod.idTextoPadre && nivel.orden==nivelMod.orden
      && nivelMod.estado=='INACTIVO' && nivel.texto != nivelMod.texto && nivel.ordenPadre == nivelMod.ordenPadre ){
        
         
        return nivelMod.texto;
      } 
      if(nivel.nombreNivel==nivelMod.nombreNivel && nivelMod.orden==nivel.orden && nivel.texto != nivelMod.texto
        && nivel.ordenPadre == nivelMod.ordenPadre ){
        
    
       return nivelMod.texto;
      }
    }
    return null;
  
  }

  compararRequerimientos(nivel:Nivel){
    for (let nivelMod of this.cambioReg.nivelesModificados ){
     if(nivelMod.tieneRequerimientos =="S"){
      
     }
    }


 
    
  }
  applyChangesToBaseline() {

    const dialogRef = this.dialog.open(ModalCambioRegComponent, {
      width: '750px',
      data: {
        title: 'Confirmación',
        body1: `Esta opción Aplica los cambios de la regulacion`,
        body2:  'Por favor confirmar esta acción',
        acceptButton: 'Aceptar',
        
      }
    });
    console.log(this.cambioReg.idCambioReg)
    console.log(this.cambioReg.regulacionNueva.id)
    this.cambioRegService.getAplicarCambio(this.usuarioService.usuario.nombreUsuario,this.cambioReg.idCambioReg,this.usuarioService.usuario.cliente.idCliente,this.cambioReg.regulacionNueva.id,false).subscribe(data => {
      if(data.status==this.cambioRegService.codOK){
        console.log(data.response);
      }else{
      this.cambioRegService.getManejoError(data);
      }
      })

   /* const dialogRef = this.dialog.open(ModalRegulationsComponent, {
      width: '750px',
      data: {}
    });*/
  }






  tieneHijos(node){
    return !!node.children && node.children.length > 0;
  }

 
 

  evaluarIsExpanded(node:any){
    if(node.cargaInicial){
      node.cargaInicial=false;
      if(node.coincidencias){
        this.treeControl.expand(node);
        return true;
      }
      if(!this.treeControl.isExpanded(node)){
        if(this.evaluarHijosCoincidentes(node)){
          this.treeControl.expand(node);
          return true;
        }
      }
    } 
    return this.treeControl.isExpanded(node);
  }

  evaluarHijosCoincidentes(node:any){
    if(this.tieneHijos(node)){
      for(let child of node.children){
        if(child.coincidencias){
          return true;
        }else{
          this.evaluarHijosCoincidentes(child);
        }
      }
    }
    return false;
  }

    construirArbol(busqueda:string) {
		//defino la variable arbol
    let nodo: ItemNode= new ItemNode();
    let nivelesSuperiores:Array<Nivel>=[];
    this.arbol=[];
		//saco los niveles que NO tienen padre, es decir que el idTextoPadre sea nulo o vacio
    for(let i=0;i< this.regulacion.niveles.length ; i++){
      if(this.regulacion.niveles[i].nivelSuperior == null){
        nivelesSuperiores.push(this.regulacion.niveles[i]);
      }
    }
		//instancio el arbol
		//inicio la adi cion de hijos, le envio el arbol entero, los niveles padres, y TODOS los niveles existentes (padres e hijos)
    this.busqueda=busqueda;
    this.addRamaArbol(nodo, nivelesSuperiores, this.regulacion.niveles, busqueda);
    this.dataSource.data=this.arbol;
	}
	
  addRamaArbol(nodo:ItemNode,hijos:Array<Nivel>,nivelesEstructura:Array<Nivel>, busqueda:string) {
    let nivelesSuperioresAux: Array<Nivel>=[];

    let nivel2:Nivel;
      for(let nivel of hijos) {
        for(let nivel2 of nivelesEstructura) {
          if(nivel2.idTextoPadre !=null){
            if(nivel.idTexto==nivel2.idTextoPadre) {
              nivelesSuperioresAux.push(nivel2);
            }
          }
        }
    
        let nodoHijo: ItemNode= new ItemNode();
        let reqs: Array<ItemNode>= [];
        let coincidencias:boolean=false;

        if(busqueda!='' && (nivel.texto.toUpperCase().includes(busqueda.toUpperCase()) 
        || nivel.textoAsociado.toUpperCase().includes(busqueda.toUpperCase()))){
          coincidencias=true;
        }

        if(nivel.requerimientos.length>0){
          for(let i=0;i< nivel.requerimientos.length ; i++){
            let nodoReq: ItemNode= new ItemNode();
            nodoReq.id=nivel.requerimientos[i].idRequerimiento;
            nodoReq.idTextoPadre=nivel.idTexto;
            nodoReq.texto=nivel.requerimientos[i].codigoRequerimiento;
            nodoReq.textoAsociado=nivel.requerimientos[i].requerimiento;
            nodoReq.textoAsociado=nivel.requerimientos[i].requerimiento;
            nodoReq.cambioRegulatorioRegulacion=nivel.cambioRegulatorioRegulacion;
            nodoReq.type='R';
            nodoReq.tieneRequerimientos=nivel.tieneRequerimientos;
            nodoReq.coincidencias=false;
            nodoReq.cargaInicial=false;
            nodoReq.orden=nivel.orden;
            nodoReq.ordenPadre=nivel.ordenPadre;
            nodoReq.nombreNivel=nivel.nombreNivel;
            nodoReq.estado=nivel.estado;
            reqs.push(nodoReq);
          }
        }
        if(nivel.idTextoPadre==null){
          nodo= new ItemNode();
          nodo.id=nivel.idTexto;
          nodo.texto=nivel.texto;
          nodo.idTextoPadre=nivel.idTextoPadre;
          nodo.textoAsociado=nivel.textoAsociado;
          nodo.type='N';
          nodo.tieneRequerimientos=nivel.tieneRequerimientos;
          nodo.cambioRegulatorioRegulacion=nivel.cambioRegulatorioRegulacion;
          nodo.children=reqs;
          nodo.coincidencias=coincidencias;
          nodo.cargaInicial=true;
          nodo.orden=nivel.orden;
          nodo.ordenPadre=nivel.ordenPadre;
          nodo.nombreNivel=nivel.nombreNivel;
          nodo.estado=nivel.estado;
          nodoHijo=nodo;
          this.arbol.push(nodo);
        }else{
          nodoHijo.type='N';
          nodoHijo.id=nivel.idTexto;
          nodoHijo.texto=nivel.texto;
          nodoHijo.idTextoPadre=nivel.idTextoPadre;
          nodoHijo.textoAsociado=nivel.textoAsociado;
          nodoHijo.tieneRequerimientos=nivel.tieneRequerimientos;
          nodoHijo.cambioRegulatorioRegulacion=nivel.cambioRegulatorioRegulacion;
          nodoHijo.children=reqs;
          nodoHijo.coincidencias=coincidencias;
          nodoHijo.cargaInicial=true;
          nodoHijo.orden=nivel.orden;
          nodoHijo.ordenPadre=nivel.ordenPadre;
          nodoHijo.nombreNivel=nivel.nombreNivel;
          nodoHijo.estado=nivel.estado;
          nodo.children.push(nodoHijo);
        }
        //llamo nuevamente a este metodo agregando un nodo nuevo, y los nuevos hijos q ahora son padres, y TODOS los niveles nuevamente
        this.addRamaArbol(nodoHijo, nivelesSuperioresAux, nivelesEstructura, busqueda);
        nivelesSuperioresAux=[];      
      }
	}
}
