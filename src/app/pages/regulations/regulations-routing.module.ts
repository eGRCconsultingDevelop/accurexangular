import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ListComponent } from './list/list.component';
import { DetailComponent } from './detail/detail.component';
import { ModificationsComponent } from './modifications/modifications.component';
import { ApplyComponent } from './apply/apply.component';

const routes: Routes = [
  { path: 'list', component: ListComponent },
  { path: 'detail', component: DetailComponent },
  { path: 'modifications', component: ModificationsComponent },
  { path: 'apply', component: ApplyComponent },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class RegulationsRoutingModule { }
