import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator, MatTableDataSource } from '@angular/material';
import { CambioRegulatorioService } from 'src/app/service/cambio-regulatorio.service';
import { CambioRegulatorio } from 'src/app/shared/dto/CambioRegulatorio';
import { Router } from '@angular/router';
import { UsuarioService } from 'src/app/service/usuario.service';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss']
})
export class ListComponent implements OnInit {
  // @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  public switchHistory = 'modifies';
  public cambiosRegulatoriosPend:Array <CambioRegulatorio>;
  displayedColumns = ['name', 'description', 'regulations', 'new_regulation', 'date_change', 'baselines', 'author'];
  dataSource = new MatTableDataSource<any>([]);
  public carga:boolean=true;

  constructor(private cambioRegService:CambioRegulatorioService,private usuarioService:UsuarioService,private router: Router) { }

  ngOnInit() {
   
    this.consultarCambiosRegPendientes();
    // this.dataSource.paginator = this.paginator;
  }
  editarCambioReg(element){
    console.log(element);
    this.cambioRegService.cambioRegulatorio=element;
       this.router.navigate(['/regulations/detail']);
  }

  
  consultarCambiosRegPendientes(){
    this.cambioRegService.getConsultarCambiosRegulatoriosPendientes(this.usuarioService.usuario.nombreUsuario,this.usuarioService.usuario.cliente.idCliente).subscribe(data => {
      if(data.status==this.cambioRegService.codOK){
        this.cambiosRegulatoriosPend=data.response;
        console.log(this.cambiosRegulatoriosPend);
        this.datosTabla();
        this.carga=false;
       // this.dataSource.paginator = this.paginator;
      }else{
      this.cambioRegService.getManejoError(data);
      }
      })
  }

  datosTabla(){
    console.log("arreglo actualizado"+this.cambiosRegulatoriosPend);
 
      this.dataSource = new MatTableDataSource(this.cambiosRegulatoriosPend);  
      console.log(this.cambiosRegulatoriosPend);

  }
}
