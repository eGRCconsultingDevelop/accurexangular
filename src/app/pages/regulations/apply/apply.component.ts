import { Component, OnInit } from '@angular/core';
import { MatTableDataSource, MatDialog } from '@angular/material';
import { LineaBaseService } from 'src/app/service/lineabase.service';
import { LineaBase } from 'src/app/shared/dto/LineaBase';
import { ModalRegulationsComponent } from 'src/app/shared/components/modal-regulations/modal-regulations.component';
import { Regulacion } from 'src/app/shared/dto/Regulacion';
import { CambioRegulatorioService } from 'src/app/service/cambio-regulatorio.service';
import { UsuarioService } from 'src/app/service/usuario.service';

@Component({
  selector: 'app-apply',
  templateUrl: './apply.component.html',
  styleUrls: ['./apply.component.scss']
})
export class ApplyComponent implements OnInit {
  public switchHistory = 'modifies';

  displayedColumns = ['baseline', 'description', 'date_created', 'date_updated', 'select'];
  dataSource = new MatTableDataSource<any>([]);
  public lineasBase:Array <LineaBase>;
  public lineasBaseTemporales:Array <LineaBase>=[];
  public regulacion:Regulacion=this.cambioRegService.regulacionNueva;
  public lineasBaseSelec:Array<LineaBase>=[];
  constructor(private lineaBaseService:LineaBaseService,  
    private dialog: MatDialog,
    private usuarioService:UsuarioService,
    public cambioRegService:CambioRegulatorioService) { }

  ngOnInit() {
  this.consultarLineasBase();
  console.log(this.regulacion);
  }
  datosTabla(){
    console.log("arreglo actualizado"+this.lineasBaseTemporales);
 
      this.dataSource = new MatTableDataSource(this.lineasBaseTemporales);  
      console.log(this.lineasBaseTemporales);

  }
  consultarLineasBase(){
    this.lineaBaseService.getConsultarLineasBase(this.usuarioService.usuario.nombreUsuario,1000).subscribe(data => {
      if(data.status==this.lineaBaseService.codOK){
        this.lineasBase=data.response;
        console.log(this.lineasBase);

        for(let i=0;i<this.lineasBase.length;i++){
          if(this.lineasBase[i].estadoLineaBaseUsuario == "TEMPORAL"){
              this.lineasBaseTemporales.push(this.lineasBase[i]);
          }
        }
        this.datosTabla();
      }else{
      this.lineaBaseService.getManejoError(data);
      }
      })
  }
  seleccionarLineaBase(event,element){
    console.log(element);
    if(event.checked){
      var index=this.lineasBaseSelec.indexOf(element);
      if ( index == -1 ) {
        this.lineasBaseSelec.push(element);
        console.log(this.lineasBaseSelec);
      }
   
    }else{
      var index=this.lineasBaseSelec.indexOf(element);
      if ( index !== -1 ) {
        this.lineasBaseSelec.splice( index, 1 );
      }
      console.log(this.lineasBaseSelec);
    }
  }
  aplicarCambio(){
    this.cambioRegService.regulacionNueva=this.regulacion;
    this.cambioRegService.lineasBaseAfect=this.lineasBaseSelec;
   const dialogRef = this.dialog.open(ModalRegulationsComponent, {
      width: '750px',
      data: {}
    });
  }
}
