import { Component, OnInit } from '@angular/core';
import { MatTableDataSource } from '@angular/material';
import { CambioRegulatorioService } from 'src/app/service/cambio-regulatorio.service';
import { Regulacion } from 'src/app/shared/dto/Regulacion';
import { Router } from '@angular/router';
import { UsuarioService } from 'src/app/service/usuario.service';

@Component({
  selector: 'app-modifications',
  templateUrl: './modifications.component.html',
  styleUrls: ['./modifications.component.scss']
})
export class ModificationsComponent implements OnInit {
  public switchHistory = 'news';

  displayedColumns = ['name', 'description', 'date_pub', 'actions'];
  dataSource = new MatTableDataSource<any>([]);
  public regulaciones:Array<Regulacion>;
  public carga:boolean=true;
  
  constructor(public cambioRegService:CambioRegulatorioService,
    private usuarioService:UsuarioService,
    private router: Router) { }

  ngOnInit() {
this.consultarRegulacionesNuevas();
  }

  datosTabla(){
    console.log("arreglo actualizado"+this.regulaciones);
 
      this.dataSource = new MatTableDataSource(this.regulaciones);  
      console.log(this.regulaciones);

  }
    
  consultarRegulacionesNuevas(){
    this.cambioRegService.getConsultarRegNuevas(this.usuarioService.usuario.nombreUsuario,this.usuarioService.usuario.cliente.idCliente).subscribe(data => {
      if(data.status==this.cambioRegService.codOK){
        this.regulaciones=data.response;
        console.log(this.regulaciones);
        this.datosTabla();
        this.carga=false;
       // this.dataSource.paginator = this.paginator;
      }else{
      this.cambioRegService.getManejoError(data);
      }
      })
  }

  asociarLineaBase(element){
    this.cambioRegService.regulacionNueva=element;
    this.router.navigate(['/regulations/apply']);
  }
}

