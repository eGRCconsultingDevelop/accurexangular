import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MomentModule } from 'ngx-moment';

import { RegulationsRoutingModule } from './regulations-routing.module';

import { ListComponent } from './list/list.component';
import { DetailComponent } from './detail/detail.component';

import { SharedModule } from '../../shared/shared.module';
import { RegulationMaterialModules } from './regulations.material';
import { ModificationsComponent } from './modifications/modifications.component';
import { ApplyComponent } from './apply/apply.component';
import { FormsModule } from '@angular/forms';


@NgModule({
  declarations: [ListComponent, DetailComponent, ModificationsComponent, ApplyComponent],
  imports: [
    CommonModule,
    RegulationsRoutingModule,
    SharedModule,
    MomentModule,
    FormsModule,
    ...RegulationMaterialModules
  ]
})
export class RegulationsModule { }
