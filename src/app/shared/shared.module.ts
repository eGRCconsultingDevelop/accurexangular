import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { ReactiveFormsModule } from '@angular/forms';
import { MomentModule } from 'ngx-moment';

import { MaterialModules } from './material.imports';
import { HeaderComponent } from './components/header/header.component';
import { HeaderFilterSearchComponent } from './components/header-filter-search/header-filter-search.component';
import { ResultItemSearchComponent } from './components/result-item-search/result-item-search.component';
import { ResultItemConsultaComponent } from './components/result-item-consulta/result-item-consulta.component';
import { ModalComponent } from './components/modal/modal.component';

import { HeaderFilterBaselineComponent } from './components/header-filter-baseline/header-filter-baseline.component';
import { ModalSaveSearchComponent } from './components/modal-save-search/modal-save-search.component';
import { HeaderFilterFulfillmentComponent } from './components/header-filter-fulfillment/header-filter-fulfillment.component';
import { ModalSupportComponent } from './components/modal-support/modal-support.component';
import { ModalNotificationsComponent } from './components/modal-notifications/modal-notifications.component';
import { ModalRegulationsComponent } from './components/modal-regulations/modal-regulations.component';
import { AutoCompleteModule } from 'primeng/autocomplete';
import { FormsModule } from '@angular/forms';
import { ModalSaveBaselineComponent } from './components/modal-save-baseline/modal-save-baseline.component';
import { FilterSidebarComponent } from './components/filter-sidebar/filter-sidebar.component';
import { ModalIngoBaselineComponent } from './components/modal-ingo-baseline/modal-ingo-baseline.component';
import { ModalConfirmacionComponent } from './components/modal-confirmacion/modal-confirmacion.component';
import { ModalDetailComponent } from './components/modal-detail/modal-detail.component';
import { ModalBaselineComponent } from './components/modal-baseline/modal-baseline.component';
import { HighlightSearch } from 'src/app/shared/highlight.pipe';
import { ModalCumplimientoComponent } from './components/modal-cumplimiento/modal-cumplimiento.component';
import { ModalCumpliminetoConfirmarComponent } from './components/modal-cumplimiento-confirmar/modal-cumplimiento-confirmar.component';
import { ResultItemCambioRegComponent } from './components/result-cambio-reg/result-cambio-reg.component';
import { ModalCambioRegComponent } from './components/modal-cambio-reg/modal-cambio-reg.component';
import { ModalAdministrarCuentaComponent } from './components/modal-administrar-cuenta/modal-administrar-cuenta.component';
import { ModalSuscripcionComponent } from './components/modal-suscripcion/modal-suscripcion.component';
import { ModalValidadorComponent } from './components/modal-validador/modal-validador.component';

@NgModule({
  declarations: [
    HeaderComponent,
    HeaderFilterSearchComponent,
    ResultItemSearchComponent,
    ModalComponent,
    ModalConfirmacionComponent,
    ModalDetailComponent,
    ModalSaveSearchComponent,
    ModalBaselineComponent,
    ModalSaveBaselineComponent,
    HeaderFilterFulfillmentComponent,
    HeaderFilterBaselineComponent,
    ModalSupportComponent,
    ModalNotificationsComponent,
    ModalRegulationsComponent,
    ModalSaveBaselineComponent,
    ModalCumplimientoComponent,
    FilterSidebarComponent,
    ModalIngoBaselineComponent,
    ResultItemConsultaComponent,
    ResultItemCambioRegComponent,
    ModalCambioRegComponent,
    ModalCumpliminetoConfirmarComponent,
    ModalAdministrarCuentaComponent,
    ModalSuscripcionComponent,
    ModalValidadorComponent,
    HighlightSearch
  ],
  imports: [
    CommonModule,
    RouterModule,
    AutoCompleteModule,
    ReactiveFormsModule,
    FormsModule,
    MomentModule,
    ...MaterialModules
  ],
  exports: [
    HeaderComponent,
    HeaderFilterSearchComponent,
    ResultItemSearchComponent,
    ResultItemConsultaComponent,
    ResultItemCambioRegComponent,
    HeaderFilterBaselineComponent,
    ModalComponent,
    ModalConfirmacionComponent,
    ModalDetailComponent,
    ModalSaveSearchComponent,
    ModalBaselineComponent,
    ModalSaveBaselineComponent,
    HeaderFilterFulfillmentComponent,
    ModalSupportComponent,
    ModalSuscripcionComponent,
    ModalNotificationsComponent,
    ModalRegulationsComponent,
    ModalCumplimientoComponent,
    FilterSidebarComponent,
    ModalIngoBaselineComponent,
    ModalCambioRegComponent,
    ModalCumpliminetoConfirmarComponent,
    ModalAdministrarCuentaComponent,
    ModalValidadorComponent,
    HighlightSearch
  ],
  entryComponents: [
    ModalComponent,
    ModalConfirmacionComponent,
    ModalDetailComponent,
    ModalSaveSearchComponent,
    ModalBaselineComponent,
    ModalSupportComponent,
    ModalNotificationsComponent,
    ModalRegulationsComponent,
    ModalCumplimientoComponent,
    ModalIngoBaselineComponent,
    ModalCumpliminetoConfirmarComponent,
    ModalSaveBaselineComponent,
    ModalCambioRegComponent,
    ModalAdministrarCuentaComponent,
    ModalSuscripcionComponent,
    ModalValidadorComponent,
    ResultItemCambioRegComponent
    
  ]
})
export class SharedModule { }
