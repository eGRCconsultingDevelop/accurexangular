import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
    name: 'highlight'
})

export class HighlightSearch implements PipeTransform {

    transform(value: any, args: any): any {
        if (args && value) {
            return value.toLowerCase().replaceAll(args.toLowerCase(),"<mark>" + args + "</mark>");
        }
        return value;
    }
}