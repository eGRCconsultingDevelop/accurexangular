export class FiltrosBusqueda{
	palabraClavePrincipal: string = '';
	tag:String ='N';
	estado: string ='ACTIVO';
	idJurisdiccionesSel:Array<number> =[];
	idSectoresSel:Array<number> =[];
	idDominiosImpactoSel:Array<number> =[];
	idFuentesAutoridadSel:Array<number> =[];
    idRegulacionesSel:Array<number> =[];
    
    constructor(){}
}