import { FiltrosDetalleBusqueda } from 'src/app/shared/dto/FiltrosDetalleBusqueda';
export class Busqueda  {
    idBusqueda: number=0;
    nombreBusqueda: string='';
    descripcion: string='';
    idRegulacionesSeleccionadas: Array<number>=[];
    filtrosDetalle : FiltrosDetalleBusqueda= new FiltrosDetalleBusqueda();
    estado: string='';
    
    constructor(){}
}