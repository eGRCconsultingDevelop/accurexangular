import { FiltrosDetalleBusqueda } from './FiltrosDetalleBusqueda';
import { Regulacion } from './Regulacion';
import { Area } from './Area';
import { RiesgoAsociado } from './RiesgoAsociado';
import { EstadoRegulacion } from './EstadoRegulacion';

export class ResultadoBusqueda{
    idBusqueda:number=0;
    nombreBusqueda:string ='';
	descripcion:string='';
	riesgosAsociados:Array<RiesgoAsociado>=[];
    areasResponsables:Array<Area>=[];
    areasInvolucradas:Array<Area>=[];
	estadosRegulaciones:Array<EstadoRegulacion>=[];
	filtrosDetalleDTO:FiltrosDetalleBusqueda = new FiltrosDetalleBusqueda();
    regulacionesDTO:Array<Regulacion>=[];
    
    constructor(){}  
}