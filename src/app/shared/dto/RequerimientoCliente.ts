import { RequerimientoTotal } from './RequerimientoTotal';
import { Requerimiento } from './Requerimiento';

export class RequerimientoCliente{
    id:number=0;
	requerimiento:Requerimiento=new Requerimiento();
    idTextoUsuario:number=0;
    idLineaBaseUsuario:number=0;     
    estadoRequerimientoCliente:string='';
    criticidad:string='';
    nivelCumplimiento:String='';
    fechaCumplimiento:any='';
	politica:string='';
    numeralPolitica:string='';
    riesgo:string='';
    controles:string='';
    responsable:string='';
    correosElectronicosResponsable:string='';
    planAccion:string='';
    fechaEjecucionPlanAccion:any='';
    notificacionesActivo:string='';
    fechaInicioNotificaciones:any='';
    periodicidadNotificaciones:string='';
    correosElectronicosNotificaciones:string='';
	fechaActualizacion:any='';
    constructor(){}  
}