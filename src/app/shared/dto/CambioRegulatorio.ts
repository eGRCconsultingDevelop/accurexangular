import { LineaBaseSimple } from './LineaBaseSimple';
import { Regulacion } from './Regulacion';
import { Nivel } from './Nivel';

export class CambioRegulatorio{
    idCambioReg: number=0;
    nombreCambioReg: string='';
    descripcionCambio: string='';
    fechaCambio: any ='';
    lineasBaseAfectadas : Array <LineaBaseSimple>=[];
    nivelesModificados:Array <Nivel>=[];
    regulacionNueva: Regulacion= new Regulacion();
    idActualizacionVigente: number=0;
    constructor(){}
}