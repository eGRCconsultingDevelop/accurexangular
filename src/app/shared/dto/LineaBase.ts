import { Regulacion } from './Regulacion';

export class LineaBase{
    idLinea: number=0;
    nombreLineaBase: string='';
    descripcionLineaBase: string='';
    fechaCreacion: any ='';
    fechaActualizacion : any='';
    cambiosRegulatorios:Array <string>=[];
    fusionada:string="";
    idBusquedas:Array <number>=[];
    idTextos:Array <number>=[];
    nombreUsuario: string='';
    estadoCumplimiento:number=0;
    cumplimiento:number=0;
    regulaciones:Array <Regulacion>=[];
    estadoLineaBaseUsuario:string='';
    origen: string='';
   
    constructor(){}
}