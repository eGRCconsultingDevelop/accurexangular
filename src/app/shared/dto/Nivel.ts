import { Requerimiento } from 'src/app/shared/dto/Requerimiento';

export class Nivel{
	idNivel:number=0;
	nombreNivel:string ='';
	tieneRequerimientos:string ='';
	nivelSuperior:number =0;
	letraNegrita:string ='';
	letraNegritaTextoAsociado:string ='';
	idTexto:number=0;
	idTextoPadre:number=0;
	texto:string ='';
	textoAsociado:string ='';
	orden:number=0;
	ordenPadre:string='';
	estado:string ='';
	cambioRegulatorioRegulacion:number=0;
    requerimientos:Array<Requerimiento>=[];
    constructor(){}  
}