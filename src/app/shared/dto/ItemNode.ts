export class ItemNode{
    children: ItemNode[];
    id:number;
	nombre:string;
	letraNegrita:string;
    letraNegritaTextoAsociado:string;
    idTextoPadre:number;
	texto:string ='';
    textoAsociado:string ='';
    tieneRequerimientos:string='';
    type: string;
    coincidencias: boolean=false;
    nombreNivel:string ='';
    cargaInicial: boolean=true;
    idRegulacion:number=0
    orden:number=0;
    ordenPadre:string='';
    estado:string ='';
    cambioRegulatorioRegulacion:number=0;
    seleccionado:boolean=false;
    constructor(){}  
}