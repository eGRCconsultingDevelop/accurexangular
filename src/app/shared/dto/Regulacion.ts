import { Nivel } from 'src/app/shared/dto/Nivel';
export class Regulacion{

    id:number= 0;
	seleccionada:string = '';
	contratado:string = '';
	fuenteAutoridad:string = '';
	nombreRegulacion:string = '';
    nombreRegulacionNivelII:string = '';	
	url:string = '';
	riesgosAsociados:Array<string>=[];
    multasAsociadas:Array<string>=[];
	areasResponsables:Array<string>=[];
	areasInvolucradas:Array<string>=[];
	estado:string = '';
	fechaVigencia:any ='';
	fechaDerogado:any ='';
	abstractReg:string = '';
	sector:string = '';
	jurisdiccion:string = '';
	dominiosImpacto:Array<string>=[];
    niveles:Array<Nivel> =[];   
    
    constructor(){}  
}