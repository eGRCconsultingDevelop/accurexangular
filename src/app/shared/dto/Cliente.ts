import { Requerimiento } from 'src/app/shared/dto/Requerimiento';

export class Cliente{
	idCliente:number=0;
	nit:string ='';
	razonSocial:string ='';
	direccion:string ='';
	cantidadUsuarios:number=0;
	fechaLimitePago:string ='';
	tipoPago:string ='';
	fechaInicioContrato:string ='';
	fechaFinContrato:string ='';


    constructor(){}  
}