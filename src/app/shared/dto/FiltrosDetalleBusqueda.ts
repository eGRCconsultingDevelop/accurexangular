import { FiltrosBusqueda } from 'src/app/shared/dto/FiltrosBusqueda';

export class FiltrosDetalleBusqueda{
     fechaInicio:any='';
     fechaFin:any='';
     palabraClaveDetalle:string='';
     idRiesgosAsociadosSel:Array<number>=[];
	 idAreasResponsablesSel:Array<number>=[];
	 idAreasInvolucradasSel:Array<number>=[];
	 idEstadoRegulacionSel:number =0;
	 idRegulacionesSel:Array<number>=[];
	 filtros:FiltrosBusqueda= new FiltrosBusqueda();
	 nombreRegulacion:string='';
	 estado:string='ACTIVO';  

    constructor(){}    
}