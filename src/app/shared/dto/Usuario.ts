import { Requerimiento } from 'src/app/shared/dto/Requerimiento';
import { Cliente } from './Cliente';

export class Usuario{
	nombreUsuario:string ='';
	correo:string ='';
	nombres:string ='';
	apellidos:string ='';
	estado:string ='';
	tiempoSesion:number=0;
	periodoCaducidadContrasena:number=0;
	idRol:number=0;
	rol:string ='';
	token:string ='';
	contrasena:string ='';
	contrasenaNueva:string ='';
	cliente:Cliente=new Cliente();
    constructor(){}  
}