import { Component, OnInit, Inject } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef, MatDialog } from '@angular/material';
import { BusquedaService } from 'src/app/service/busqueda.service';
import { Regulacion } from '../../dto/Regulacion';
import { Busqueda } from '../../dto/Busqueda';
import { Router } from '@angular/router';
import { FiltrosDetalleBusqueda } from '../../dto/FiltrosDetalleBusqueda';
import { FiltrosBusqueda } from '../../dto/FiltrosBusqueda';
import { ModalValidadorComponent } from '../modal-validador/modal-validador.component';
import { BusquedaUsuario } from '../../dto/BusquedaUsuario';
import { UsuarioService } from 'src/app/service/usuario.service';


@Component({
  selector: 'app-modal-save-search',
  templateUrl: './modal-save-search.component.html',
  styleUrls: ['./modal-save-search.component.scss']
})



export class ModalSaveSearchComponent implements OnInit {

  public regulacionesSelec: Array<Regulacion>= [];
  public fuentesAutoridad: Array<String>=[];
  public busqueda : Busqueda = new Busqueda();
  public nombreBusqueda:String;
  public descripcion:String;
  public fuentesFiltradas: Array<String>;
  public busquedasUsuarioGuardadas: Array<BusquedaUsuario>=[];
  public nombreIgual:boolean=false;

  constructor(private busquedaService:BusquedaService,
    public dialog: MatDialog,
    public dialogRef: MatDialogRef<ModalSaveSearchComponent>,
    private usuarioService:UsuarioService,
    @Inject(MAT_DIALOG_DATA) public data: any , private router: Router) {
    this.busqueda.idBusqueda= this.busquedaService.resultadoBusqueda.idBusqueda;
    this.busqueda.nombreBusqueda= this.busquedaService.resultadoBusqueda.nombreBusqueda;
    this.busqueda.descripcion= this.busquedaService.resultadoBusqueda.descripcion;
    this.busqueda.filtrosDetalle= this.busquedaService.resultadoBusqueda.filtrosDetalleDTO;
    this.busqueda.estado="ACTIVO";
   }

  
  ngOnInit() {
    this.consultarBusquedasGuardadas();
    console.log(this.busquedaService.resultadoBusqueda.filtrosDetalleDTO.idRegulacionesSel);
    for(let i=0;i<this.busquedaService.resultadoBusqueda.regulacionesDTO.length;i++){
      for(let j=0;j<this.busquedaService.resultadoBusqueda.filtrosDetalleDTO.idRegulacionesSel.length;j++){
      
      if(this.busquedaService.resultadoBusqueda.regulacionesDTO[i].id ==  this.busquedaService.resultadoBusqueda.filtrosDetalleDTO.idRegulacionesSel[j]){
        this.regulacionesSelec.push(this.busquedaService.resultadoBusqueda.regulacionesDTO[i]);
      }
      }
    }
    for(let i=0;i<this.regulacionesSelec.length;i++){
      this.fuentesAutoridad.push(this.regulacionesSelec[i].fuenteAutoridad);
    }
    
    this.fuentesFiltradas = this.fuentesAutoridad.filter((el, index) => this.fuentesAutoridad.indexOf(el) === index);
    console.log(this.fuentesAutoridad);
    console.log(this.fuentesFiltradas);
    for(let i=0;i<this.regulacionesSelec.length;i++){
      this.busqueda.idRegulacionesSeleccionadas.push(this.regulacionesSelec[i].id);
    }

  }

  close(): void {
    this.dialogRef.close();
  }
  guardarBusqueda() {
  
   /* if(this.busqueda.filtrosDetalle.filtros.estado == null){
      this.busqueda.filtrosDetalle.filtros= new FiltrosBusqueda();
    }*/

   for(let i=0;i<this.busquedasUsuarioGuardadas.length;i++){
      if(this.busquedasUsuarioGuardadas[i].nombreBusqueda == this.busqueda.nombreBusqueda){
        this.nombreIgual=true;
        console.log("Son iguales");
      }else{
        this.nombreIgual=false;
      }

   }

   if(this.nombreIgual){
    const dialogRef = this.dialog.open(ModalValidadorComponent, {
      width: '500px',
      data: {
        body1:"Ya existe una busqueda con este nombre",
        title:"Busquedas iguales",
        acceptButton:"Aceptar"
      }
    });

    this.nombreIgual=false;

   }else if(this.busqueda.nombreBusqueda == ""  && this.busqueda.descripcion != ""){
    const dialogRef = this.dialog.open(ModalValidadorComponent, {
      width: '500px',
      data: {
        body1:"El campo Nombre de la búsqueda esta vacio",
        title:"Campos vacios",
        acceptButton:"Aceptar"
      }
    });
   }else if(this.busqueda.descripcion == ""  && this.busqueda.nombreBusqueda != ""){
    const dialogRef = this.dialog.open(ModalValidadorComponent, {
      width: '500px',
      data: {
        body1:"El campo Descripción de la búsqueda esta vacio",
        title:"Campos vacios",
        acceptButton:"Aceptar"
      }
    });

   }else if(this.busqueda.nombreBusqueda== "" && this.busqueda.descripcion == ""){
    const dialogRef = this.dialog.open(ModalValidadorComponent, {
      width: '500px',
      data: {
        body1:"Los campos Nombre de la búsqueda y Descripción de la búsqueda estan vacios",
        title:"Validación de campos",
        acceptButton:"Aceptar"
      }
    });
  }else{
    
    console.log(this.busqueda.filtrosDetalle.filtros);
    console.log(this.busqueda.idRegulacionesSeleccionadas);
        this.busquedaService.getguardarBusqueda(this.usuarioService.usuario.nombreUsuario,this.busqueda).subscribe(data => {
          if(data.status==this.busquedaService.codOK){
            console.log(this.busqueda);  
            this.dialogRef.close();
            this.busquedaService.resultadoBusqueda=null;
            this.router.navigate(['/search/history']);
          }else{
            console.log(data);
           this.busquedaService.getManejoError(data);
          }
          })
        }
  }

  onCheckChange(event,id) {
    console.log(id);
    if(event.checked){
      this.busqueda.idRegulacionesSeleccionadas.push(id);
      console.log(this.busqueda.idRegulacionesSeleccionadas);
    }else{
    
      var index=this.busqueda.idRegulacionesSeleccionadas.indexOf(id);
      if ( index !== -1 ) {
        this.busqueda.idRegulacionesSeleccionadas.splice( index, 1 );
      }
      console.log(this.busqueda.idRegulacionesSeleccionadas);
    }
    
  }

  consultarBusquedasGuardadas(){
 
    this.busquedaService.getBusquedasEstado(this.usuarioService.usuario.nombreUsuario,"GUARDADA").subscribe(data => {
      if(data.status==this.busquedaService.codOK){
      this.busquedasUsuarioGuardadas=data.response;
      console.log(this.busquedasUsuarioGuardadas);
     // this.showDialog();
      }else{
      this.busquedaService.getManejoError(data);
      }
      })
  }

}
