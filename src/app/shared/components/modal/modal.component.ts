import { Component, OnInit, Inject } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';
import { HistoryComponent } from '../../../pages/search/history/history.component';
import { BusquedaService } from 'src/app/service/busqueda.service';
import { BusquedaUsuario } from 'src/app/shared/dto/BusquedaUsuario';
import { Router } from '@angular/router';
import { LineaBaseService } from 'src/app/service/lineabase.service';
import { UsuarioService } from 'src/app/service/usuario.service';
@Component({
  selector: 'app-modal',
  templateUrl: './modal.component.html',
  styleUrls: ['./modal.component.scss']
})
export class ModalComponent implements OnInit {

  public busquedasUsuario: Array<BusquedaUsuario>=[];

  constructor(
    public dialogRef: MatDialogRef<ModalComponent>,
    private busquedaService: BusquedaService,
    private lineaBaseService:LineaBaseService,
    private router: Router,
    private usuarioService:UsuarioService,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) { }

  ngOnInit() {
    console.log(this.data);
  }

  close(type): void {
    this.data.reason = type;
    this.busquedaService.respuestaModal='';
    console.log(this.data.reason);
    if(this.data.reason == "cancel"){
      this.busquedaService.eliminar=false;
      this.dialogRef.close(this.data);
    }else{
      if(this.data.tipo =="Busqueda"){
      this.busquedaService.getBorrarBusquedaUsuario(this.usuarioService.usuario.nombreUsuario,this.data.id).subscribe(data => {
        if(data.status==this.busquedaService.codOK){
    
 
          this.router.navigateByUrl('/search/history', { skipLocationChange: true }).then(() => {
            this.router.navigate(['/search/history']);
            this.dialogRef.close(this.data);
        });
        }else{
        this.busquedaService.getManejoError(data);
        this.dialogRef.close(this.data);
        }
     
        })
    }else{
      this.lineaBaseService.getBorrarLineaBaseUsuario(this.usuarioService.usuario.nombreUsuario,this.data.id).subscribe(data => {
        if(data.status==this.lineaBaseService.codOK){
      
          this.router.navigateByUrl('/baseline', { skipLocationChange: true }).then(() => {
          this.router.navigate(['/baseline']);
          this.dialogRef.close(this.data);
        });
        }else{
        this.lineaBaseService.getManejoError(data);
        this.dialogRef.close(this.data);
        }
    
        })
    }
  }

    
    
  }

}
