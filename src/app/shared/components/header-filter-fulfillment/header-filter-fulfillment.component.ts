import { Component, OnInit } from '@angular/core';
import { Regulacion } from '../../dto/Regulacion';
import { CumplimientoService } from 'src/app/service/cumplimiento.service';
import { RequerimientoCliente } from '../../dto/RequerimientoCliente';
import { Router } from '@angular/router';
import { UsuarioService } from 'src/app/service/usuario.service';

@Component({
  selector: 'app-header-filter-fulfillment',
  templateUrl: './header-filter-fulfillment.component.html',
  styleUrls: ['./header-filter-fulfillment.component.scss']
})
export class HeaderFilterFulfillmentComponent implements OnInit {
  public options: any = [];
  public regulaciones: Array<Regulacion>=this.cumplimientoService.lineaBase.regulaciones;
  public requerimientosClienteTotal:Array<RequerimientoCliente>=[];
  public requerimientosCliente:Array<RequerimientoCliente>=[];
  public responsables:Array <string>=this.cumplimientoService.responsables;
  public requerimiento: string;
  public controles: string;
  public email: string;
  public plan: string;
  public criticidad:Array<string>;
  public nivelCumplimiento:Array<string>;
  public responsableSelec:Array<string>;
  public notificacion:Array<string>;
  public requerimientosFiltrados:Array<RequerimientoCliente>=[];
  constructor(public cumplimientoService:CumplimientoService,  
    private usuarioService:UsuarioService,
    private router: Router) { }

  ngOnInit() {
    console.log(this.regulaciones);
    console.log(this.responsables);
    for(let i=0;i<this.regulaciones.length;i++){ 
      this.consultarRequerimientos(this.regulaciones[i].id);
      }
  }


  consultarRequerimientos(id:number){
    console.log(id);
   // requerimientosAny:Array<any>
    this.cumplimientoService.getConsultarRequerimientos(id,this.usuarioService.usuario.nombreUsuario).subscribe(data => {
      if(data.status==this.cumplimientoService.codOK){
      this.requerimientosCliente=data.response;
      console.log(this.requerimientosCliente);
      for(let i=0;i<this.requerimientosCliente.length;i++){ 
        this.requerimientosClienteTotal.push(this.requerimientosCliente[i]);
      }
      }else{
      this.cumplimientoService.getManejoError(data);
      }
      })
      
  }

  filtrar(){

    this.requerimientosFiltrados=[];
    console.log(this.requerimiento);
    console.log(this.controles);
    console.log(this.email);
    console.log(this.plan);
    console.log(this.criticidad);
    console.log(this.nivelCumplimiento);
    console.log(this.notificacion);
    console.log(this.responsableSelec);
    for(let i=0;i<this.requerimientosClienteTotal.length;i++){
      for(let h=0;h<1;h++){
        let band=0;
        if( this.controles != undefined){
          if(this.controles == this.requerimientosClienteTotal[i].controles){
            band=1;
          }else{
            band=0;
            break;
          }
        }
          if(this.criticidad != undefined){
              for(let j=0;j<this.criticidad.length;j++){
                if(this.criticidad[j] == this.requerimientosClienteTotal[i].criticidad){
                 band=1;
                 break;
                 }else{
                  band=0;
                 }
             }
             if(band==0){
              break;    
             }
           }
  
          if( this.requerimiento != undefined){
           if(this.requerimiento == this.requerimientosClienteTotal[i].requerimiento.requerimiento){
              band=1;
            }else{
             band=0;
             break;
           }
          }
  
          if( this.responsableSelec != undefined){
            for(let j=0;j<this.responsableSelec.length;j++){
              if(this.responsableSelec[j] == this.requerimientosClienteTotal[i].responsable){
               band=1;
               break;
               }else{
                band=0;
               }
           }
           if(band==0){
            break;    
           }
         }
  
         if( this.nivelCumplimiento != undefined){
          for(let j=0;j<this.nivelCumplimiento.length;j++){
            if(this.nivelCumplimiento[j] == this.requerimientosClienteTotal[i].nivelCumplimiento){
             band=1;
             break;
             }else{
              band=0;
              
             }
         }
         if(band==0){
          break;    
         }
       }
      
       if( this.email != undefined){
        if(this.email == this.requerimientosClienteTotal[i].correosElectronicosResponsable){
           band=1;
         }else{
          band=0;
          break;
        }
       }
  
       if(this.plan != undefined){
        if(this.plan == this.requerimientosClienteTotal[i].planAccion){
           band=1;
         }else{
          band=0;
        }
       }
  
       if( this.notificacion != undefined){
        for(let j=0;j<this.notificacion.length;j++){
          if(this.notificacion[j] == this.requerimientosClienteTotal[i].notificacionesActivo){
           band=1;
           break;
           }else{
            band=0;
           }
       }
       if(band==0){
        break;    
       }
     }
  
     console.log(band);
          if(band==1){
            this.requerimientosFiltrados.push(this.requerimientosClienteTotal[i]);
            console.log(this.requerimientosFiltrados);
            console.log(band);
          }
      }
     
  }

  this.router.navigateByUrl('/fulfillment', { skipLocationChange: true }).then(() => {
    this.router.navigate(['/fulfillment']);
    });

}

}
