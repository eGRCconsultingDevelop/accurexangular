import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Router } from '@angular/router';
import { BusquedaService } from 'src/app/service/busqueda.service';
import { MatTableDataSource, MatPaginator, MatDialog } from '@angular/material';
import { ModalDetailComponent } from 'src/app/shared/components/modal-detail/modal-detail.component';
import { ModalIngoBaselineComponent } from '../modal-ingo-baseline/modal-ingo-baseline.component';
import { CambioRegulatorioService } from 'src/app/service/cambio-regulatorio.service';
@Component({
  selector: 'app-result-cambio-reg',
  templateUrl: './result-cambio-reg.component.html',
  styleUrls: ['./result-cambio-reg.component.scss']
})
export class ResultItemCambioRegComponent implements OnInit {
  @Input() public item: any = {};
  @Input() public withLink: boolean = true;
  @Output() public checkChange = new EventEmitter<any>();
  
  public autores:Array <string>=this.cambioRegService.autores;
  public lineasBase:Array <string>=this.cambioRegService.lineasBase;
  public dominioImpacto:Array<String>;
  constructor(public busquedaService:BusquedaService ,public dialog: MatDialog,private cambioRegService : CambioRegulatorioService, private router: Router) { }

  ngOnInit() {
   
    console.log(this.cambioRegService.autores)
    console.log(this.cambioRegService.lineasBase)
  }

  openInfoDialog(id) {
    this.busquedaService.regulacion=id;
    const dialogRef = this.dialog.open(ModalIngoBaselineComponent, {
      width: '900px'
    });
  }
}
