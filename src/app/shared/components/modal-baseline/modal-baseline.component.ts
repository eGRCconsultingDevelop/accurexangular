import { Component, OnInit, Inject } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';
import { BusquedaService } from 'src/app/service/busqueda.service';
import { BusquedaUsuario } from 'src/app/shared/dto/BusquedaUsuario';
import { Router } from '@angular/router';
@Component({
  selector: 'app-modal-baseline',
  templateUrl: './modal-baseline.component.html',
  styleUrls: ['./modal-baseline.component.scss']
})
export class ModalBaselineComponent implements OnInit {

  public busquedasUsuario: Array<BusquedaUsuario>=[];

  constructor(
    public dialogRef: MatDialogRef<ModalBaselineComponent>,
    private busquedaService: BusquedaService,
    private router: Router,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) { }

  ngOnInit() {
    console.log(this.data);
  }

  close(type): void {
    this.data.reason = type;
    console.log(this.data.reason);  
     this.dialogRef.close(this.data);
  }

}
