import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Router } from '@angular/router';
import { BusquedaService } from 'src/app/service/busqueda.service';
@Component({
  selector: 'app-result-item-consulta',
  templateUrl: './result-item-consulta.component.html',
  styleUrls: ['./result-item-consulta.component.scss']
})
export class ResultItemConsultaComponent implements OnInit {
  @Input() public item: any = {};
  @Input() public withLink: boolean = true;
  @Output() public checkChange = new EventEmitter<any>();
 
  constructor( private busquedaService : BusquedaService, private router: Router) { }

  ngOnInit() {
  }

  onCheckChange(event) {
    
    this.checkChange.emit({
      item: this.item,
      check_state: event
    });
  }

  verDetalle(event){
    console.log(event);

    for(let i=0;i < this.busquedaService.resultadoBusqueda.regulacionesDTO.length; i++){
      if(this.busquedaService.resultadoBusqueda.regulacionesDTO[i].id == event.id){
      this.busquedaService.regulacion=this.busquedaService.resultadoBusqueda.regulacionesDTO[i];
        }
    }
    console.log(this.busquedaService.regulacion);
  
   
    this.router.navigate(['/search/consulta-detail']);
  }

}
