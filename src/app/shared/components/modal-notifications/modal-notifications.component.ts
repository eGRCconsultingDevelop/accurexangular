import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { CumplimientoService } from 'src/app/service/cumplimiento.service';
import { RequerimientoCliente } from '../../dto/RequerimientoCliente';
import { DatePipe } from '@angular/common';
import { UsuarioService } from 'src/app/service/usuario.service';

@Component({
  selector: 'app-modal-notifications',
  templateUrl: './modal-notifications.component.html',
  styleUrls: ['./modal-notifications.component.scss']
})
export class ModalNotificationsComponent implements OnInit {
  public requerimientoCliente:RequerimientoCliente=this.cumplimientoService.requerimientoCliente;
  constructor(
    public dialogRef: MatDialogRef<ModalNotificationsComponent>,
    private cumplimientoService:CumplimientoService,
    private usuarioService:UsuarioService,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) { }

  ngOnInit() {
    console.log(this.requerimientoCliente);
  }

  close(): void {
    this.dialogRef.close();
  }
  guardarNotificacion() {

    
    this.requerimientoCliente.fechaInicioNotificaciones=this.transform(this.requerimientoCliente.fechaInicioNotificaciones);
    console.log(this.requerimientoCliente.fechaInicioNotificaciones);
    this.cumplimientoService.getGuardarTextoReqCliente(this.usuarioService.usuario.nombreUsuario,this.requerimientoCliente).subscribe(data => {
      if(data.status==this.cumplimientoService.codOK){
        console.log(data);
        this.dialogRef.close();
      }else{
      this.cumplimientoService.getManejoError(data);
      }
      })
   /* if(this.busqueda.filtrosDetalle.filtros.estado == null){
      this.busqueda.filtrosDetalle.filtros= new FiltrosBusqueda();
    }*/

  }
  opcionSeleccionadaCumpli(event){
    console.log(event);
    this.requerimientoCliente.nivelCumplimiento=event;

  }
  transform(value: string) {
        
    if(value.length == undefined){
      var datePipe = new DatePipe("en-US");
      value = datePipe.transform(value, 'dd/MM/yyyy');
      return value;
    }else{
    return value;
    }
 }
  opcionSeleccionadaCriti(event){
    console.log(event);
    this.requerimientoCliente.criticidad=event;

  }

  onCheckChange(event) {
    console.log(event.checked);
    if(event.checked){
    this.requerimientoCliente.notificacionesActivo="S";
    }else{
      this.requerimientoCliente.notificacionesActivo="N";
    }

    
  }
  
}
