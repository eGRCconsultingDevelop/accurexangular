import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Router } from '@angular/router';
import { BusquedaService } from 'src/app/service/busqueda.service';
import { MatTableDataSource, MatPaginator, MatDialog } from '@angular/material';
import { ModalDetailComponent } from 'src/app/shared/components/modal-detail/modal-detail.component';
@Component({
  selector: 'app-result-item-search',
  templateUrl: './result-item-search.component.html',
  styleUrls: ['./result-item-search.component.scss']
})
export class ResultItemSearchComponent implements OnInit {
  @Input() public item: any = {};
  @Input() public withLink: boolean = true;
  @Output() public checkChange = new EventEmitter<any>();
  
  public dominioImpacto:Array<String>;
  constructor( public dialog: MatDialog,private busquedaService : BusquedaService, private router: Router) { }

  ngOnInit() {
    this.dominioImpacto=this.item.impacts;
    console.log(this.dominioImpacto);
    console.log(this.item.habilitar);
    console.log(this.busquedaService.dominiosImpacto);
    if(this.item.habilitar == "Optional[S]"){

    }else{
      
    }
    /*for(let j=0;j<this.busquedaService.cantidadDominiosSelec;j++){
       for(let i=0;i<this.dominioImpacto.length;i++){
    
      if(this.busquedaService.dominiosImpacto[j].nombreDominioImpacto == this.item.impacts[i] && this.busquedaService.dominiosImpacto[j].habilitar){
        this.item.habilitar=true;
        console.log(this.item.habilitar);
      }else if(this.busquedaService.dominiosImpacto[j].nombreDominioImpacto == this.item.impacts[i] && !this.busquedaService.dominiosImpacto[j].habilitar){
        this.item.habilitar=false;
        console.log(this.item.habilitar);
      }
      }
    }

    */
  }
  ventanaEmergente(){
    let respuestaDialogo='';
    const dialogRef = this.dialog.open(ModalDetailComponent, {
      width: '500px',
      data: {
        title: 'Confirmación',
        body1: `La regulación pertenece a un dominio de impacto que no esta contratado`, 
        acceptButton: 'Aceptar',
    
      }
    });
  }
  onCheckChange(event) {
    
    this.checkChange.emit({
      item: this.item,
      check_state: event
    });
  }

  verDetalle(event){
    console.log(event);

    for(let i=0;i < this.busquedaService.resultadoBusqueda.regulacionesDTO.length; i++){
      if(this.busquedaService.resultadoBusqueda.regulacionesDTO[i].id == event.id){
      this.busquedaService.regulacion=this.busquedaService.resultadoBusqueda.regulacionesDTO[i];
        }
    }
    console.log(this.busquedaService.regulacion);
    this.router.navigate(['/search/detail']);
  //  window.open('/search/detail');
  }

}
