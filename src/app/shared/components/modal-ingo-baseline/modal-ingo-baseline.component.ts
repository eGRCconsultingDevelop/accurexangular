import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { Regulacion } from '../../dto/Regulacion';
import { BusquedaService } from 'src/app/service/busqueda.service';
import { BusquedaUsuario } from '../../dto/BusquedaUsuario';

@Component({
  selector: 'app-modal-ingo-baseline',
  templateUrl: './modal-ingo-baseline.component.html',
  styleUrls: ['./modal-ingo-baseline.component.scss']
})
export class ModalIngoBaselineComponent implements OnInit {
  public regulacion : Regulacion = this.busquedaService.regulacion;
  public mockItem: any ;

  constructor(
    public dialogRef: MatDialogRef<ModalIngoBaselineComponent>,private busquedaService:BusquedaService,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) { 

    var fechaDerogado = this.regulacion.fechaDerogado.split("/", 3); 
  
    var fechaVigencia = this.regulacion.fechaVigencia.split("/", 3); 

    if(this.regulacion.estado == "DEROGADO"){
    this.mockItem ={
      id: this.regulacion.id,
      title: this.regulacion.fuenteAutoridad,
      check_title: this.regulacion.nombreRegulacion,
      info: [
        {
          name: 'Riesgos asociados',
          value: this.regulacion.riesgosAsociados,
        },
        {
          name: 'Multas asociadas',
          value: this.regulacion.multasAsociadas,
        },
        {
          name: 'Areas Responsables',
          value:  this.regulacion.areasResponsables,
        },
        {
          name: 'Areas Involucradas',
          value: this.regulacion.areasInvolucradas,
        },
      ],
      validity: {
        state: this.regulacion.estado,
        date: {
          day: fechaDerogado[1],
            month: fechaDerogado[0],
            year: fechaDerogado[2] 
        }
      },
      url:this.regulacion.url,
      resume: this.regulacion.abstractReg,
      sectors: [this.regulacion.sector],
      jurisdictions: [this.regulacion.jurisdiccion],
      impacts: [this.regulacion.dominiosImpacto]
    };
  }else{
    this.mockItem ={
      id: this.regulacion.id,
      title: this.regulacion.fuenteAutoridad,
      check_title: this.regulacion.nombreRegulacion,
      info: [
        {
          name: 'Riesgos asociados',
          value: this.regulacion.riesgosAsociados,
        },
        {
          name: 'Multas asociadas',
          value: this.regulacion.multasAsociadas,
        },
        {
          name: 'Areas Responsables',
          value:  this.regulacion.areasResponsables,
        },
        {
          name: 'Areas Involucradas',
          value: this.regulacion.areasInvolucradas,
        },
      
      ],
      validity: {
        state: this.regulacion.estado,
        date: {
          day: fechaVigencia[1],
            month: fechaVigencia[0],
            year: fechaVigencia[2] 
        }
      },
      url:this.regulacion.url,
      resume: this.regulacion.abstractReg,
      sectors: [this.regulacion.sector],
      jurisdictions: [this.regulacion.jurisdiccion],
      impacts: [this.regulacion.dominiosImpacto]
    };

  }
  }
  ngOnInit() {
  }

  close(type = null): void {
    this.dialogRef.close();
  }

}
