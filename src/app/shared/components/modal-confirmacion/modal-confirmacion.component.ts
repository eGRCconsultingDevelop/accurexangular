import { Component, OnInit, Inject } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';
import { HistoryComponent } from '../../../pages/search/history/history.component';
import { BusquedaService } from 'src/app/service/busqueda.service';
import { BusquedaUsuario } from 'src/app/shared/dto/BusquedaUsuario';
import { Router } from '@angular/router';
import { UsuarioService } from 'src/app/service/usuario.service';
@Component({
  selector: 'app-modal-confirmacion',
  templateUrl: './modal-confirmacion.component.html',
  styleUrls: ['./modal-confirmacion.component.scss']
})
export class ModalConfirmacionComponent implements OnInit {

  public busquedasUsuario: Array<BusquedaUsuario>=[];

  constructor(
    public dialogRef: MatDialogRef<ModalConfirmacionComponent>,
    private busquedaService: BusquedaService,
    private router: Router,
    private usuarioService:UsuarioService,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) { }

  ngOnInit() {
    console.log(this.data);
  }

  close(type): void {
    this.data.reason = type;
    this.busquedaService.respuestaModal='';
    console.log(this.data.id);
    if(this.data.reason == "accept"){

      this.busquedaService.getBusquedaUsuario(this.usuarioService.usuario.nombreUsuario,this.data.id,"ACTIVO").subscribe(data => {
        if(data.status==this.busquedaService.codOK){
        this.busquedaService.resultadoBusqueda=data.response;
        console.log(this.busquedaService.resultadoBusqueda);
        this.busquedaService.eliminar=false;
        this.dialogRef.close(this.data);
        this.router.navigate(['/search']);
      
        }else{
        this.busquedaService.getManejoError(data);
        }
        })
    
    }else{
      this.busquedaService.getBorrarBusquedaUsuario(this.usuarioService.usuario.nombreUsuario,this.data.id).subscribe(data => {
        if(data.status==this.busquedaService.codOK){
          for (let i = 0; i < this.busquedasUsuario.length; i++) {
            if(this.busquedasUsuario[i].idBusqueda == this.data.id){
              this.busquedasUsuario.splice(i,1);
              this.busquedaService.respuestaModal='cancel';
            }
          }
 
          this.router.navigateByUrl('/search/history', { skipLocationChange: true }).then(() => {
            this.router.navigate(['/search/history']);
        });
        }else{
        this.busquedaService.getManejoError(data);
        }
        this.dialogRef.close(this.data);
        })
    }

    
    
  }

}
