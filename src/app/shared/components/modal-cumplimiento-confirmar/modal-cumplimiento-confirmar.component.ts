import { Component, OnInit, Inject } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';
import { BusquedaService } from 'src/app/service/busqueda.service';
import { BusquedaUsuario } from 'src/app/shared/dto/BusquedaUsuario';
import { Router } from '@angular/router';
@Component({
  selector: 'app-modal-cumplimiento-confirmar',
  templateUrl: './modal-cumplimiento-confirmar.component.html',
  styleUrls: ['./modal-cumplimiento-confirmar.component.scss']
})
export class ModalCumpliminetoConfirmarComponent implements OnInit {

  public busquedasUsuario: Array<BusquedaUsuario>=[];

  constructor(
    public dialogRef: MatDialogRef<ModalCumpliminetoConfirmarComponent>,
    private busquedaService: BusquedaService,
    private router: Router,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) { }

  ngOnInit() {
    console.log(this.data);
  }

  close(type): void {
    this.data.reason = type;
    console.log(this.data.reason);
 
   // this.router.navigate(['/fulfillment/list']);
      

  }

}
