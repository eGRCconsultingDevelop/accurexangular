import { Component, OnInit, Inject } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';
import { BusquedaService } from 'src/app/service/busqueda.service';
import { Regulacion } from '../../dto/Regulacion';
import { Busqueda } from '../../dto/Busqueda';
import { Router } from '@angular/router';
import { FiltrosDetalleBusqueda } from '../../dto/FiltrosDetalleBusqueda';
import { FiltrosBusqueda } from '../../dto/FiltrosBusqueda';
import { UsuarioService } from 'src/app/service/usuario.service';


@Component({
  selector: 'app-modal-administrar-cuenta',
  templateUrl: './modal-administrar-cuenta.component.html',
  styleUrls: ['./modal-administrar-cuenta.component.scss']
})



export class ModalAdministrarCuentaComponent implements OnInit {

  public iniciales:string;
  constructor(private busquedaService:BusquedaService,
    public dialogRef: MatDialogRef<ModalAdministrarCuentaComponent>,
    private usuarioService:UsuarioService,
    @Inject(MAT_DIALOG_DATA) public data: any , private router: Router) {
    
   }

  
  ngOnInit() {
    
    this.iniciales=this.usuarioService.usuario.nombres[0].toUpperCase()+this.usuarioService.usuario.apellidos[0].toUpperCase();
    console.log(this.iniciales)
  }

  close(): void {
    this.dialogRef.close();
  }
 
  cambiarContrasena(){
    this.router.navigate(['/password/change']);
    this.dialogRef.close();
  }
}
