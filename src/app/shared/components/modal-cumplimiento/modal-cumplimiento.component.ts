import { Component, OnInit, Inject } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';
import { BusquedaService } from 'src/app/service/busqueda.service';
import { Regulacion } from '../../dto/Regulacion';
import { Busqueda } from '../../dto/Busqueda';
import { Router } from '@angular/router';
import { FiltrosDetalleBusqueda } from '../../dto/FiltrosDetalleBusqueda';
import { FiltrosBusqueda } from '../../dto/FiltrosBusqueda';
import { CumplimientoService } from 'src/app/service/cumplimiento.service';
import { Requerimiento } from '../../dto/Requerimiento';
import { RequerimientoCliente } from '../../dto/RequerimientoCliente';
import * as moment from 'moment';
import { DatePipe } from '@angular/common';
import {DateAdapter, MAT_DATE_FORMATS, MAT_DATE_LOCALE} from '@angular/material/core';
import {MatDatepicker} from '@angular/material/datepicker';
import { UsuarioService } from 'src/app/service/usuario.service';

export const MY_FORMATS = {
  parse: {
    dateInput: 'ddd/MM/YYYY',
  },
  display: {
    dateInput: 'dd/MM/YYYY',
    monthYearLabel: 'MMM YYYY',
    dateA11yLabel: 'LL',
    monthYearA11yLabel: 'MMMM YYYY',
  },
};

@Component({
  selector: 'app-modal-cumplimiento',
  templateUrl: './modal-cumplimiento.component.html',
  styleUrls: ['./modal-cumplimiento.component.scss'],
  providers: [
    // `MomentDateAdapter` can be automatically provided by importing `MomentDateModule` in your
    // application's root module. We provide it at the component level here, due to limitations of
    // our example generation script.
    

    {provide: MAT_DATE_FORMATS, useValue: MY_FORMATS},
  ],
})



export class ModalCumplimientoComponent implements OnInit {

  public regulacionesSelec: Array<Regulacion>= [];
  public fuentesAutoridad: Array<String>=[];
  public busqueda : Busqueda = new Busqueda();
  public nombreBusqueda:String;
  public descripcion:String;
  public fuentesFiltradas: Array<String>;
  public requerimientoCliente:RequerimientoCliente=this.cumplimientoService.requerimientoCliente;
  constructor(private cumplimientoService:CumplimientoService,
    private usuarioService:UsuarioService,
    public dialogRef: MatDialogRef<ModalCumplimientoComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any , private router: Router) {

   }

  
  ngOnInit() {
    console.log(this.requerimientoCliente);
  

    console.log(this.requerimientoCliente.fechaEjecucionPlanAccion);

  }
  opcionSeleccionadaCumpli(event){
    console.log(event);
    this.requerimientoCliente.nivelCumplimiento=event;

  }

  opcionSeleccionadaCriti(event){
    console.log(event);
    this.requerimientoCliente.criticidad=event;

  }
  close(): void {
    this.dialogRef.close();
  }
  transform(value: string) {
      console.log(value)
    if(value.length == undefined){
      var datePipe = new DatePipe("en-US");
      value = datePipe.transform(value, 'dd/MM/yyyy');
      console.log(value)
      return value;
    }else{
    return value;
    }
 }
  guardarInformacion() {
    
    console.log(this.requerimientoCliente.fechaEjecucionPlanAccion);
    this.requerimientoCliente.fechaEjecucionPlanAccion=this.transform(this.requerimientoCliente.fechaEjecucionPlanAccion);
    this.requerimientoCliente.fechaInicioNotificaciones=this.transform(this.requerimientoCliente.fechaInicioNotificaciones);
    this.cumplimientoService.getGuardarTextoReqCliente(this.usuarioService.usuario.nombreUsuario,this.requerimientoCliente).subscribe(data => {
      if(data.status==this.cumplimientoService.codOK){
        console.log(data);
        console.log(this.requerimientoCliente);
      }else{
      this.cumplimientoService.getManejoError(data);
      }
      })
   /* if(this.busqueda.filtrosDetalle.filtros.estado == null){
      this.busqueda.filtrosDetalle.filtros= new FiltrosBusqueda();
    }*/
   

  
  }

  onCheckChange(event) {
    console.log(event.checked);
    if(event.checked){
    this.requerimientoCliente.notificacionesActivo="S";
    }else{
      this.requerimientoCliente.notificacionesActivo="N";
    }

    
  }
}
