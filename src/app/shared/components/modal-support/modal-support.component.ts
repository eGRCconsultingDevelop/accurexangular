import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';

@Component({
  selector: 'app-modal-support',
  templateUrl: './modal-support.component.html',
  styleUrls: ['./modal-support.component.scss']
})
export class ModalSupportComponent implements OnInit {

  constructor(
    public dialogRef: MatDialogRef<ModalSupportComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) { }

  ngOnInit() {
    console.log(this.data);
  }

  close(): void {
    this.dialogRef.close();
  }

}
