import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { LineaBaseService } from 'src/app/service/lineabase.service';

@Component({
  selector: 'app-header-filter-baseline',
  templateUrl: './header-filter-baseline.component.html',
  styleUrls: ['./header-filter-baseline.component.scss']
})
export class HeaderFilterBaselineComponent implements OnInit {
  @Output() public clickButtonFuse = new EventEmitter<any>();
  @Output() public clickButtonNew = new EventEmitter<any>();

  public switchFilter = 'filters';
  public cantidadLineasBase:number=0;
  public fusionar:boolean;
  constructor(private lineaBaseService:LineaBaseService) {

   }

  ngOnInit() {
   
  }

  clickFuse() {
    this.clickButtonFuse.emit({});
    this.fusionar=true;
    
  }

  clickNew() {
    this.clickButtonNew.emit({});
  }

}
