import { Component, OnInit, Inject } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';
import { HistoryComponent } from '../../../pages/search/history/history.component';
import { BusquedaService } from 'src/app/service/busqueda.service';
import { BusquedaUsuario } from 'src/app/shared/dto/BusquedaUsuario';
import { Router } from '@angular/router';
@Component({
  selector: 'app-modal-cambio-reg',
  templateUrl: './modal-cambio-reg.component.html',
  styleUrls: ['./modal-cambio-reg.component.scss']
})
export class ModalCambioRegComponent implements OnInit {

  public busquedasUsuario: Array<BusquedaUsuario>=[];

  constructor(
    public dialogRef: MatDialogRef<ModalCambioRegComponent>,
    private busquedaService: BusquedaService,
    private router: Router,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) { }

  ngOnInit() {
    console.log(this.data);
  }

  close(type): void {
      this.dialogRef.close(this.data);
      this.router.navigate(['/search/history']);
  }

}
