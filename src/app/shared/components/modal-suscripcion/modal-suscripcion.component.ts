import { Component, OnInit, Inject } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';
import { BusquedaService } from 'src/app/service/busqueda.service';
import { Regulacion } from '../../dto/Regulacion';
import { Busqueda } from '../../dto/Busqueda';
import { Router } from '@angular/router';
import { FiltrosDetalleBusqueda } from '../../dto/FiltrosDetalleBusqueda';
import { FiltrosBusqueda } from '../../dto/FiltrosBusqueda';
import { UsuarioService } from 'src/app/service/usuario.service';
import { Jurisdiccion } from '../../dto/Jurisdiccion';
import { Sector } from '../../dto/Sector';
import { DominioImpacto } from '../../dto/DominioImpacto';


@Component({
  selector: 'app-modal-suscripcion',
  templateUrl: './modal-suscripcion.component.html',
  styleUrls: ['./modal-suscripcion.component.scss']
})



export class ModalSuscripcionComponent implements OnInit {
  public jurisdicciones:Jurisdiccion;
  public sectores:Sector;
  public dominiosImpacto:DominioImpacto;

  constructor(private busquedaService:BusquedaService,
    private usuarioService:UsuarioService,
    public dialogRef: MatDialogRef<ModalSuscripcionComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any , private router: Router) {
    
   }

  
  ngOnInit() {
    this.consultarJurisdicciones(); 
    this.consultarSectores(); 
    this.consultarDominiosImpacto(); 

  }

  close(): void {
    this.dialogRef.close();
  }
 
  administrarUsuarios(){
    this.router.navigate(['/users/list']);
    this.dialogRef.close();
  }


  consultarJurisdicciones(){
    this.busquedaService.getJurisdiccionesCliente(this.usuarioService.usuario.nombreUsuario,this.usuarioService.usuario.cliente.idCliente).subscribe(data => {
      if(data.status==this.busquedaService.codOK){
      this.jurisdicciones=data.response;
        console.log(this.jurisdicciones);
      }else{
      this.busquedaService.getManejoError(data);
      }
      })
    }

    consultarSectores(){
      this.busquedaService.getSectoresCliente(this.usuarioService.usuario.nombreUsuario,this.usuarioService.usuario.cliente.idCliente).subscribe(data => {
        if(data.status==this.busquedaService.codOK){
        this.sectores=data.response;
          console.log(this.sectores);
        }else{
        this.busquedaService.getManejoError(data);
        }
        })
      }

    consultarDominiosImpacto(){
        this.busquedaService.getDominiosImpactoCliente(this.usuarioService.usuario.nombreUsuario,this.usuarioService.usuario.cliente.idCliente).subscribe(data => {
          if(data.status==this.busquedaService.codOK){
          this.dominiosImpacto=data.response;
          console.log(this.dominiosImpacto);
          }else{
          this.busquedaService.getManejoError(data);
          }
          })
      }
}
