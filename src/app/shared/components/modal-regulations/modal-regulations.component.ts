import { Component, OnInit, Inject } from '@angular/core';
import { NestedTreeControl } from '@angular/cdk/tree';
import { MatTreeNestedDataSource, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { CambioRegulatorioService } from 'src/app/service/cambio-regulatorio.service';
import { Regulacion } from '../../dto/Regulacion';
import { ItemNode } from '../../dto/ItemNode';
import { Nivel } from '../../dto/Nivel';
import { LineaBase } from '../../dto/LineaBase';
import { UsuarioService } from 'src/app/service/usuario.service';

@Component({
  selector: 'app-modal-regulations',
  templateUrl: './modal-regulations.component.html',
  styleUrls: ['./modal-regulations.component.scss']
})
export class ModalRegulationsComponent implements OnInit {

  treeControl = new NestedTreeControl<any>(node => node.children);
  dataSource = new MatTreeNestedDataSource<any>();
  public regulacion:Regulacion=this.cambioRegService.regulacionNueva;
  public arbol:Array<ItemNode>=[];
  public lineasBaseAfec:Array <LineaBase>=this.cambioRegService.lineasBaseAfect;
  public idLineasBase:Array <number>=[];
  constructor(
    public dialogRef: MatDialogRef<ModalRegulationsComponent>,
    public cambioRegService:CambioRegulatorioService,
    private usuarioService:UsuarioService,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) {
    this.construirArbol('');

  }

 
  hasChild = (_: number, node: any) => !!node.children && node.children.length > 0;
  isReq = (_: number, node: any) => node.type=='R';
  ngOnInit() {
   console.log(this.regulacion);
    for(let i=0;i<this.lineasBaseAfec.length;i++){
      this.idLineasBase.push(this.lineasBaseAfec[0].idLinea);
    }
  }

  close(): void {
    this.dialogRef.close();
  }

  tieneHijos(node){
    return !!node.children && node.children.length > 0;
  }

 


asociarLinasBase(){
  console.log(this.regulacion.id);
  console.log(this.idLineasBase);
  this.cambioRegService.getAsociarALinea(this.usuarioService.usuario.nombreUsuario,this.regulacion.id,this.usuarioService.usuario.cliente.idCliente,this.idLineasBase).subscribe(data => {
    if(data.status==this.cambioRegService.codOK){
     
      console.log(data.response);
      this.dialogRef.close();
     
     
    }else{
    this.cambioRegService.getManejoError(data);
    }
    })
}

  evaluarIsExpanded(node:any){
    if(node.cargaInicial){
      node.cargaInicial=false;
      if(node.coincidencias){
        this.treeControl.expand(node);
        return true;
      }
      if(!this.treeControl.isExpanded(node)){
        if(this.evaluarHijosCoincidentes(node)){
          this.treeControl.expand(node);
          return true;
        }
      }
    } 
    return this.treeControl.isExpanded(node);
  }

  evaluarHijosCoincidentes(node:any){
    if(this.tieneHijos(node)){
      for(let child of node.children){
        if(child.coincidencias){
          return true;
        }else{
          this.evaluarHijosCoincidentes(child);
        }
      }
    }
    return false;
  }

    construirArbol(busqueda:string) {
		//defino la variable arbol
    let nodo: ItemNode= new ItemNode();
    let nivelesSuperiores:Array<Nivel>=[];
    this.arbol=[];
		//saco los niveles que NO tienen padre, es decir que el idTextoPadre sea nulo o vacio
    for(let i=0;i< this.regulacion.niveles.length ; i++){
      if(this.regulacion.niveles[i].nivelSuperior == null){
        nivelesSuperiores.push(this.regulacion.niveles[i]);
      }
    }
		//instancio el arbol
		//inicio la adi cion de hijos, le envio el arbol entero, los niveles padres, y TODOS los niveles existentes (padres e hijos)

    this.addRamaArbol(nodo, nivelesSuperiores, this.regulacion.niveles);
    this.dataSource.data=this.arbol;
	}
	
  addRamaArbol(nodo:ItemNode,hijos:Array<Nivel>,nivelesEstructura:Array<Nivel>) {
    let nivelesSuperioresAux: Array<Nivel>=[];

    let nivel2:Nivel;
      for(let nivel of hijos) {
        for(let nivel2 of nivelesEstructura) {
          if(nivel2.idTextoPadre !=null){
            if(nivel.idTexto==nivel2.idTextoPadre) {
              nivelesSuperioresAux.push(nivel2);
            }
          }
        }
    
        let nodoHijo: ItemNode= new ItemNode();
        let reqs: Array<ItemNode>= [];
        let coincidencias:boolean=false;

      

        if(nivel.requerimientos.length>0){
          for(let i=0;i< nivel.requerimientos.length ; i++){
            let nodoReq: ItemNode= new ItemNode();
            nodoReq.id=nivel.requerimientos[i].idRequerimiento;
            nodoReq.idTextoPadre=nivel.idTexto;
            nodoReq.texto=nivel.requerimientos[i].codigoRequerimiento;
            nodoReq.textoAsociado=nivel.requerimientos[i].requerimiento;
            nodoReq.type='R';
            nodoReq.tieneRequerimientos=nivel.tieneRequerimientos;
            nodoReq.coincidencias=false;
            nodoReq.cargaInicial=false;
            reqs.push(nodoReq);
          }
        }
        if(nivel.idTextoPadre==null){
          nodo= new ItemNode();
          nodo.id=nivel.idTexto;
          nodo.texto=nivel.texto;
          nodo.idTextoPadre=nivel.idTextoPadre;
          nodo.textoAsociado=nivel.textoAsociado;
          nodo.type='N';
          nodo.tieneRequerimientos=nivel.tieneRequerimientos;
          nodo.children=reqs;
          nodo.coincidencias=coincidencias;
          nodo.cargaInicial=true;
          nodoHijo=nodo;
          this.arbol.push(nodo);
        }else{
          nodoHijo.type='N';
          nodoHijo.id=nivel.idTexto;
          nodoHijo.texto=nivel.texto;
          nodoHijo.idTextoPadre=nivel.idTextoPadre;
          nodoHijo.textoAsociado=nivel.textoAsociado;
          nodoHijo.tieneRequerimientos=nivel.tieneRequerimientos;
          nodoHijo.children=reqs;
          nodoHijo.coincidencias=coincidencias;
          nodoHijo.cargaInicial=true;
          nodo.children.push(nodoHijo);
        }
        //llamo nuevamente a este metodo agregando un nodo nuevo, y los nuevos hijos q ahora son padres, y TODOS los niveles nuevamente
        this.addRamaArbol(nodoHijo, nivelesSuperioresAux, nivelesEstructura);
        nivelesSuperioresAux=[];      
      }
	}
}
