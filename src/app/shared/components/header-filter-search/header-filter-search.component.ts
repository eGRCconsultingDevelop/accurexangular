import { Component, OnInit } from '@angular/core';
import { BusquedaService } from 'src/app/service/busqueda.service';
import { FiltrosBusqueda } from '../../dto/FiltrosBusqueda';
import { Jurisdiccion } from '../../dto/Jurisdiccion';
import { Sector } from '../../dto/Sector';
import { DominioImpacto } from '../../dto/DominioImpacto';
import { FuenteAutoridad } from '../../dto/FuenteAutoridad';
import {FormControl} from '@angular/forms';
import {Observable} from 'rxjs';
import {map, startWith} from 'rxjs/operators';
import { Router } from '@angular/router';
import { UsuarioService } from 'src/app/service/usuario.service';

@Component({
  selector: 'app-header-filter-search',
  templateUrl: './header-filter-search.component.html',
  styleUrls: ['./header-filter-search.component.scss'],
})
export class HeaderFilterSearchComponent implements OnInit {

  public switchFilter;
  public filtrosBusqueda:FiltrosBusqueda= new FiltrosBusqueda;
  public jurisdicciones:Jurisdiccion;
  public sectores:Sector;
  public dominiosImpacto:DominioImpacto;
  public fuentesAutoridad:FuenteAutoridad;
  public palabraClave: string;
  public jurisSeleccionadas: string[]=[];
  public regulacionesSelec: number[]=[];
  public myControl = new FormControl();
  public myControl2 = new FormControl();
  public options: string[] = [];
  public regulaciones: string[]=[];
  public filteredOptions: Observable<string[]>;
  public filteredRegulacion: Observable<string[]>;
  public regulacion: string; 
  public option: string;

  constructor(private busquedaService: BusquedaService,
    private router: Router,private usuarioService:UsuarioService) { 
      console.log(this.busquedaService.menu);
  if(this.busquedaService.menu == 'filters' || this.busquedaService.menu == undefined){
    this.switchFilter='filters'
  }else{
    this.switchFilter='search'
  }
  this.consultarJurisdicciones(); 
  this.consultarSectores(); 
  this.consultarDominiosImpacto(); 
  this.consultarFuentesAutoridad(); 
   // this.filtrosBusqueda= this.busquedaService.resultadoBusqueda.filtrosDetalleDTO.FiltrosBusquedaDTO;
   console.log(this.busquedaService.resultadoBusqueda);
   if(this.busquedaService.resultadoBusqueda != null){
  //this.filtrosBusqueda= this.busquedaService.resultadoBusqueda.filtrosDetalleDTO.FiltrosBusquedaDTO;  
    this.filtrosBusqueda =this.busquedaService.resultadoBusqueda.filtrosDetalleDTO.filtros;
    this.palabraClave=this.filtrosBusqueda.palabraClavePrincipal;
    this.regulacion=this.busquedaService.resultadoBusqueda.filtrosDetalleDTO.nombreRegulacion;
   }else{
 
   }
  }
  
  ngOnInit() {
    this.filteredOptions = this.myControl.valueChanges
    /*.pipe(
      startWith(''),
      map(value => this._filter(value))
    );*/

    this.filteredRegulacion= this.myControl2.valueChanges
    .pipe(
      startWith(''),
      map(value => this._filterRegulacion(value))
    );
  }
  /*private _filter(value: string): string[] {
    const filterValue = value.toLowerCase();

    return this.options.filter(palabraClave => palabraClave.toLowerCase().includes(filterValue));
  }*/
  
  private _filterRegulacion(value: string): string[] {
    const filterValue = value.toLowerCase();

    return this.regulaciones.filter(regulacion => this.regulacion.toLowerCase().includes(filterValue));
  }
  cambiarFiltro(valor){ 
    console.log(valor);
    this.busquedaService.menu=valor;
    this.switchFilter=valor;
  }
/*consultarTags(valor){
  this.filtrosBusqueda.palabraClavePrincipal=valor;
  console.log(this.filtrosBusqueda.palabraClavePrincipal);
  if(valor != ""){
  this.busquedaService.getTags(valor,'ACTIVO').subscribe(data => {
    if(data.status==this.busquedaService.codOK){
    this.options=data.response;
    console.log(this.options);
    }else{
    this.busquedaService.getManejoError(data);
    }
    })

    this.ngOnInit();
  }
  }*/
  consultarJurisdicciones(){
    this.busquedaService.getJurisdiccionesCliente(this.usuarioService.usuario.nombreUsuario,this.usuarioService.usuario.cliente.idCliente).subscribe(data => {
      if(data.status==this.busquedaService.codOK){
      this.jurisdicciones=data.response;
        console.log(this.jurisdicciones);
      }else{
      this.busquedaService.getManejoError(data);
      }
      })
    }

    consultarSectores(){
      this.busquedaService.getSectoresCliente(this.usuarioService.usuario.nombreUsuario,this.usuarioService.usuario.cliente.idCliente).subscribe(data => {
        if(data.status==this.busquedaService.codOK){
        this.sectores=data.response;
          console.log(this.sectores);
        }else{
        this.busquedaService.getManejoError(data);
        }
        })
      }

    consultarDominiosImpacto(){
        this.busquedaService.getDominiosImpactoCliente(this.usuarioService.usuario.nombreUsuario,this.usuarioService.usuario.cliente.idCliente).subscribe(data => {
          if(data.status==this.busquedaService.codOK){
          this.dominiosImpacto=data.response;
          console.log(this.dominiosImpacto);
          }else{
          this.busquedaService.getManejoError(data);
          }
          })
      }

    consultarFuentesAutoridad(){
        this.busquedaService.getFuenteAutoridadCliente(this.usuarioService.usuario.nombreUsuario,this.usuarioService.usuario.cliente.idCliente).subscribe(data => {
          if(data.status==this.busquedaService.codOK){
          this.fuentesAutoridad=data.response;
            console.log(this.fuentesAutoridad);
          }else{
          this.busquedaService.getManejoError(data);
          }
          })
      }

    consultarRegulacioesNombre(valor){
      console.log(valor);
      if(valor != ""){
      this.busquedaService.getNombresRegulacion(valor,"ACTIVO",this.usuarioService.usuario.nombreUsuario).subscribe(data => {
        if(data.status==this.busquedaService.codOK){
        this.regulaciones=data.response;
          console.log(this.regulaciones);
        }else{
        this.busquedaService.getManejoError(data);
        }
        })
      }
        this.ngOnInit();
    }

    crearBusqueda(event){
     this.busquedaService.dominiosImpacto=this.dominiosImpacto;
     this.busquedaService.cantidadDominiosSelec=this.filtrosBusqueda.idDominiosImpactoSel.length
     console.log(this.filtrosBusqueda.idDominiosImpactoSel.length); 
      this.filtrosBusqueda.estado="ACTIVO"; 
      this.filtrosBusqueda.palabraClavePrincipal=this.palabraClave;
      console.log(this.filtrosBusqueda.palabraClavePrincipal);
        this.busquedaService.getRegulacionesFiltro(this.filtrosBusqueda,this.usuarioService.usuario.nombreUsuario,0).subscribe(data => {
          if(data.status==this.busquedaService.codOK){
            this.busquedaService.resultadoBusqueda=data.response;
            console.log(this.busquedaService.resultadoBusqueda);  
            this.router.navigate(['/search']);
          }else{
           this.busquedaService.getManejoError(data);
          }
          })
    }

    crearBusquedaRegulacion(event){
     
      this.filtrosBusqueda.estado="ACTIVO";
      console.log(this.filtrosBusqueda);
        this.busquedaService.getRegulacionesNombre(this.regulacionesSelec,this.usuarioService.usuario.nombreUsuario,this.regulacion,"ACTIVO",0).subscribe(data => {
          if(data.status==this.busquedaService.codOK){
            this.busquedaService.resultadoBusqueda=data.response;
            console.log(this.busquedaService.resultadoBusqueda);
            this.router.navigate(['/search']);
          }else{
           this.busquedaService.getManejoError(data);
          }
          })
    }
    
    
  }
