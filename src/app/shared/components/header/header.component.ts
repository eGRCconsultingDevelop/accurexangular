import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material';

import { ModalSupportComponent } from '../modal-support/modal-support.component';
import { ModalComponent } from '../modal/modal.component';
import { CambioRegulatorioService } from 'src/app/service/cambio-regulatorio.service';
import { CambioRegulatorio } from '../../dto/CambioRegulatorio';
import { ModalAdministrarCuentaComponent } from '../modal-administrar-cuenta/modal-administrar-cuenta.component';
import { ModalSuscripcionComponent } from '../modal-suscripcion/modal-suscripcion.component';
import { BusquedaService } from 'src/app/service/busqueda.service';
import { Router } from '@angular/router';
import { UsuarioService } from 'src/app/service/usuario.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {
  public cambiosRegulatoriosPend:Array <CambioRegulatorio>=[];
  public iniciales:string;
  
  constructor(private cambioRegService:CambioRegulatorioService,
    private busquedaService:BusquedaService,
    private router:Router,
    private usuarioService:UsuarioService,
    public dialog: MatDialog
  ) { 

    this.consultarCambiosRegPendientes();
  }

  ngOnInit() {
    this.iniciales=this.usuarioService.usuario.nombres[0].toUpperCase()+this.usuarioService.usuario.apellidos[0].toUpperCase();
    console.log(this.iniciales)
  }


  busqueda(){
    this.busquedaService.resultadoBusqueda=undefined;
    console.log("sadasd")
    this.router.navigate(['/search/history'])
  }

  openSupportForm() {
    const dialogRef = this.dialog.open(ModalSupportComponent, {
      width: '500px',
      data: {

      }
    });
  }
  openAdministrarCuenta() {
    const dialogRef = this.dialog.open(ModalAdministrarCuentaComponent, {
      width: '500px',
      data: {

      }
    });
  }
  openSuscripcion() {
    const dialogRef = this.dialog.open(ModalSuscripcionComponent, {
      width: '500px',
      data: {

      }
    });
  }
  closeSession() {
    const dialogRef = this.dialog.open(ModalComponent, {
      width: '500px',
      data: {
        title: 'Confirmación',
        body: `Está seguro de que desea cerrar sesión`,
        cancelButton: 'Cancelar',
        acceptButton: 'Sí'
      }
    });
  }
    
  consultarCambiosRegPendientes(){
    this.cambioRegService.getConsultarCambiosRegulatoriosPendientes(this.usuarioService.usuario.nombreUsuario,this.usuarioService.usuario.cliente.idCliente).subscribe(data => {
      if(data.status==this.cambioRegService.codOK){
        this.cambiosRegulatoriosPend=data.response;
        console.log(this.cambiosRegulatoriosPend.length);
      
       // this.dataSource.paginator = this.paginator;
      }else{
      this.cambioRegService.getManejoError(data);
      }
      })
  }

}
