import { Component, OnInit, Inject } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';
import { HistoryComponent } from '../../../pages/search/history/history.component';
import { BusquedaService } from 'src/app/service/busqueda.service';
import { BusquedaUsuario } from 'src/app/shared/dto/BusquedaUsuario';
import { Router } from '@angular/router';
@Component({
  selector: 'app-modal-validador',
  templateUrl: './modal-validador.component.html',
  styleUrls: ['./modal-validador.component.scss']
})
export class ModalValidadorComponent implements OnInit {

  public busquedasUsuario: Array<BusquedaUsuario>=[];

  constructor(
    public dialogRef: MatDialogRef<ModalValidadorComponent>,
    private busquedaService: BusquedaService,
    private router: Router,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) { }

  ngOnInit() {
    console.log(this.data);
  }

  close(type): void {
      this.dialogRef.close(this.data);
     
  }

}
