import { Component, OnInit, Inject } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';
import { LineaBaseService } from 'src/app/service/lineabase.service';
import { Router } from '@angular/router';
import { UsuarioService } from 'src/app/service/usuario.service';

@Component({
  selector: 'app-modal-save-baseline',
  templateUrl: './modal-save-baseline.component.html',
  styleUrls: ['./modal-save-baseline.component.scss']
})
export class ModalSaveBaselineComponent implements OnInit {

  constructor(private lineaBaseService:LineaBaseService,private router: Router,
    public dialogRef: MatDialogRef<ModalSaveBaselineComponent>,
    private usuarioService:UsuarioService,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) { }

  ngOnInit() {
    
    console.log(this.data);
    console.log(this.lineaBaseService.lineasBaseFusion)
  }
  onCheckChange(event,lineasBase) {
    console.log(event.checked);
    console.log(lineasBase);
    if(event.checked){
      
    
      this.lineaBaseService.fusionLineaBase.idLineasBase.push(lineasBase.idLinea);
      /*this.busqueda.idRegulacionesSeleccionadas.push(id);
      console.log(this.busqueda.idRegulacionesSeleccionadas);*/
    }else{
   

     
     var index=   this.lineaBaseService.fusionLineaBase.idLineasBase.indexOf(lineasBase.idLinea);
      if ( index !== -1 ) {
        this.lineaBaseService.fusionLineaBase.idLineasBase.splice( index, 1 );
      }
      console.log(  this.lineaBaseService.fusionLineaBase.idLineasBase);
    }
    
  }
  close(type): void {
    this.dialogRef.close();
    this.dialogRef.close(this.data);
  }
  guardaLineaBase(type): void{
    this.data.reason = type;
    this.lineaBaseService.fusionLineaBase.nombreUsuario=this.usuarioService.usuario.nombreUsuario;
    this.lineaBaseService.fusionLineaBase.estadoLineaBaseUsuario="DEFINITIVO";
    console.log(this.lineaBaseService.fusionLineaBase);
    this.lineaBaseService.getFusionarLineas(this.lineaBaseService.fusionLineaBase).subscribe(data => {
      if(data.status==this.lineaBaseService.codOK){
       
        this.router.navigateByUrl('/baseline', { skipLocationChange: true }).then(() => {
          this.router.navigate(['/baseline']);
      });
       
      }else{
      this.lineaBaseService.getManejoError(data);
      }
      this.dialogRef.close(this.data);
      })
  }

}
